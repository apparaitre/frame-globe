<?php
	if (!defined("_PROFIL_ACCESS")){header("location:../erreur");}
	
	//switch($_SESSION["contexte"][$_SESSION["contexte"]["rubrique"]]["page"])
	//$page = isset($_GET["page"])?$_GET["page"]:"";
	switch($_PAGE)
	{
		default:
		
		case "liste_rubrique":
			include("actions/admin/articles_liste_rubrique.php");
			break;
			
		case "liste_articles":
			include("actions/admin/articles_liste_articles.php");
			break;
			
		case "ajouter_rubrique":
			include("actions/admin/articles_ajouter_rubrique.php");
			break;
			
		case "editer_rubrique":
			include("actions/admin/articles_editer_rubrique.php");
			break;
			
		case "supprimer_rubrique":
			include("actions/admin/articles_supprimer_rubrique.php");
			break;
			
		case "ajouter_article":
			include("actions/admin/articles_ajouter_article.php");
			break;
			
		case "editer_article":
			include("actions/admin/articles_editer_article.php");
			break;
			
		case "supprimer_article":
			include("actions/admin/articles_supprimer_article.php");
			break;
			
	}
	
	$page = new Page($templateDir, $templateFile,  $_SESSION["locale"]);
	$page->setVars($dynamic_vars);
	
	$page->replace_tags($tmpVars, $replace);
	$html = $page->output();
	
?>