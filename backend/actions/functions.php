<?php
	if (!defined("_PROFIL_ACCESS")){header("location:../erreur");}
	$page = new Page();
	
		$accueil =  array (
			"accueil"	=> array( 
				"code" => "accueil",
				"class" => "", 
				"libelle" => $page->_T("libMenuDASHBOARD"),
				"color" => "orange",
				"icon" =>	"fa-edit",
				"sousmenu" => array()
			)
		);
	

		$modules = array( "modules"	=> array( 
				"code" => "modules",
				"behavior" => "nolink",
				"class" => "", 
				"libelle" => $page->_T("libMenuMODULES"),
				"color" => "red",
				"icon" =>	"fa-dashboard",
				"sousmenu" => array()
			)
		);
		/*
		 * modules
		 * Insertion du hook menu principal
		 */
		
		$module_hook = "menu_principal";
		
		
		if ($handle = opendir(_MODULE_PATH)) {
		   while (false !== ($entry = readdir($handle))) {
		   		if ($entry != "." && $entry != "..") {
		   			if(is_file(_MODULE_PATH . "/" . $entry . "/module.php"))
		   			{
		   				include(_MODULE_PATH . "/" . $entry . "/module.php");

		   				$item = array("code" => $code, "icon" => $icon, "color" => $color, "libelle" => $libelle);

		   				if($position == "primary")
		   				{
		   					$pModules['module-' . $code] = $item;
		   				}
		   				elseif($position == "modules")
		   				{
		   					$ssModules['module-' . $code] = $item;
		   				}
		   			}
			    	
			     }
		    }


		    closedir($handle);
		}

		$menu['admin'] = array_merge($accueil, $pModules, $modules);
		if(count($ssModules) > 0)
		{
			$menu['admin']['modules']['sousmenu'] = $ssModules;
		}
		else
		{
			unset($menu['admin']['modules']);
		}
		

$make_menu = function($user_groupe)
{
	global $public_role, $menu;
	if(isset($user_groupe))
	{
		if($public_role != "")
			{
				$role = $public_role ;
				$public = $public_role;
			}
			else
			{
				$role =  $user_groupe;
				$public = "";
			}
		$menu_role = $menu[$role];
		if(count($menu_role) > 0)
		{
			$menu_principal = '<ul class="nav navbar-nav side-nav" id="navigation">';
			foreach ($menu_role as $item)
			{
				$rubrique = $item["code"];
				$active = $rubrique == $_SESSION["contexte"]["rubrique"] ? "active" : "";
				$class = $item["class"] . " " . $active;
				if(count($item["sousmenu"]) > 0)
				{
					$class .=  " dropdown";
				}

				if($item["behavior"] == "nolink")
				{
					$item_url = "#";
					$class_toggle = "dropdown-toggle";
					$data_toggle="dropdown";
				}
				else
				{
					$rubrique_url = new Url();
					$rubrique_url->setType($_SESSION["type_url"]);
					/*
					if($public_role != "" && $_SESSION["user"]["groupe"] != $public_role)
					{
						$rubrique_url->setParam("role", $public_role);
					}
					*/

					$item_url = $rubrique_url->getUrl("modules," . $rubrique);
					$class_toggle = "";
					$data_toggle="";

				}
				

				$menu_principal .= "<li class=\"menu " . $item["code"] . " " . $class . "\">
                <a href=\"" . $item_url . "\" title=\"" . $item["libelle"] . "\" class=\"" . $class_toggle . "\" data-toggle=\"" . $data_toggle . "\">
                  <i class=\"fa " . $item["icon"] . "\">
                    <span class=\"overlay-label " . $item["color"] . "\"></span>
                  </i>
                  " . $item["libelle"] . " <b class=\"fa fa-angle-left dropdown-arrow\"></b>
                </a>";

             	if(count($item["sousmenu"]) > 0)
				{
					$sous_menu[$rubrique] .= '<ul class="dropdown-menu animated fadeInDown">';
					foreach ($item["sousmenu"] as $ssitem)
					{
						$ssitem_url = new Url();
						$ssitem_url->setType($_SESSION["type_url"]);
						/*
						if($public_role != "" && $_SESSION["user"]["groupe"] != $public_role)
						{
							$role = $public_role ."@";
						}
						*/
						$page_url = $ssitem_url->getUrl($role . $rubrique . "," . $ssitem["code"]);
						
						$ssactive = $ssitem["code"] == $_SESSION["contexte"][$_SESSION["contexte"]["rubrique"]]["page"] ? "active" : "";
						$class = $ssitem["class"] . " " . $ssactive;
						$sous_menu[$rubrique] .= "<li><a href=\"" . $page_url . "\" title=\"" . $ssitem["libelle"] . "\" ><i class=\"fa " . $ssitem["icon"] . "\"><span class=\"overlay-label " . $ssitem["color"] . "\"></span></i>" . $ssitem["libelle"] . "</a></li>";
						
					}
				
					$sous_menu[$rubrique] .= "</ul>";
				}
				$menu_principal .= $sous_menu[$rubrique] . "</li>";
			}
			$menu_principal .= "</ul>";
		}
	}
	return $menu_principal;
}
?>