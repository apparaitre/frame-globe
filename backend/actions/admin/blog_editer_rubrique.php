<?php
	$page_name="";
	$page = new Page();
	$soustitre = $page->_T("libSoustitre");
	$description = $page->_T("libDescription");
	$content = ""; 

	if(!isset($_GET['id_rubrique']))
	{
		header('location:index.php?page=liste_rubriques');
	}
	
	$form = new Formulaire($conn);
	$form->load("backend/formulaires/rubrique");
	$form->setRedirection("?page=liste_rubriques");
	$form->setStaticInput('T05_codeinterne_i', $_GET['id_rubrique']);
	
	if(isset($_POST['submit']))
	{
		$retour = $form->update($_GET['id_rubrique']);
		$error = $retour['error'];
	}
	
		$article = $form->get($_GET['id_rubrique']);
	
	$tmpVars = array();
	$replace = array();
	
	$templateVars = $form->getTemplateFormVars($article, $error, "form");
	
	$tmpVars = $templateVars['tmpVars'];
	$replace = $templateVars['replace'];
	$templateFile = "admin_blog_editer_rubrique.html";
?>