<?php
	$page_name="";
	$page = new Page();
	$soustitre = $page->_T("libSoustitre");
	$description = $page->_T("libDescription");
	$content = ""; 
	$LISTE_RUBRIQUES = ""; 
	
	$rubriques = $T05->selectAll();
	
	if(count($rubriques) > 0)
	{
		$LISTE_RUBRIQUES = '<table class="datatable"><thead><tr><th class="w100p">ID</th><th>Titre</th><th class="w100p"></th></tr></thead><tbody>';
		foreach($rubriques as $rubrique)
		{
			$LISTE_RUBRIQUES .= '<tr><td>' . $rubrique->T05_codeinterne_i . '</td><td><a href="index.php?page=liste_articles&id_rubrique=' . $rubrique->T05_codeinterne_i . '">' . $rubrique->T05_titre_va . '</a></td><td><a href="index.php?page=editer_rubrique&id_rubrique=' . $rubrique->T05_codeinterne_i . '" title="Editer la rubrique"><img src="backend/templates/images/icons/icon_editer.png" alt="Editer la rubrique"  width="16" height="16"/></a></td>';
		}
		$LISTE_RUBRIQUES .= '</table>';
	}
	else
	{
		$message = '<div class="error">' . _T('libPasDeRubrique') . '</div>';
	}
	
	$templateFile = "admin_blog_liste_rubrique.html";
	$tmpVars = array("/{LISTE_RUBRIQUES}/i", "/{MESSAGE}/i");
	$replace = array($LISTE_RUBRIQUES, $message);	
	$dynamic_vars = array();

?>