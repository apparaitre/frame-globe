<?php
	$page_name="";
	$page = new Page();
	$soustitre = $page->_T("libSoustitre");
	$description = $page->_T("libDescription");
	$content = ""; 

	$form = new Formulaire($conn);
	$form->load("backend/formulaires/rubrique");
	$form->setRedirection("?page=liste_rubrique");
	$form->setStaticInput('T01_codeinterne_i', $_SESSION['user']['id_user']);

	if(isset($_POST['submit']))
	{
		$retour = $form->save();
		$error = $retour['error'];
		if(count($error) == 0)
		{
			$rubrique = $form->get($retour['codeinterne']);
		}
	}
	
	$tmpVars = array();
	$replace = array();
	
	$templateVars = $form->getTemplateFormVars($rubrique, $error, "form");
	
	$tmpVars = $templateVars['tmpVars'];
	$replace = $templateVars['replace'];
	
	$templateFile = "admin_blog_ajouter_rubrique.html";

?>