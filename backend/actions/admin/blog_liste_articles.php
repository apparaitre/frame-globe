<?php
	$page_name="";
	$page = new Page();
	$soustitre = $page->_T("libSoustitre");
	$description = $page->_T("libDescription");
	$content = ""; 
	
	$articles = $T06->selectByRubrique($_GET['id_rubrique']);
	
	if(count($articles) > 0)
	{
		$LISTE_ARTICLES = '<table class="datatable"><thead><tr><th class="w100p">ID</th><th>Titre</th><th class="w100p"></th></tr></thead><tbody>';
		foreach($articles as $article)
		{
			$LISTE_ARTICLES .= '<tr><td>' . $article->T06_codeinterne_i . '</td><td><a href="index.php?page=editer_article&id_rubrique=' . $_GET['id_rubrique'] . '&id_article=' . $article->T06_codeinterne_i . '">' . $article->T06_titre_va . '</a></td><td><a href="index.php?page=editer_article&id_rubrique=' . $_GET['id_rubrique'] . '&id_article=' . $article->T06_codeinterne_i . '" title="Editer l\'article"><img src="backend/templates/images/icons/icon_editer.png" alt="Editer l\'article"  width="16" height="16"/></a></td>';
		}
		$LISTE_ARTICLES .= '</table>';
	}
	else
	{
		$message = '<div class="error">' . _T('libPasDArticle') . '</div>';
	}
	
	$templateFile = "admin_blog_liste_articles.html";
	$tmpVars = array("/{CONTENT}/i", "/{MESSAGE}/i", "/{LISTE_ARTICLES}/i", "/{T05_codeinterne_i}/i");
	$replace = array($content, $message, $LISTE_ARTICLES, $_GET['id_rubrique']);	
	$dynamic_vars = array();

?>