<?php
	$page_name="";
	$page = new Page();
	$soustitre = $page->_T("libSoustitre");
	$description = $page->_T("libDescription");
	$content = ""; 
	if(!isset($_GET['id_article']))
	{
		header('location:index.php?page=liste_rubriques');
	}
	
	$form = new Formulaire($conn);
	$form->load("backend/formulaires/article");
	$form->setRedirection("?page=liste_articles&id_rubrique=" . $_GET['id_rubrique']);
	$form->setStaticInput('T05_codeinterne_i', $_GET['id_rubrique']);
	
	if(isset($_POST['submit']))
	{
		$retour = $form->update($_GET['id_article']);
		$error = $retour['error'];
	}
	
		$article = $form->get($_GET['id_article']);
	
	$tmpVars = array();
	$replace = array();
	
	$templateVars = $form->getTemplateFormVars($article, $error, "form");
	
	$tmpVars = $templateVars['tmpVars'];
	$replace = $templateVars['replace'];
	$templateFile = "admin_blog_editer_article.html";


?>