<?php
	if (!defined("_PROFIL_ACCESS")){header("location:../erreur");}
	## Profil admin
	#
	#	Routage des fonctionnalités du role admin
	#
	#	Attention ! Ce fichier ne doit jamais être édité
	
	$soustitre = "";
	
	
	switch($_SESSION["contexte"]["rubrique"])
	{
		case "accueil":
			$titre = "Accueil";
			$soustitre = "";
			include("admin_accueil.php");
			$_CONTEXTE = "private";
			break;
		
		case "modules":
			$titre = "Modules";
			$soustitre = "";
			include("admin_modules.php");
			$_CONTEXTE = "private";
			break;	
	}
	
	/* inclure le role public */
	if($_CONTEXTE != "private" && $has_public_role)
	{
		include("public.php");
	}
	
	$meta_title = isset($metas[$page_id]["title"])?$metas[$page_id]["title"]:"";
	$meta_keywords = isset($metas[$page_id]["keywords"])?$metas[$page_id]["keywords"]:"";
	$meta_description = isset($metas[$page_id]["description"])?$metas[$page_id]["description"]:"";
	
	$templateDir = "backend/templates/";
	$templateFile = isset($_GET["popin"]) ? "popin.html" : "main_admin.html"; 
	$tmpVars = array("/{SITE_NAME}/i", "/{SITE_BASELINE}/i", "/{CONTENT}/i", "/{MENU_PRINCIPAL}/i", "/{SOUS_MENU}/i", "/{TITRE}/i", "/{SOUSTITRE}/i", "/{INFOS_CONNEXION}/i", "/{REDIRECT}/i","/{META_TITLE}/i", "/{META_KEYWORDS}/i", "/{META_DESCRIPTION}/i", "/{BROADCAST}/i");
	$replace = array(_SITE_NAME, _SITE_BASELINE, $html, $menu_principal, $sous_menu[$_SESSION["contexte"]["rubrique"]], $titre, $soustitre, infos_connexion($menu[$_SESSION["user"]["groupe"]]["item1"]["code"]), $_SERVER["REQUEST_URI"], $meta_title, $meta_keywords, $meta_description, $BROADCAST);
	
	/* Hook injection variables dans layout principal */
	$hook_file= _VHOST_PATH . $_SESSION['app'] . '/hook/main_layout_vars.php';
	if(file_exists($hook_file))
	{
		include($hook_file);
	}
	/* Hook injection variables dans layout principal */
	
	$page = new Page($templateDir, $templateFile, $_SESSION["locale"]);
	$page->replace_tags($tmpVars, $replace);
	echo $page->output();
	exit;
?>