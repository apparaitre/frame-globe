<?php
	define("_PROFIL_ACCESS", "FRAME-GLOBE");
	include('../../' . $_SESSION['app'] . '/includes/functions.php');
	include('../../classes/class.page.php');
	
	$templateDir = "templates/";
	 $_SESSION['locale'] = "FR";
	
	if(!isset($_GET['step']))
	{
		/* Tests droits dossiers et fichier*/
		$files = array('../../backend/templates', '../../app/templates', '../../backend/', '../../app/', '../../app/actions', '../../classes/db', '../../app/cms', '../../includes/db.php', '../../includes/config.php', '../../traductions');
		$content =  "<ul>";
		$err = 0;
		foreach($files as $file)
		{
			$content .=   "<li>" . $file . ": " . perm($file) . " </li>";
			if( perm($file) != "OK"){$err++;}
		}
		$content .=  "</ul>";
		if($err == 0) 
		{
			$content .=  '<a href="install.php?step=config">étape suivante</a>';
		}
		else 
		{
			$content .=  '<br/>Réglez les droits des fichiers et répertoires et <a href="install.php">actualisez la page</a>';
		}
	}
	else
	{
		switch($_GET['step'])
		{
			case "config":
				$host = validInputData($_POST['host'], "string");
				$dbName = validInputData($_POST['dbName'], "string");
				$user = validInputData($_POST['user'], "string");
				$pwd = validInputData($_POST['pwd'], "string");
				$nom_email = validInputData($_POST['nom_email'], "string");
				$email = validInputData($_POST['email'], "string");
				$login = validInputData($_POST['login'], "string");
				$password = validInputData($_POST['password'], "string");

				$url_server = $_SERVER['HTTP_HOST'];
				

				$url = validInputData($_POST['url'], "string", $_SERVER['HTTP_HOST']);
				$dir = validInputData($_POST['dir'], "string", $_POST['dir']);
				$site_name = validInputData($_POST['site_name'], "string");
				$site_baseline = validInputData($_POST['site_baseline'], "string");
				
				$host_err = "";
				$dbName_err = "";
				$user_err = "";
				$pwd_err = "";
				$nom_email_err = "";
				$email_err = "";
				$login_err = "";
				$password_err = "";
				$url_err = "";
				$site_name_err = "";
				$site_baseline_err = "";
				$messageDB = "";
				
				if(isset($_POST['submit']))
				{
					/* Test champs obligatoires */
					$err=0;
					if(strlen($host) == 0) { $err++; $host_err = "<span class=\"obligatoire\">obligatoire</span>";}
					if(strlen($dbName) == 0) { $err++; $dbName_err = "<span class=\"obligatoire\">obligatoire</span>";}
					if(strlen($user) == 0) { $err++; $user_err = "<span class=\"obligatoire\">obligatoire</span>";}
					if(strlen($pwd) == 0) { $err++; $pwd_err = "<span class=\"obligatoire\">obligatoire</span>";}
					if(strlen($nom_email) == 0) { $err++; $nom_email_err = "<span class=\"obligatoire\">obligatoire</span>";}
					if(strlen($email) == 0) { $err++; $email_err = "<span class=\"obligatoire\">obligatoire</span>";}
					if(strlen($login) == 0) { $err++; $login_err = "<span class=\"obligatoire\">obligatoire</span>";}
					if(strlen($password) == 0) { $err++; $password_err = "<span class=\"obligatoire\">obligatoire</span>";}
					if(strlen($url) == 0) { $err++; $url_err = "<span class=\"obligatoire\">obligatoire</span>";}
					if(strlen($site_name) == 0) { $err++; $site_name_err = "<span class=\"obligatoire\">obligatoire</span>";}
					if(strlen($site_baseline) == 0) { $err++; $site_baseline_err = "<span class=\"obligatoire\">obligatoire</span>";}
					
					if($err == 0)
					{
						/* test de connexion à la base de données */
						try {
							$dbh = new PDO('mysql:host=' . $host . ';dbname=' . $dbName . '', $user, $pwd);
							

							/* t00_url */
							$query = "CREATE TABLE IF NOT EXISTS `t00_url` (
  `T00_codeinterne_i` bigint(20) NOT NULL AUTO_INCREMENT,
  `T00_url_va` varchar(255) DEFAULT NULL,
  `T00_role_va` varchar(255) DEFAULT NULL,
  `T00_application_va` varchar(255) DEFAULT NULL,
  `T00_rubrique_va` varchar(255) DEFAULT NULL,
  `T00_page_va` varchar(255) DEFAULT NULL,
  `T00_locale_va` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`T00_codeinterne_i`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;";
							$dbh->query($query);


							/* t01_users */
							$query = "CREATE TABLE IF NOT EXISTS `t01_users` (
  `T01_codeinterne_i` bigint(20) NOT NULL AUTO_INCREMENT,
  `T02_codeinterne_i` bigint(20) NOT NULL,
  `T01_login_va` varchar(255) DEFAULT NULL,
  `T01_password_va` varchar(255) DEFAULT NULL,
  `T01_email_va` varchar(255) DEFAULT NULL,
  `T01_lastlog_d` date DEFAULT NULL,
  `T01_locale_va` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`T01_codeinterne_i`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
							$dbh->query($query);
							
							$result = $dbh->query("select count(*)nb from t01_users where T01_login_va='" . $login . "';");
							
							$row = $result->fetch(PDO::FETCH_ASSOC);
							if($row['nb'] == 0 )
							{
								$query ="insert into t01_users (T02_codeinterne_i, T01_login_va, T01_password_va, T01_email_va, T01_locale_va) values (2, '" . $login . "', '" . md5($password) . "', '" . $email . "', 'FR')"; 
								$dbh->query($query);
							}
							
							/* t02_role */
							$query = "CREATE TABLE IF NOT EXISTS `t02_role` (
  `T02_codeinterne_i` bigint(20) NOT NULL AUTO_INCREMENT,
  `T02_role_va` varchar(255) NOT NULL,
  `T02_application_va` varchar(255) NOT NULL,
  PRIMARY KEY (`T02_codeinterne_i`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
							$dbh->query($query);
							
							$query = "INSERT INTO `t02_role` (`T02_codeinterne_i`, `T02_role_va`, `T02_application_va`) VALUES (1, 'public', 'app'),(2, 'admin', 'backend');";
							$dbh->query($query);
							
							/* t03_profil */
							$query ="CREATE TABLE IF NOT EXISTS `t03_profil` (
  `T03_codeinterne_i` bigint(20) NOT NULL AUTO_INCREMENT,
  `T01_codeinterne_i` bigint(20) NOT NULL,
  `T03_civilite` varchar(255) DEFAULT NULL,
  `T03_prenom` varchar(255) DEFAULT NULL,
  `T03_nom_va` varchar(255) DEFAULT NULL,
  `T03_adresse1_va` varchar(255) NOT NULL,
  `T03_adresse2_va` varchar(255) NOT NULL,
  `T03_codepostal_va` varchar(255) NOT NULL,
  `T03_ville_va` varchar(255) NOT NULL,
  `T03_pays_va` varchar(255) NOT NULL,
  `T03_telephone_va` varchar(255) NOT NULL,
  PRIMARY KEY (`T03_codeinterne_i`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
							$dbh->query($query);
							
							$query = "INSERT INTO `t03_profil` (`T01_codeinterne_i`),(" . $dbh->lastInsertId() . ");";
							$dbh->query($query);
							
							/* t04_dico */
							$query="CREATE TABLE IF NOT EXISTS `t04_dico` (
  `T04_codeinterne_i` bigint(20) NOT NULL AUTO_INCREMENT,
  `T04_section_va` varchar(255) NOT NULL,
  `T04_code_va` varchar(255) NOT NULL,
  `T04_locale_va` varchar(2) NOT NULL,
  `T04_libelle_va` varchar(255) NOT NULL,
  PRIMARY KEY (`T04_codeinterne_i`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
							$dbh->query($query);
							
							$query="INSERT INTO  `t04_dico` (`T04_codeinterne_i` ,`T04_section_va` ,`T04_code_va` ,`T04_locale_va` ,`T04_libelle_va`) VALUES (NULL ,  'role',  'public',  'fr',  'Public'), (NULL ,  'role',  'admin',  'fr',  'Administateur');";
							$dbh->query($query);
							

							/* t05_modules */
							$query="CREATE TABLE IF NOT EXISTS `t05_modules` (
  `T05_codeinterne_i` bigint(20) NOT NULL AUTO_INCREMENT,
  `T05_nom_va` varchar(255) NOT NULL,
  `T05_actif_b` varchar(1) NOT NULL,
  PRIMARY KEY (`T05_codeinterne_i`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
							$dbh->query($query);


							/* t06_variables */
							$query="CREATE TABLE IF NOT EXISTS `t06_variables` (
  `T06_codeinterne_i` bigint(20) NOT NULL AUTO_INCREMENT,
  `T06_name_va` varchar(255) NOT NULL,
  `T06_value_va` varchar(1) NOT NULL,
  PRIMARY KEY (`T06_codeinterne_i`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
							$dbh->query($query);


							/* Modules par défaut */
							/* Rubrique */
							$query="CREATE TABLE IF NOT EXISTS `t100_rubriques` (
  `T100_codeinterne_i` bigint(20) NOT NULL AUTO_INCREMENT,
  `T100_tag_va` varchar(255) NOT NULL,
  `T100_titre_va` varchar(255) DEFAULT NULL,
  `T100_texte_va` text,
  `T100_date_d` datetime DEFAULT NULL,
  PRIMARY KEY (`T100_codeinterne_i`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
							
							$dbh->query($query);
							
							/* article */
							$query="CREATE TABLE IF NOT EXISTS `t101_articles` (
  `T101_codeinterne_i` bigint(20) NOT NULL AUTO_INCREMENT,
  `T100_codeinterne_i` bigint(20) NOT NULL,
  `T101_type_va` varchar(255) NOT NULL,
  `T101_url_va` varchar(255) NOT NULL,
  `T101_titre_va` varchar(255) NOT NULL,
  `T101_chapo_va` text NOT NULL,
  `T101_texte_va` text NOT NULL,
  `T101_date_d` datetime NOT NULL,
  PRIMARY KEY (`T101_codeinterne_i`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
							$dbh->query($query);
							
							
							$dbh = null;
						} catch (PDOException $e) {
							$err++;
							$messageDB = '<div class="error">' . $e->getMessage() . '</div>';
						}
					}
				
					if($err == 0 )
					{
						$config_file = "<?php
		define('_ADMIN_NAME', '"  . $nom_email. "');
		define('_ADMIN_EMAIL', '"  . $email. "');
		define('_VHOST_DIR', '"  . 'http://' . $dir ."');
		define('_VHOST_URL', '"  . 'http://' . $url ."'  . _VHOST_DIR);
		define('_MAX_RESULTS', 10);
		define('_INSCRIPTION_MODE', 'closed');
		\$email_contact = array('debug' => '"  . $email. "');
		define('_APP_MODULE' , '');
		define('_OPTION_GEOLOC', false);
		
		define('_SITE_NAME', '"  . $site_name. "');
		define('_SITE_BASELINE', '"  . $site_baseline. "');
		
		
		\$host = '"  . $host. "';
		\$dbName = '"  . $dbName. "';
		\$user = '"  . $user. "';
		\$pwd = '"  . $pwd. "';
	?>";
						create_file("../../includes/",  "config.php", $config_file, "");
						chmod("../../includes/config.php", 0777);
						header('location:install.php?step=finalize');
					}
				}
				$templateFile = "form_config.html";
				$tmpVars = array("/{host}/i", "/{dbName}/i", "/{user}/i", "/{pwd}/i", "/{nom_email}/i", "/{email}/i","/{login}/i", "/{password}/i", "/{url}/i", "/{SITE_NAME}/i", "/{SITE_BASELINE}/i", "/{host_err}/i", "/{dbName_err}/i", "/{user_err}/i", "/{pwd_err}/i", "/{nom_email_err}/i", "/{email_err}/i","/{login_err}/i", "/{password_err}/i", "/{url_err}/i", "/{site_name_err}/i", "/{site_baseline_err}/i", "/{MESSAGEDB}/i");
				$replace = array($host, $dbName, $user, $pwd, $nom_email, $email, $login, $password, $url, $site_name, $site_baseline, $host_err, $dbName_err, $user_err, $pwd_err, $nom_email_err, $email_err, $login_err, $password_err, $url_err, $site_name_err, $site_baseline_err, $messageDB);
				$page = new Page($templateDir, $templateFile, $_SESSION['locale']);
				$page->replace_tags($tmpVars, $replace);
				$content =  $page->output();
				break;
				
			case "finalize":
				$content =  "you Rock!
				<p>Etapes suivantes:</p>
				<ol>
					<li>Créer le squelette de l'application: <a href=\"application.xml\" target=\"_blank\">/frameworks/application.xml</a></li>
					<li><a href=\"generer_application.php\">Générer les fichiers</a></li>
					<li>Concevoir le modèle de données avec un outil comme mysql admin (<a href=\"doc/conventions_db.html\" target=\"_blank\">Convention de nommage des tables et des champs</a>)</li>
					<li><a href=\"generer_classes.php\">générer les classes de la base de données</a></li>
					<li>Commencer à développer l'application</li>
				</ol>";
				
				
				break;
			
		}
	}
	
	
	/* Infos base de données */
	
	
	/* Test d'accès à la base de données */
	
	
	/*
		Création des tables et requêtes de base 
		T01_users
		T02_roles
		T03_profils
		T04_dico
		
	*/
	
	$templateFile = "main.html"; 
	$tmpVars = array("/{CONTENT}/i");
	$replace = array($content);
	$page = new Page($templateDir, $templateFile, $_SESSION['locale']);
	$page->replace_tags($tmpVars, $replace);
	echo $page->output();

	function perm($filename)
	{
		if (is_writable($filename)) {
			return 'OK';
		} else {
			return 'KO!';
		}
	}
?>