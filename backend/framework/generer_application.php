<?php
	session_start();
	define('_PROFIL_ACCESS', "FRAME-GLOBE");
	
	include ('../../includes/init.php');
	require_once "../../classes/XLS/class.writeexcel_workbook.inc.php";
	require_once "../../classes/XLS/class.writeexcel_worksheet.inc.php";
	require_once "../../classes/class.page.php";

	$page = new Page();

	$applications = array();
	$fname = "docs/pilotage_application." . date("Y-m-d") . ".xls";
	$workbook = new writeexcel_workbook($fname);
	$worksheet = $workbook->addworksheet();
	
	$XLS_line = 0;
	
	$worksheet->set_column(0, 0, 11);
	$worksheet->set_column(1, 1, 58);
	$worksheet->set_column(2, 2, 11	);
	$worksheet->set_column(3, 3, 64);
	$worksheet->set_column(4, 4, 7);
	$worksheet->set_column(5, 4, 7);
	$worksheet->set_column(6, 4, 7);
	$worksheet->set_column(7, 5, 60);
	
	$heading  =& $workbook->addformat(array(
                                        bold    => 1,
                                        color   => 'black',
                                        size    => 10,
                                        merge   => 6,
                                        ));
										
	$caption_format =& $workbook->addformat(array(
                                            bold    => 1,
                                            color   => 'black',
                                            size    => 9,
                                            font    => 'Verdana'
                                        ));
										
	$headings = array('Etat d\'avancement', '');
	$worksheet->write_row(0, $XLS_line, $headings, $heading);

	$XLS_line++;
	$XLS_line++;

	$worksheet->write($XLS_line, 0,  utf8_decode("Rôle"), $caption_format);
	$worksheet->write($XLS_line, 1,  utf8_decode("Fonction"), $caption_format);
	$worksheet->write($XLS_line, 2,  utf8_decode("Type"), $caption_format);
	$worksheet->write($XLS_line, 3,  utf8_decode("Description"), $caption_format);
	$worksheet->write($XLS_line, 4,  utf8_decode("Proto"), $caption_format);
	$worksheet->write($XLS_line, 5,  utf8_decode("App"), $caption_format);
	$worksheet->write($XLS_line, 6,  utf8_decode("Design"), $caption_format);
	$worksheet->write($XLS_line, 7,  utf8_decode("Commentaire"), $caption_format);
	
	

	
	
	
	$_CONTENT = "<h1>Génération du squelette de l'application</h1>";
	$application = new DomDocument();
	
	if(isset($_GET['app']) && is_file(_BACKEND_PATH . "/framework/" . $_GET['app'] . ".xml"))
	{
		$app_file = $_GET['app'] . ".xml";
	}
	else
	{
		$app_file = "application.xml";
	}
	
	$application->load($app_file);

	$params = $application->getElementsByTagName('param');
	if(count($params) > 0)
	{
		foreach ($params as $param)
		{
			$name = $param->getAttribute('name');
			$$name = $param->getAttribute('value');
		}
	}
	
	$roles = $application->getElementsByTagName('role');
	
	
	/* debut ROLES */
	if(count($roles) > 0)
	{
		$_CONTENT .= "<ul>";
		
		/* On cherche le role public */
		foreach ($roles as $role)
		{
			$FW_ROLE = $role->getAttribute('name');
			$has_public_ui[$FW_ROLE] = $role->getAttribute('public');
			if($role->getAttribute('public') == "true")
			{
				$_PUBLIC_ROLE = $FW_ROLE;
				$message_public = "Le rôle public est <b>" . $_PUBLIC_ROLE ."</b>";
			}
		}
		/* S'il n'y en a pas ou a été oublié on sélectionne le premier role trouvé comme public */
		if(!isset($_PUBLIC_ROLE))
		{
			$_PUBLIC_ROLE = $roles->item(0)->getAttribute('name');
			$message_public = "<span style=\"font-size:1.5em;color:red\">Attention! aucun rôle public n'a été trouvé dans votre fichier <b>{$app_file}</b>.
			<br/>Le rôle suivant a été sélectionné : <b>" . $_PUBLIC_ROLE . "</b> mais attention il se peut que cela ne soit pas le bon
			<br/>Vous devez rajouter l'attribut public=\"true\" sur le role public de <b>{$app_file}</b></span>";
		}
		foreach ($roles as $role)
		{
			$FW_ROLE = $role->getAttribute('name');
			$FW_APPLICATION = $role->getAttribute('application');
			

			/* création du répertoire actions et des répertoires enfants */
			$dir_actions = "../../" . $FW_APPLICATION . "/actions";
			if (!is_dir($dir_actions)) {
				mkdir($dir_actions);         
			}

			$dir_role = "../../" . $FW_APPLICATION . "/actions/" . $FW_ROLE;
			
			if (!is_dir($dir_role)) {
				mkdir($dir_role);         
			}
			
			if(!in_array($FW_APPLICATION, $applications))
			{
				$applications[] = $FW_APPLICATION;
			}
			$MENU_ITEMS[$FW_APPLICATION][$FW_ROLE]['menu_principal'] = $role->getAttribute('menu_principal');
			$MENU_ITEMS[$FW_APPLICATION][$FW_ROLE]['code'] .= '';
			
			$_CONTENT .= "<li><h2>Application : "  . $FW_APPLICATION . " / ROLE : " . $FW_ROLE . " / PUBLIC: " . $_PUBLIC_ROLE . "</h2></li>";
			$_CONTENT .= "<li>" . $message_public ."</li>";
			
			
			
			
/* 
	DEBUT SECTION PREMIER NIVEAU FRAGMENT 1

	{FW_APPLICATION}/{FW_ROLE}.php

*/			
			$ROLE_FILE = '<?php
	if (!defined("_PROFIL_ACCESS")){header("location:../erreur");}
	## Profil ' . $FW_ROLE . '
	#
	#	Routage des fonctionnalités du role ' . $FW_ROLE . '
	#
	#	Attention ! Ce fichier ne doit jamais être édité
	
	$soustitre = "";
	
	
	switch($_SESSION["contexte"]["rubrique"])
	{
		';
		if($FW_ROLE == $_PUBLIC_ROLE)
					{
						$ROLE_FILE .= 'default:
						';
					}
					
/* FIN SECTION PREMIER NIVEAU FRAGMENT 1*/						
			
			
			$rubriques = $role->getElementsByTagName('rubrique');
			/* debut RUBRIQUES */
			if(count($rubriques)>0)
			{
				$_CONTENT .= "<ul>";
				
/* DEBUT MENU ITEMS FRAGMENT 1 
	{FW_APPLICATION}/{FW_ROLE}.php
	*/	
				$MENU_ITEMS[$FW_APPLICATION][$FW_ROLE]['code'] .= 'array (';

/* FIN MENU ITEMS FRAGMENT 1 */	

				$k_item = 1;
				
				/* debut foreach RUBRIQUES */
				foreach ($rubriques as $rubrique)
				{
					$FW_RUBRIQUE = $rubrique->getAttribute('name');
					$FW_TITRE = $rubrique->getAttribute('titre');
					$RUB_HIDDEN = "";
					$RUB_HIDDEN = $rubrique->getAttribute('hidden');
					$_CONTENT .= "<li><h3>" . $FW_RUBRIQUE . "</h3></li>";
					
					/* DEBUT remplissages URL Rubriques */
					
					$url = $T00->getUrl($FW_RUBRIQUE, "", $FW_APPLICATION);
					if(empty($url))
					{
						$T00->insert($FW_RUBRIQUE, $FW_RUBRIQUE, $FW_ROLE, $FW_APPLICATION, $_SESSION['locale']);
					}
					
					/* FIN remplissages URL Rubriques */
					
					
/* DEBUT MENU ITEMS FRAGMENT 2 */	
					
					if($RUB_HIDDEN != "true")
					{	
						$MENU_ITEMS[$FW_APPLICATION][$FW_ROLE]['code'] .= '
											"item' . $k_item . '"	=> array( 
																"code" => "' . $FW_RUBRIQUE . '",';
																
						if($rubrique->getAttribute('home'))
						{
							$MENU_ITEMS[$FW_APPLICATION][$FW_ROLE]['code'] .= '
																"home" => true,';
						}									
						$MENU_ITEMS[$FW_APPLICATION][$FW_ROLE]['code'] .= '
																"class" => "", 
																"libelle" => $page->_T("libMenu' . ucfirst(strtolower($FW_RUBRIQUE)) . '")';
						$MENU_LANG .= "libMenu" . ucfirst(strtolower($FW_RUBRIQUE)) ."=" . $FW_TITRE . "\n";
					}
					/* Recherche sousnav */										
					$pages = $rubrique->getElementsByTagName('page');
					if(count($pages)>0)
					{
						$sousmenu = "";
						$k2_item = 1;
						foreach ($pages as $page)
						{
							if($page->getAttribute('sousmenu')=="true")
							{
								$FW_PAGE = $page->getAttribute('name');
								$sousmenu .= '													"item' . $k_item . '-' . $k2_item . '" => array( "code" => "' . $FW_PAGE . '", "class" => "", "libelle" => $page->_T("libMenu' . ucfirst(strtolower($FW_RUBRIQUE)) . ucfirst(strtolower($FW_PAGE)) . '")),
								';
								$MENU_LANG .= "libMenu" . ucfirst(strtolower($FW_RUBRIQUE)) . ucfirst(strtolower($FW_PAGE))  ."=" . $page->getAttribute('titre') . "\n";
								$k2_item++;
							}
							
							/* DEBUT remplissages URL Rubriques */
							$url = $T00->getUrl($FW_RUBRIQUE, $page->getAttribute('name'), $FW_APPLICATION);
							if(empty($url))
							{
								$T00->insert($FW_RUBRIQUE . "_" . $page->getAttribute('name'), $FW_RUBRIQUE, $page->getAttribute('name'), $FW_APPLICATION, $_SESSION['locale']);
							}
							/* FIN remplissages URL Rubriques */
						}
					
						
						if(count($sousmenu) > 0 && $RUB_HIDDEN != "true")
						{
							$sousmenu = rtrim($sousmenu, ",");
							$MENU_ITEMS[$FW_APPLICATION][$FW_ROLE]['code'] .= ',
																"sousmenu" => array(
							' . $sousmenu . '						
															)';
						}
					}
					if($RUB_HIDDEN != "true")
					{
						$MENU_ITEMS[$FW_APPLICATION][$FW_ROLE]['code'] .= '
													),';
					}
					
					$k_item++;
					
/* FIN MENU ITEMS FRAGMENT 2 */	

/* DEBUT SECTION PREMIER NIVEAU FRAGMENT 2 */						
					
					$ROLE_FILE .= '
		case "' . $FW_RUBRIQUE . '":
			$titre = "'  . $FW_TITRE . '";
			$soustitre = "";
			';
			
					if($FW_ROLE == $_PUBLIC_ROLE)
					{
						$ROLE_FILE .= '$page_id = "' . $FW_RUBRIQUE . '";
			';
					}
			$ROLE_FILE .= 'include("' . $FW_ROLE . '_' . $FW_RUBRIQUE . '.php");
			';
				if($FW_ROLE != $_PUBLIC_ROLE)
				{			
					$ROLE_FILE .= '$_CONTEXTE = "private";';
				}
			$ROLE_FILE .= '
			break;
			';
/* FIN SECTION PREMIER NIVEAU FRAGMENT 2 */	

/* 
	DEBUT SECTION DEUXIEME NIVEAU FRAGMENT 1 
	{FW_APPLICATION}/{FW_ROLE}.php
*/	
					$RUBRIQUE_FILE = '<?php
	if (!defined("_PROFIL_ACCESS")){header("location:../erreur");}
	
	//switch($_SESSION["contexte"][$_SESSION["contexte"]["rubrique"]]["page"])
	//$page = isset($_GET["page"])?$_GET["page"]:"";
	switch($_PAGE)
	{
		default:
		';
/* FIN SECTION DEUXIEME NIVEAU FRAGMENT 1 */	
					
					$pages = $rubrique->getElementsByTagName('page');

					if(count($pages) > 0)
					{
						
						$_CONTENT .= "<ul>";
						/* debut foreach FONCTIONS */
						foreach ($pages as $page) 
						{
							$FW_PAGE = $page->getAttribute('name');
							if( $page->getAttribute('case') != "")
							{
								$FW_PAGE_DEFAULT = "<small>[" . $page->getAttribute('case') ."]</small>";
							}
							else
							{
								$FW_PAGE_DEFAULT = "";
							}
						
							$_CONTENT .= "<li>page " . $FW_PAGE . " " . $FW_PAGE_DEFAULT . " </li>";

/* DEBUT SECTION DEUXIEME NIVEAU FRAGMENT 2 */								
							$RUBRIQUE_FILE .='
		case "' . $FW_PAGE . '":
			include("actions/' . $FW_ROLE. '/' . $FW_RUBRIQUE. '_' . $FW_PAGE. '.php");
			break;
			';
/* FIN SECTION DEUXIEME NIVEAU FRAGMENT 2 */

/* 
	DEBUT SECTION TROISIEME NIVEAU FRAGMENT 1 
	{FW_APPLICATION}/{FW_ROLE}.php
*/		

							$description = $page->getElementsByTagName('description')->item(0);
							$FW_DESCRIPTION = $description->textContent;
							
							$FONCTION_FILE = '<?php
$page_name="";
$page = new Page();
$soustitre = $page->_T("libSoustitre");
$description = $page->_T("libDescription");
$content = ""; 

$templateFile = "' . $FW_ROLE. '_' . $FW_RUBRIQUE. '_' . $FW_PAGE. '.html";
$tmpVars = array("/{CONTENT}/i");
$replace = array($content);	
$dynamic_vars = array();

?>';

/* FIN SECTION TROISIEME NIVEAU FRAGMENT 1 */

							$file = "../../" . $FW_APPLICATION . "/actions/" . $FW_ROLE. "/" . $FW_RUBRIQUE. "_" . $FW_PAGE. ".php";

							if (!file_exists($file)) 
							{
								$fp = fopen($file, "w");
								fwrite($fp, $FONCTION_FILE);
								fclose($fp);
							}
							
							/* Ecriture fichier excell pilotage */
							
							$XLS_line++;
							$worksheet->write($XLS_line, 0,  utf8_decode($FW_ROLE));
							$worksheet->write($XLS_line, 1,  utf8_decode($FW_RUBRIQUE . "/" . $FW_PAGE));
							$worksheet->write($XLS_line, 2,  "Fonction");
							
							$worksheet->write($XLS_line, 3, utf8_decode($FW_DESCRIPTION));
							$worksheet->write($XLS_line, 4,  "");
							$worksheet->write($XLS_line, 5,  "");
							$worksheet->write($XLS_line, 6,  "");
							$worksheet->write($XLS_line, 7,  "");
							
							
							/* DÉBUT MODULES */
							$modules = $page->getElementsByTagName('module');
							if(count($modules) > 0)
							{
								$TEMPLATE_FILE = "";
								foreach($modules as $module)
								{
									$_CONTENT .= " [Module " . $module->getAttribute('type') . " => " .$module->getAttribute('name') . "]<br/>";
										
										/*
										 * modules
										 * Insertion du hook module generer_pallication
										 */
										
										$module_hook = "generer_application";
										
							    		if(is_file(_MODULE_PATH . "/" . $module->getAttribute('type') . "/module.php"))
							   			{
							   				include(_MODULE_PATH . "/" . $module->getAttribute('type') . "/module.php");
							   			}
									
									$XLS_line++;
									$worksheet->write($XLS_line, 0,  utf8_decode($FW_ROLE));
									$worksheet->write($XLS_line, 1,  utf8_decode($FW_RUBRIQUE . "/" . $FW_PAGE ." module[" . $module->getAttribute('type') ."]" ));
									
									$worksheet->write($XLS_line, 2,  "Module");
									$worksheet->write($XLS_line, 3, utf8_decode($module->getAttribute('name') . "/" . $module->getAttribute('rubriqueTag')));
									$worksheet->write($XLS_line, 4,  "");
									$worksheet->write($XLS_line, 5,  "");
								}
								
								$file = "../../" . $FW_APPLICATION . "/templates/" .$FW_ROLE. "_" . $FW_RUBRIQUE. "_" . $FW_PAGE. ".html";
								$modele = "modeles/page.html";
								
								if (!file_exists($file)) {
									$fp = fopen($file, "w");
									
									if (file_exists($modele)) {
										/* si un modèle pour la page existe */
										$fmod = fopen($modele, "rb");
										$content = '';
										while (!feof($fmod)) {
											$content .= fread($fmod, 8192);
										}
										fclose($fmod);
										
										$content = str_replace("{MODULES}", $TEMPLATE_FILE, $content);
										
										fwrite($fp,$content);

									}
									else
									{
										fwrite($fp, "{CONTENT}\n");
										fwrite($fp, $TEMPLATE_FILE);
									}
									fclose($fp);
								}
							}	/* FIN MODULES */
						}
						
						/* fin foreach FONCTIONS */
						
						$_CONTENT .= "</ul>";
						
					} /* fin  FONCTIONS */
					
				
/* DEBUT SECTION DEUXIEME NIVEAU FRAGMENT 2 */							
				$RUBRIQUE_FILE .= '
	}
	
	$page = new Page($templateDir, $templateFile,  $_SESSION["locale"]);
	$page->setVars($dynamic_vars);
	
	$page->replace_tags($tmpVars, $replace);
	$html = $page->output();
	
?>';
					/* FIN SECTION DEUXIEME NIVEAU FRAGMENT 2 */
					/* fin foreach RUBRIQUES */
					$_CONTENT .= "</ul>";
				
					$file = "../../" . $FW_APPLICATION . "/" . $FW_ROLE . "_" . $FW_RUBRIQUE . ".php";
					if(!file_exists($file) || is_writable($file))
					{
						$fp = fopen($file, "w");
						fwrite($fp, $RUBRIQUE_FILE);
						fclose($fp);
					}
					else
					{
						$_CONTENT .= "<div class=\"error\">Accès en écriture impossible à " . $FW_APPLICATION . "/" . $FW_ROLE . "_" . $FW_RUBRIQUE . ".php</div>";
					}
					
				}
				
				/* fin foreach RUBRIQUES */
				
			} 
			/* fin RUBRIQUES */
			
			
			/* DEBUT MENU ITEMS FRAGMENT 3 */				
			$MENU_ITEMS[$FW_APPLICATION][$FW_ROLE]['code'] = rtrim($MENU_ITEMS[$FW_APPLICATION][$FW_ROLE]['code'], ",");
			$MENU_ITEMS[$FW_APPLICATION][$FW_ROLE]['code'] .= '
					),';
	
/* FIN MENU ITEMS FRAGMENT 3 */

/* DEBUT SECTION PREMIER NIVEAU FRAGMENT 3 */	
			$ROLE_FILE .= '
	}
	';
	
	if(isset($_PUBLIC_ROLE) && $FW_ROLE != $_PUBLIC_ROLE)
	{
		$ROLE_FILE .='
	/* inclure le role public */
	if($_CONTEXTE != "private" && $has_public_role)
	{
		include("' . $_PUBLIC_ROLE . '.php");
	}
	';
	}
	$ROLE_FILE .= '
	$meta_title = isset($metas[$page_id]["title"])?$metas[$page_id]["title"]:"";
	$meta_keywords = isset($metas[$page_id]["keywords"])?$metas[$page_id]["keywords"]:"";
	$meta_description = isset($metas[$page_id]["description"])?$metas[$page_id]["description"]:"";
	
	$templateDir = "' . $FW_APPLICATION . '/templates/";
	$templateFile = isset($_GET["popin"]) ? "popin.html" : "main_' . $FW_ROLE . '.html"; 
	$tmpVars = array("/{SITE_NAME}/i", "/{SITE_BASELINE}/i", "/{CONTENT}/i", "/{MENU_PRINCIPAL}/i", "/{SOUS_MENU}/i", "/{TITRE}/i", "/{SOUSTITRE}/i", "/{INFOS_CONNEXION}/i", "/{REDIRECT}/i","/{META_TITLE}/i", "/{META_KEYWORDS}/i", "/{META_DESCRIPTION}/i", "/{BROADCAST}/i");
	$replace = array(_SITE_NAME, _SITE_BASELINE, $html, $menu_principal, $sous_menu[$_SESSION["contexte"]["rubrique"]], $titre, $soustitre, infos_connexion($menu[$_SESSION["user"]["groupe"]]["item1"]["code"]), $_SERVER["REQUEST_URI"], $meta_title, $meta_keywords, $meta_description, $BROADCAST);
	
	/* Hook injection variables dans layout principal */
	$hook_file= _VHOST_PATH . $_SESSION[\'app\'] . \'/hook/main_layout_vars.php\';
	if(file_exists($hook_file))
	{
		include($hook_file);
	}
	/* Hook injection variables dans layout principal */
	
	$page = new Page($templateDir, $templateFile, $_SESSION["locale"]);
	$page->replace_tags($tmpVars, $replace);
	echo $page->output();
	exit;
?>';

/* FIN SECTION PREMIER NIVEAU FRAGMENT 3 */					
			$file = "../../" . $FW_APPLICATION . "/" . $FW_ROLE . ".php";	
			if(!file_exists($file) || is_writable($file))
			{
				$fp = fopen($file, "w");
				fwrite($fp, $ROLE_FILE);
				fclose($fp);
			}
			else
			{
				$_CONTENT .= "<div class=\"error\">Accès en écriture impossible à " . $FW_APPLICATION . "/" . $FW_ROLE  . ".php</div>";
			}
			$_CONTENT .= "</ul>";
			


/* 
 *	Generate main layout 
 */

			$MAIN_FILE = '<!DOCTYPE html>
<html>
	<head> 
		{#INCLURE=metas}
		{#INCLURE=styles}
		{#INCLURE=scripts}
	</head>
	<body class="{UNIVERS}" role="document">
		<header class="page" role="banner">
			<div id="connect_bar">
				<ul class="table w960p">
					<li class="pa1 w50 left"></li>
					<li class="pa1 w50 right">
					{INFOS_CONNEXION}
					</li>
				</ul>
			</div>
			{#INCLURE=header}
		</header>
		<section id="page"  role="main" class="pa3">
			{CONTENT}
			<br class="nettoyeur" />
		</section>
		<footer class="page">
			{#INCLURE=footer}
		</footer>
		<div id="qtip-growl-container"></div>
	</body>
</html>';

			$file = "../../" . $FW_APPLICATION . "/templates/main_" . $FW_ROLE . ".html";	
			if(!file_exists($file))
			{
				$fp = fopen($file, "w");
				fwrite($fp, $MAIN_FILE);
				fclose($fp);
			}
	
		
		
		} /* fin foreach ROLES */
		
		
		/* Génération du menu principal automatique */
		
		//$MENU_ITEMS[$FW_APPLICATION] = rtrim($MENU_ITEMS[$FW_APPLICATION], ",");
		foreach($applications as $application)
		{
		
			$MENU = '<?php
	$page = new Page();
	if (!defined("_PROFIL_ACCESS")){header("location:../erreur");}
	 ';
	 
			if($role->getAttribute('menu_principal') != "")
			{
				 $public_role = $has_public_ui[$FW_ROLE] == "false" ? "" :  $role->getAttribute('menu_principal');
				 $MENU .= '$public_role = "' . $public_role . '";
				 ';
			}

			$MENU .= '$menu = array (
						';
			foreach ($roles as $role)
			{
				$FW_APPLICATION = $role->getAttribute('application');
				if($FW_APPLICATION == $application)
				{
					$FW_ROLE = $role->getAttribute('name');
					$MENU .= "
						'" . $FW_ROLE . "' => " . $MENU_ITEMS[$FW_APPLICATION][$FW_ROLE]['code'];
				}
			}	
			$MENU .= '
		);
				
		$make_menu = function($user_groupe)
		{
			global $public_role, $menu;
			if(isset($user_groupe))
			{
			';
			
		foreach ($roles as $role)
		{
			if($role->getAttribute('menu_principal') != "")
			{
				$MENU .= 	'				$menu_route["' . $role->getAttribute('name') . '"] = "' . $role->getAttribute('menu_principal') . '";' . "\n";
			}
			else
			{
				$MENU .= 	'				$menu_route["' . $role->getAttribute('name') . '"] = "local";' . "\n";
			}
		}
		$MENU .= 	'
				if($menu_route[$user_groupe] == "local")
				{
					$role =  $user_groupe;
					$public = "";
				}
				else
				{';

			if($has_public_ui[$FW_ROLE] == "false")
			{
			
				$MENU .= '
					$role =  $user_groupe;
					$public = "";';
					
			}
			else
			{
				$MENU .= 'if($public_role != "")
				{
					$role = $public_role ;
					$public = $public_role;
				}
				else
				{
					$role =  $user_groupe;
					$public = "";
				}';
			}
			$MENU .= '}';
			$MENU .= '
			$menu_role = $menu[$role];
			if(count($menu_role) > 0)
			{
				$menu_principal = "<nav id=\"menu\"><ul class=\"menu\">";
				foreach ($menu_role as $item)
				{
					$rubrique = $item["code"];
					$active = $rubrique == $_SESSION["contexte"]["rubrique"] ? "on" : "off";
					$class = $item["class"] . " " . $active;
					
					$rubrique_url = new Url();
					$rubrique_url->setType($_SESSION["type_url"]);
					/*
					if($public_role != "" && $_SESSION["user"]["groupe"] != $public_role)
					{
						$rubrique_url->setParam("role", $public_role);
					}
					*/
					$item_url = $rubrique_url->getUrl($rubrique.",");
					
					$menu_principal .= "<li class=\"menu " . $item["code"] . " " . $class . "\"><a href=\"" . $item_url . "\" ><span class=\"lib\">" . $item["libelle"] . "</span></a>";
					if(count($item["sousmenu"]) > 0)
					{
						$sous_menu[$rubrique] .= "<ul class=\"sousmenu\">";
						foreach ($item["sousmenu"] as $ssitem)
						{
							$ssitem_url = new Url();
							$ssitem_url->setType($_SESSION["type_url"]);
							/*
							if($public_role != "" && $_SESSION["user"]["groupe"] != $public_role)
							{
								$role = $public_role ."@";
							}
							*/
							$page_url = $ssitem_url->getUrl($rubrique . "," . $ssitem["code"]);
							
							$ssactive = $ssitem["code"] == $_SESSION["contexte"][$_SESSION["contexte"]["rubrique"]]["page"] ? "on" : "off";
							$class = $ssitem["class"] . " " . $ssactive;
							$sous_menu[$rubrique] .= "<li class=\"" . $ssitem["code"] . " " . $class . "\"><a href=\"" . $page_url . "\" ><span class=\"lib\">" . $ssitem["libelle"] . "</span></a></li>";
							
						}
					
						$sous_menu[$rubrique] .= "</ul>";
					}
					$menu_principal .= $sous_menu[$rubrique] . "</li>";
				}
				$menu_principal .= "</ul></nav>";
			}
		}
		return $menu_principal;
	}
?>';
			
			$fp = fopen("../../" . $application . "/menu_principal.php", "w");
			fwrite($fp, $MENU);
			fclose($fp);
			
		}
			
	}/* fin ROLES */
	
	$workbook->close();
	$fh = fopen($fname, "rb");

	/* Fichiers de langues */
	$fp_menu = fopen("../../traductions/menu.FR.inc", "w");
	fwrite($fp_menu, $MENU_LANG);
	fclose($fp_menu);


	$templateDir = "templates/";
	$templateFile = "main.html"; 
	$tmpVars = array("/{CONTENT}/i");
	$replace = array($_CONTENT);
	$page = new Page($templateDir, $templateFile, $_SESSION['locale']);
	$page->replace_tags($tmpVars, $replace);
	echo $page->output();
?>