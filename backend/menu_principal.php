<?php
	$page = new Page();
	if (!defined("_PROFIL_ACCESS")){header("location:../erreur");}
	 $menu = array (
						
						'admin' => array (
											"item1"	=> array( 
																"code" => "accueil",
																"class" => "", 
																"libelle" => $page->_T("libMenuAccueil"),
																"sousmenu" => array(
													
															)
													),
											"item2"	=> array( 
																"code" => "utilisateurs",
																"class" => "", 
																"libelle" => $page->_T("libMenuUtilisateurs"),
																"sousmenu" => array(
													
															)
													),
											"item3"	=> array( 
																"code" => "blog",
																"class" => "", 
																"libelle" => $page->_T("libMenuBlog"),
																"sousmenu" => array(
													
															)
													),
											"item4"	=> array( 
																"code" => "cms",
																"class" => "", 
																"libelle" => $page->_T("libMenuCms"),
																"sousmenu" => array(
													
															)
													)
					),
		);
				
		$make_menu = function($user_groupe)
		{
			global $public_role, $menu;
			if(isset($user_groupe))
			{
							$menu_route["admin"] = "local";

				if($menu_route[$user_groupe] == "local")
				{
					$role =  $user_groupe;
					$public = "";
				}
				else
				{if($public_role != "")
				{
					$role = $public_role ;
					$public = $public_role;
				}
				else
				{
					$role = $user_groupe;
					$public = "";
				}}
			$menu_role = $menu[$role];
			if(count($menu_role) > 0)
			{
				$menu_principal = "<nav id=\"menu\"><ul class=\"menu\">";
				foreach ($menu_role as $item)
				{
					$rubrique = $item["code"];
					$active = $rubrique == $_SESSION["contexte"]["rubrique"] ? "on" : "off";
					$class = $item["class"] . " " . $active;
					
					$rubrique_url = new Url();
					$rubrique_url->setType($_SESSION["type_url"]);
					/*
					if($public_role != "" && $_SESSION["user"]["groupe"] != $public_role)
					{
						$rubrique_url->setParam("role", $public_role);
					}
					*/
					$item_url = $rubrique_url->getUrl($rubrique.",");
					
					$menu_principal .= "<li class=\"menu " . $item["code"] . " " . $class . "\"><a href=\"" . $item_url . "\" ><span class=\"lib\">" . $item["libelle"] . "</span></a>";
					if(count($item["sousmenu"]) > 0)
					{
						$sous_menu[$rubrique] .= "<ul class=\"sousmenu\">";
						foreach ($item["sousmenu"] as $ssitem)
						{
							$ssitem_url = new Url();
							$ssitem_url->setType($_SESSION["type_url"]);
							/*
							if($public_role != "" && $_SESSION["user"]["groupe"] != $public_role)
							{
								$role = $public_role ."@";
							}
							*/
							$page_url = $ssitem_url->getUrl($rubrique . "," . $ssitem["code"]);
							
							$ssactive = $ssitem["code"] == $_SESSION["contexte"][$_SESSION["contexte"]["rubrique"]]["page"] ? "on" : "off";
							$class = $ssitem["class"] . " " . $ssactive;
							$sous_menu[$rubrique] .= "<li class=\"" . $ssitem["code"] . " " . $class . "\"><a href=\"" . $page_url . "\" ><span class=\"lib\">" . $ssitem["libelle"] . "</span></a></li>";
							
						}
					
						$sous_menu[$rubrique] .= "</ul>";
					}
					$menu_principal .= $sous_menu[$rubrique] . "</li>";
				}
				$menu_principal .= "</ul></nav>";
			}
		}
		return $menu_principal;
	}
?>