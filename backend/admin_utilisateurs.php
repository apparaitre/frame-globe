<?php
	if (!defined("_PROFIL_ACCESS")){header("location:../erreur");}
	
	//switch($_SESSION["contexte"][$_SESSION["contexte"]["rubrique"]]["page"])
	//$page = isset($_GET["page"])?$_GET["page"]:"";
	switch($_PAGE)
	{
		default:
		
		case "liste":
			include("actions/admin/utilisateurs_liste.php");
			break;
			
		case "ajouter_utilisateur":
			include("actions/admin/utilisateurs_ajouter_utilisateur.php");
			break;
			
		case "editer_utilisateur":
			include("actions/admin/utilisateurs_editer_utilisateur.php");
			break;
			
		case "supprimer_utilisateur":
			include("actions/admin/utilisateurs_supprimer_utilisateur.php");
			break;
			
	}
	
	$page = new Page($templateDir, $templateFile,  $_SESSION["locale"]);
	$page->setVars($dynamic_vars);
	
	$page->replace_tags($tmpVars, $replace);
	$html = $page->output();
	
?>