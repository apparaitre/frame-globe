$(document).ready(function(){
   
    $('.fg_updateField').on('click', function(){
        console.log('click');
        table = $(this).data('table');
        field = $(this).data('field');
        codeinterne = $(this).data('codeinterne');
        value = $(this).data('value');

        $.get('ajax/updateTableField.php', {'table': table, 'field': field, 'codeinterne': codeinterne, 'value': value}, function(data){
            console.log(data);
        });
    })
});

window.broadcast = function(titre, texte) {
    console.log(titre);
    var target = $('.qtip.jgrowl:visible:last');

    $('<div/>').qtip({
        content: {
            text: texte,
            title: {
                text: titre,
                button: true
            }
        },
        position: {
            target: [0,0],
            container: $('#qtip-growl-container')
        },
        show: {
            event: false,
            ready: true,
            effect: function() {
                $(this).stop(0, 1).animate({ height: 'toggle' }, 400, 'swing');
            },
            delay: 0,
            persistent: texte
        },
        hide: {
            event: false,
            effect: function(api) {
                $(this).stop(0, 1).animate({ height: 'toggle' }, 400, 'swing');
            }
        },
        style: {
            width: 250,
            classes: 'jgrowl',
            tip: false
        },
        events: {
            render: function(event, api) {
                if(!api.options.show.texte) {
                    $(this).bind('mouseover mouseout', function(e) {
                        var lifespan = 5000;
                        clearTimeout(api.timer);
                        if (e.type !== 'mouseover') {
                            api.timer = setTimeout(function() { api.hide(e) }, lifespan);
                        }
                    })
                    .triggerHandler('mouseout');
                }
            }
        }
    });
}