<?php
    session_start();
	if (!defined("_PROFIL_ACCESS")){header('location:erreur');}
	$time_start = microtime(true);
	
	
	
	
	/* URL */
	$_SESSION['contexte'] = isset($_SESSION['contexte']) ? $_SESSION['contexte'] : "";
	$_SESSION['contexte']['rubrique'] = isset($_SESSION['contexte']['rubrique']) ? $_SESSION['contexte']['rubrique'] : "";
	$_SESSION['contexte']['rubrique'] = isset($_REQUEST['rubrique']) ? $_REQUEST['rubrique'] : $_SESSION['contexte']['rubrique'];
	
	$_PAGE = isset($_REQUEST["page"]) ? $_REQUEST["page"] : "";
	

	/* initialisations et includes */
	include ('includes/init.php');
	

	/* 
		rechercher l'url dans T00_url et déterminer de quelle application il est question
		si  le role de l'application correspondant est disponible pour $_SESSION['user']['groupe']
		alors on l'affiche.
		Si l'application est publique alors on affiche
		s'il y a conflit d'URL on affiche l'url de l'application et du role auquel l'utilisateur appartien 
		et on affiche une alerte.
	*/
	
	/* détermination du role public */

	$application = new DomDocument();
	$application->load(_BACKEND_PATH . "/framework/" . "application.xml");
	
	$roles = $application->getElementsByTagName('role');
	
	if(count($roles) > 0)
	{
		foreach ($roles as $role)
		{
			if($role->getAttribute('public') == "true")
			{
				
				$publicDir =  $role->getAttribute('application') . "/templates/";
				$publicApp =  $role->getAttribute('application');
				$publicRole = $role->getAttribute('name');
				stop;
			}
		}	
	}
	
	/* Routage du module en fonction de l'URL et du role de l'utilisateur */
	$url = str_replace(_VHOST_DIR . "/" , '', $_SERVER['REQUEST_URI']);
	if(false !== strpos($url, '?'))
	{
		$url = substr($url, 0, strpos($url, '?'));
	}

	/* cas de l'url vide */
	if(empty($url))
	{
		include(_VHOST_PATH . $publicApp . '/menu_principal.php');
		if(file_exists(_VHOST_PATH . $publicApp . '/actions/functions.php'))
		{
			include(_VHOST_PATH . $publicApp . '/actions/functions.php');
		}
		/* si on n'a pas trouvé de home on reroute par defaut vers la première page du menu */
		$item = reset($menu[$publicRole]);
		$rubrique_url = new Url();
		$rubrique_url->setType($_SESSION["type_url"]);
		$item_url = $rubrique_url->getUrl($item['code'].",");
		header('location:' . $item_url);
		exit;
	}
	
	$url_infos = $T00->getUrlInfos(str_replace('/', "", $url));
	
	if(isset($_GET['preview']) && role_exists($_GET['preview']))
	{
		$module = $_GET['preview'];
	}
	else
	{
		$module = strtolower($_SESSION['user']['groupe']);
	
	}

	if(count($url_infos) > 0)
	{
		if(count($url_infos) == 1)
		{
			$url_infos = $url_infos[0];
			
			if($url_infos->T02_role_va == $module)
			{
				/* Si l'url correspond bien au role de l'utilisateur */
				$_SESSION['app'] = $url_infos->T00_application_va;
			}
			elseif($url_infos->T02_role_va == $publicRole)
			{
				$_SESSION['app'] = $url_infos->T00_application_va;
				$module = $publicRole;
			}
		}
	}


	/* Initialisation variables templates */
	$mainDir = $_SESSION['app'] . "/templates/";
	$templateDir = $mainDir;
	$modulesDir = _VHOST_PATH . "classes/modules/";
	$publicDir = "app/templates/";

	
	
	/* Point d'insertion du hook main_template */

	if(file_exists(_VHOST_PATH . $_SESSION['app'] . '/hook/init.php'))
	{
		include(_VHOST_PATH . $_SESSION['app'] . '/hook/init.php');
	}

	$has_public_role = false;
	if(file_exists(_VHOST_PATH . $_SESSION['app'] . '/public.php'))
	{
		$has_public_role = true;
	}

	include(_VHOST_PATH . $_SESSION['app'] . '/menu_principal.php');
	if(file_exists(_VHOST_PATH . $_SESSION['app'] . '/actions/functions.php'))
	{
		include(_VHOST_PATH . $_SESSION['app'] . '/actions/functions.php');
	}

	/* Url */
	$rubPage = $T00->getRubPage($_SERVER['REDIRECT_url_propre'], $_SESSION['app']);
	if(!empty($rubPage))
	{
		$_SESSION['contexte']['rubrique'] = $rubPage[0]->T00_rubrique_va;
		$_PAGE =  $rubPage[0]->T00_page_va;
		$_ROLE =  $rubPage[0]->T00_role_va;
	}
	
	/* $make_menu est une fonction anonyme qui peut être ovveridée dans function.php */
	if(isset( $make_menu))
	{
		$menu_principal = $make_menu($module);
	}
	
	
	$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : "";
	
	switch($action)
	{
		default:
			include_once($_SESSION['app'] . "/" . $module.'.php');
			
			echo $_SESSION['app'] . "/" . $module.'.php';
			$debug_vars .= $module;
			echo show_bar($debug_vars);
			exit;
			break;
		
		case "erreur":
		case "login":
			$page_id="login";
			$titre = "Identification";
			$soustitre = "";
				
			$form_connexion = "";
			$social_connect = "";
			$return_to = "";
			$close_script = "";
			
			if(isset($_GET['error']))
			{
				switch ($_GET['error'])
				{
					case "oAuth":
						$page = new Page();
						$message = $page->_T('libErrorOauthMessage');
						unset($page);
						break;
				}
			}
			
			$templateFile = "login.html"; 
			$tmpVars = array("/{MESSAGE}/i", "/{FORM_CONNEXION}/i", "/{SOCIAL_CONNECT}/i", "/{RETURN_TO}/i", "/{CLOSE_SCRIPT}/i");
			$replace = array($message, $form_connexion, $social_connect, $return_to, $close_script);
			
			
			$page = new Page($publicDir, $templateFile, "");
			$page->replace_tags($tmpVars, $replace);
			$login =  $page->output();
			
			
			$meta_title = isset($metas[$page_id]["title"])?$metas[$page_id]["title"]:"";
			$meta_keywords = isset($metas[$page_id]["keywords"])?$metas[$page_id]["keywords"]:"";
			$meta_description = isset($metas[$page_id]["description"])?$metas[$page_id]["description"]:"";
			
			$templateFile = "main_public.html"; 
			$tmpVars = array("/{SITE_NAME}/i", "/{SITE_BASELINE}/i", "/{CONTENT}/i", "/{MENU_PRINCIPAL}/i", "/{TITRE}/i", "/{SOUSTITRE}/i", "/{INFOS_CONNEXION}/i", "/{REDIRECT}/i", "/{UNIVERS}/i", "/{META_TITLE}/i", "/{META_KEYWORDS}/i", "/{META_DESCRIPTION}/i");
			$replace = array(_SITE_NAME, _SITE_BASELINE, $login, $menu_principal, $titre, $soustitre, infos_connexion(), _VHOST_URL . "/accueil", $_SESSION['univers']['actif'],   $meta_title, $meta_keywords, $meta_description);
			
			/* Hook injection variables template dans layout principal */
			$hook_file= _VHOST_PATH . $_SESSION['app'] . '/hook/main_layout_vars.php';
			
			if(file_exists($hook_file))
			{
				include($hook_file);
			}
			/* Hook injection variables template dans layout principal */
			
			$page = new Page($publicDir, $templateFile, $_SESSION['locale']);
			$page->replace_tags($tmpVars, $replace);
			echo $page->output();
			break;
			
		case "verifLogin":
			if( isset($_REQUEST["login"]) && isset($_REQUEST["password"]) )
			{
				if(verifLogin($conn, $_REQUEST['login'], $_REQUEST['password']))
				{
					/*
						Si dans application.xml un attribut home a été placé à true dans une des rubriques du rôle
						La personne identifiée sera redirigée vers cette page
						Sinon par défaut sur l'index global
					*/
					foreach($menu[$_SESSION['user']['groupe']] as $item)
					{
						
						if ($item['home'])
						{

							header('location:' . $item['code']);
							exit;
						}
					}
					
					
					/* si on n'a pas trouvé de home on reroute par defaut vers la première page du menu */
					$item = reset($menu[$_SESSION['user']['groupe']]);
					$rubrique_url = new Url();
					$rubrique_url->setType($_SESSION["type_url"]);
					$item_url = $rubrique_url->getUrl($item['code'].",");


					header('location:' . $item_url);
				}
				else
				{
					header('location:erreur');
				}
			}
			elseif( isset($_REQUEST["provider"]) )
			{
				$provider = $_REQUEST["provider"];
				
				try{
					// initialize Hybrid_Auth with a given file
					
					$hybridauth = new Hybrid_Auth( _VHOST_PATH . '/classes/hybridauth/hybridauth/config.php' );
					$adapter = $hybridauth->authenticate( $provider );
					$user = $adapter->getUserProfile();
					log_user($user, $provider);
				}
				catch( Exception $e )
				{
					echo "Error: please try again!";
					echo "Original error message: " . $e->getMessage();
				}
				
			}    
			
			
			if($_SESSION['maintenance'] == "on" && ($_SESSION['user']['role'] != "ADMIN" || $_SESSION['user']['role'] != "SUPERADMIN" ))
			{
				session_destroy();
			}
			//header('location:index.php');
			break;
		
		case "reset_password":
			$page_id="reset_password";
			$titre = "Authentification";
			$sous_titre = "Réinitialisation du mot de passe";
			
			
			if(isset($_POST['hash']) && $_POST['hash'] == $_SESSION['hash'] && isset($_POST['login']))
			{
				$user = $T01->getCompteByLogin($_POST['login']);
				
				if($user)
				{
					$_SESSION['hash']=null;
					$code_interne = encrypt($user[0]->T01_codeinterne_i);
					
					$url_reset = _VHOST_URL . "/reset-" . $code_interne;

					$templateFile = "reset_password.html"; 
					$tmpVars = array("/{URL_RESET}/i");
					$replace = array($url_reset);
					$page = new Page($_SESSION['app'] . "/templates/emails/", $templateFile,  $_SESSION["locale"]);
					$page->replace_tags($tmpVars, $replace);
					$body_message =  $page->output();


					include_once('classes/class.phpmailer.php');
					$mail = new phpmailer();
					$mail->IsHTML(true);
					$mail->CharSet = 'UTF-8';
					$mail->From     = _ADMIN_NAME;
					$mail->FromName = _ADMIN_EMAIL;
					$mail->Mailer   = "sendmail";
					$mail->Subject = "[SOITC] Réinitialisation du mot de passe";
					$mail->Body    = $body_message;
					$mail->AddAddress($user[0]->T01_email_va, $user[0]->T03_nom_structure_va);

					if($mail->Send())
					{
						$message = "Un message vous a été envoyé";
					}
					else
					{
						$message = "Il y a eu un problème lors de l'envoi de votre email.";
					}
				}
				else
				{
					$message="Cet email n'existe pas";
				}

				$templateFile = "contenu.html"; 
				$tmpVars = array("/{TITRE}/i", "/{SOUS_TITRE}/i", "/{CONTENT}/i");
				$replace = array( $titre, $sous_titre, $message);
				$page = new Page($publicDir, $templateFile, "");
				$page->replace_tags($tmpVars, $replace);
				$login =  $page->output();
			}
			else
			{
				$_SESSION['hash'] = md5(time());
				$templateFile = "reset_password.html"; 
				$tmpVars = array("/{HASH}/i","/{MESSAGE}/i");
				$replace = array($_SESSION['hash'], $message);
				
				$page = new Page($publicDir, $templateFile, "");
				$page->replace_tags($tmpVars, $replace);
				$login =  $page->output();
			}
			

			$templateFile = "main_public.html"; 
			$tmpVars = array( "/{SITE_NAME}/i", "/{SITE_BASELINE}/i", "/{CONTENT}/i", "/{MENU_PRINCIPAL}/i", "/{TITRE}/i", "/{SOUSTITRE}/i", "/{INFOS_CONNEXION}/i", "/{REDIRECT}/i", "/{UNIVERS}/i", "/{META_TITLE}/i", "/{META_KEYWORDS}/i", "/{META_DESCRIPTION}/i");
			$replace = array(_SITE_NAME, _SITE_BASELINE, $login, "", $titre, $soustitre, "", _VHOST_URL, $_SESSION['univers']['actif'],   $meta_title, $meta_keywords, $meta_description);
			
			/* Hook injection variables template dans layout principal */
			$hook_file= _VHOST_PATH . $_SESSION['app'] . '/hook/main_layout_vars.php';
			
			if(file_exists($hook_file))
			{
				include($hook_file);
			}
			/* Hook injection variables template dans layout principal */
			

			$page = new Page($publicDir, $templateFile, $_SESSION['locale']);
			$page->replace_tags($tmpVars, $replace);
			echo $page->output();
			break;

		case "reset":
			$page_id="reset";
			$titre = "Authentification";
			$soustitre = "Réinitialisation du mot de passe";
			if(isset($_POST['hash']) && $_POST['hash'] == $_SESSION['hash'] && isset($_POST['password']))
			{
				$password = md5($_POST['password']);
				$password2 = md5($_POST['password2']);
				if($password == $password2)
				{
					
					$code_interne = decrypt($_POST['code_interne']);
					
					$T01->updatePassword($password, $code_interne);
					$_SESSION['hash'] = null;
					$message = "Votre mot de passe a été réinitialisé";
				}
				else
				{
					$message = "Les deux mots de passes ne sont pas identiques";
				}
				$templateFile = "contenu.html"; 
				$tmpVars = array("/{TITRE}/i", "/{SOUS_TITRE}/i", "/{CONTENT}/i");
				$replace = array( $titre, $sous_titre, $message);
				$page = new Page($publicDir, $templateFile, "");
				$page->replace_tags($tmpVars, $replace);
				$login =  $page->output();
			}
			else
			{
				$_SESSION['hash'] = md5(time());

				$templateFile = "reset.html"; 
				$tmpVars = array("/{HASH}/i","/{MESSAGE}/i","/{CODE_INTERNE}/i");
				$replace = array($_SESSION['hash'], $message, urlencode($_GET['code_interne']));
				
				$page = new Page($publicDir, $templateFile, "");
				$page->replace_tags($tmpVars, $replace);
				$login =  $page->output();
			}


			$templateFile = "main_public.html"; 
			$tmpVars = array("/{SITE_NAME}/i", "/{SITE_BASELINE}/i", "/{CONTENT}/i", "/{MENU_PRINCIPAL}/i", "/{TITRE}/i", "/{SOUSTITRE}/i", "/{INFOS_CONNEXION}/i", "/{REDIRECT}/i", "/{UNIVERS}/i", "/{META_TITLE}/i", "/{META_KEYWORDS}/i", "/{META_DESCRIPTION}/i");
			$replace = array(_SITE_NAME, _SITE_BASELINE, $login, $menu_principal, $titre, $soustitre, infos_connexion(), _VHOST_URL . "/accueil", $_SESSION['univers']['actif'],   $meta_title, $meta_keywords, $meta_description);
			

			/* Hook injection variables template dans layout principal */
			$hook_file= _VHOST_PATH . $_SESSION['app'] . '/hook/main_layout_vars.php';
			
			if(file_exists($hook_file))
			{
				include($hook_file);
			}
			/* Hook injection variables template dans layout principal */
			

			$page = new Page($publicDir, $templateFile, $_SESSION['locale']);
			$page->replace_tags($tmpVars, $replace);
			echo $page->output();

			break;

		case "maintenance_login":
			if($_SESSION['access'] == "erreur")
			{
				$message = "Problème à l'identification<br/>Veuillez recommencer";
			}
			
			$templateFile = "form_connexion.html"; 
			$tmpVars = array("/{MESSAGE}/i", "/{SOCIAL_CONNECT}/i");
			$replace = array($message, $social_connect);
			$page = new Page($publicDir, $templateFile,  $_SESSION['locale']);
			$page->replace_tags($tmpVars, $replace);
			$form_connexion = $page->output();
			
			$templateFile = "accueil.html"; 
			$tmpVars = array("/{CONTENT}/i", "/{MESSAGE}/i", "/{FORM_CONNEXION}/i");
			$replace = array("", $message, $form_connexion);
			$page = new Page($publicDir, $templateFile, "");
			$page->replace_tags($tmpVars, $replace);
			echo $page->output();
			exit;
			break;
			
		
		
		case "logout":
			session_destroy();
			$provider = $_SESSION["user"]["provider"];
			if($provider != "internal")
			{
				try 
				{
					$hybridauth = new Hybrid_Auth( _VHOST_PATH . '/classes/hybridauth/hybridauth/config.php' );
					$adapter = $hybridauth->getAdapter( $provider );
					$adapter->logout();
				}
				catch( Exception $e )
				{
					echo "Error: please try again!";
					echo "Original error message: " . $e->getMessage();
				}
			}
			foreach($menu['public'] as $item)
			{
				if ($item['home'])
				{
					header('location:' . $item['code']);
					exit;
				}
			}
			header('location:' . _VHOST_URL);
			break;
		
	}
	
?>