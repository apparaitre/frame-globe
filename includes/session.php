<?php
/*
 *  Gestion des profils
 *  @author: Thierry Cazalet
 *
 */

if (!defined("_PROFIL_ACCESS")){header('location:../erreur');}

	
function verifLogin($conn, $login, $password, $referer='index.php')
{
	
	// On instancie la classe T01_compte
    $T01 = new T01_users($conn);
	$users = $T01->getCompteByLogin($login);
	$user = $users[0];
	
	if ($user->T01_password_va == md5($password))
	{
		
		$_SESSION['user']['id_user'] = $user->T01_codeinterne_i;
		$_SESSION['user']['login'] = $login;
		$_SESSION['user']['avatar'] = "";
		$_SESSION['user']['role'] = $user->T04_libelle_va;
		$_SESSION['user']['id_collectivite'] = $user->T120_codeinterne_i;
		$_SESSION['user']['groupe'] = $user->T02_role_va;
		$_SESSION['user']['roleID'] = $user->T02_codeinterne_i;
		$_SESSION['user']['email'] = $user->T01_email_va;
		$_SESSION['user']['nom']= $user->T03_nom_va;
		$_SESSION['user']['prenom'] = $user->T03_prenom_va;
		$_SESSION['user']['telephone'] = $user->T03_telephone_va;
		$_SESSION['user']['adresse']['cp'] = $user->T100_codepostal_va;
		$_SESSION['user']['adresse']['ville'] = $user->T100_ville_va;
		$_SESSION['user']['adresse']['pays'] = $user->T03_pays_va == null ? "France" : $adresse->country;
		$_SESSION['user']['lastlog'] = $user->T01_lastlog_d;
		$_SESSION['locale'] =  $user->T01_locale_va;
		$_SESSION["user"]["provider"] = "internal";
		$_SESSION['app'] = $user->T02_application_va;
		

		// $_SESSION['user']['logo'] = _ASSET_PATH . "logo/" . $user->T03_logo_va;
		$result = $T01->updateDateConnection(now(), $user->T01_codeinterne_i);
		$_SESSION['access'] = "logged";
		
		return true;
		
	}
	else
	{
		return false;
	}
}

?>