<?php

	
function dj_validate_auth_cookie($cookie = '', $scheme = '') 
{
	global $dbHM;
	if ( ! $cookie_elements = wp_parse_auth_cookie($cookie, $scheme) ) 
	{
		//do_action('auth_cookie_malformed', $cookie, $scheme);
		
		return false;
	}
	extract($cookie_elements, EXTR_OVERWRITE);
	
	$expired = $expiration;

	
	// Quick check to see if an honest cookie has expired
	if ( $expired < time() ) {
		//do_action('auth_cookie_expired', $cookie_elements);
		
		return false;
	}

	
	
	$query = "SELECT * FROM hb_users where user_login=:login ";
	$sth = $dbHM->prepare($query);
	$sth->bindValue(':login', $username );
	$sth->execute();
	$user = $sth->fetch(PDO::FETCH_OBJ);
	
	if ( ! $user ) {
		//do_action('auth_cookie_bad_username', $cookie_elements);
		return false;
	}
	else
	{
		$query = "SELECT * FROM hb_usermeta  where user_id=:id ";
		$sth = $dbHM->prepare($query);
		$sth->bindValue(':id', $user->ID );
		$sth->execute();
		$user_metas = $sth->fetch(PDO::FETCH_OBJ);
		foreach ($user_metas as $meta)
		{
			$metas[$meta->meta_key] = $meta->meta_value;
		}
			
		$oUser = "{
			   'id' : " . $user->ID . ",
			   'username' : '" . $user->display_name . "',
			   'lastname' : '" . $user->ID . "',
			   'firstname' : '" . $user->ID . "',
			   'password' : '" . $user->user_pass . "',
			   'email' : '" . $user->user_email . "',
			   'addressList' : [
				  {
					 'address' : '5 rue Saint-Joseph',
					 'city' : 'Paris',
					 'zipCode' : '75002',
				  }
			   ]
			  'facebookId' : null
			}";
	}

	$pass_frag = substr($user->user_pass, 8, 4);

	$key = wp_hash($username . $pass_frag . '|' . $expiration, $scheme);
	$hash = hash_hmac('md5', $username . '|' . $expiration, $key);
	
	if ( $hmac != $hash ) {
		//do_action('auth_cookie_bad_hash', $cookie_elements);
		return false;
	}

	if ( $expiration < time() )
	{
		$GLOBALS['login_grace_period'] = 1;
	}
	
	//do_action('auth_cookie_valid', $cookie_elements, $user);
		
	return json_encode($oUser);
}

function dj_parse_auth_cookie($cookie = '', $scheme = '') {
	if ( empty($cookie) ) 
	{
		switch ($scheme)
		{
			case 'auth':
				$cookie_name = AUTH_COOKIE;
				break;
			case 'secure_auth':
				$cookie_name = SECURE_AUTH_COOKIE;
				break;
			case "logged_in":
				$cookie_name = LOGGED_IN_COOKIE;
				break;
			default:
				$cookie_name = AUTH_COOKIE;
				$scheme = 'auth';
				
	    }

		if ( empty($_COOKIE[$cookie_name]) )
		{
			return false;
		}
		$cookie = $_COOKIE[$cookie_name];
	}

	$cookie_elements = explode('|', $cookie);
	if ( count($cookie_elements) != 3 )
	{
		return false;
	}

	list($username, $expiration, $hmac) = $cookie_elements;

	return compact('username', 'expiration', 'hmac', 'scheme');
}


function dj_hash($data, $scheme = 'auth') 
{
	$salt = LOGGED_IN_KEY . LOGGED_IN_SALT ;
	return hash_hmac('md5', $data, $salt);
}
?>