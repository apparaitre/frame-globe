<?php

	/*
	 * Fichier généré par framework/generer_classes.php\ Ne pas éditer
	*/

	include_once(_VHOST_PATH . 'classes/db/class.t00_url.php');
	$T00 = new t00_url ($conn);
	include_once(_VHOST_PATH . 'classes/db/class.t01_users.php');
	$T01 = new t01_users ($conn);
	include_once(_VHOST_PATH . 'classes/db/class.t02_role.php');
	$T02 = new t02_role ($conn);
	include_once(_VHOST_PATH . 'classes/db/class.t03_profil.php');
	$T03 = new t03_profil ($conn);
	include_once(_VHOST_PATH . 'classes/db/class.t04_dico.php');
	$T04 = new t04_dico ($conn);
	include_once(_VHOST_PATH . 'classes/db/class.t05_modules.php');
	$T05 = new t05_modules ($conn);
	include_once(_VHOST_PATH . 'classes/db/class.t06_variables.php');
	$T06 = new t06_variables ($conn);
	include_once(_VHOST_PATH . 'classes/db/class.t100_rubriques.php');
	$T100 = new t100_rubriques ($conn);
	include_once(_VHOST_PATH . 'classes/db/class.t101_articles.php');
	$T101 = new t101_articles ($conn);
?>