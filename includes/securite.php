<?php
	/*
	 * ecran_securite.php
	 * ------------------
	 */

	define('_ECRAN_SECURITE', '1.1.7'); // 24 mai 2013
		
	/*
	 * Test utilisateur
	 */
	if (isset($_GET['test_ecran_securite']))
		$ecran_securite_raison = 'test '._ECRAN_SECURITE;

	/*
	 * Détecteur de robot d'indexation
	 */
	if (!defined('_IS_BOT'))
		define('_IS_BOT',
			isset($_SERVER['HTTP_USER_AGENT'])
			AND preg_match(
			// mots generiques
			',bot|slurp|crawler|spider|webvac|yandex|'
			// UA plus cibles
			. '80legs|accoona|AltaVista|ASPSeek|Baidu|Charlotte|EC2LinkFinder|eStyle|Google|INA dlweb|Java VM|LiteFinder|Lycos|Rambler|Scooter|ScrubbyBloglines|Yahoo|Yeti'
			. ',i',(string) $_SERVER['HTTP_USER_AGENT'])
		);
		
	/*
	 * Échappement xss referer
	 */
	if (isset($_SERVER['HTTP_REFERER']))
		$_SERVER['HTTP_REFERER'] = strtr($_SERVER['HTTP_REFERER'], '<>"\'', '[]##');
	
	/*
	 * Contrôle de quelques variables (XSS)
	 */
	foreach(array('rubrique', 'page', 'debug') as $var) {
		if (isset($_GET[$var]))
			$_REQUEST[$var] = $GLOBALS[$var] = $_GET[$var] = preg_replace(',[^\w\,/#&;-]+,',' ',(string)$_GET[$var]);
		if (isset($_POST[$var]))
			$_REQUEST[$var] = $GLOBALS[$var] = $_POST[$var] = preg_replace(',[^\w\,/#&;-]+,',' ',(string)$_POST[$var]);
	}


	/*
	 * Bloque les requêtes contenant %00 (manipulation d'include)
	 */
	if (strpos(
		@get_magic_quotes_gpc() ?
			stripslashes(serialize($_REQUEST)) : serialize($_REQUEST),
		chr(0)
	) !== false)
		$ecran_securite_raison = "%00";

	
	
	/*
	 * S'il y a une raison de mourir, mourons
	 */
	if (isset($ecran_securite_raison)) {
		header("HTTP/1.0 403 Forbidden");
		header("Expires: Wed, 11 Jan 1984 05:00:00 GMT");
		header("Cache-Control: no-cache, must-revalidate");
		header("Pragma: no-cache");
		header("Content-Type: text/html");
		die("<html><title>Error 403: Forbidden</title><body><h1>Error 403</h1><p>You are not authorized to view this page ($ecran_securite_raison)</p></body></html>");
	}
	
	
/*
 * Bloque les bots quand le load déborde
 */
if (!defined('_ECRAN_SECURITE_LOAD'))
	define('_ECRAN_SECURITE_LOAD', 4);

if (
	defined('_ECRAN_SECURITE_LOAD')
	AND _ECRAN_SECURITE_LOAD>0
	AND _IS_BOT
	AND $_SERVER['REQUEST_METHOD'] === 'GET'
	AND (
		(function_exists('sys_getloadavg')
		  AND $load = sys_getloadavg()
		  AND is_array($load)
		  AND $load = array_shift($load)
		)
		OR
		(@is_readable('/proc/loadavg')
		  AND $load = file_get_contents('/proc/loadavg')
		  AND $load = floatval($load)
		)
	)
	AND $load > _ECRAN_SECURITE_LOAD // eviter l'evaluation suivante si de toute facon le load est inferieur a la limite
	AND rand(0, $load*$load) > _ECRAN_SECURITE_LOAD*_ECRAN_SECURITE_LOAD
) {
	header("HTTP/1.0 503 Service Unavailable");
	header("Retry-After: 300");
	header("Expires: Wed, 11 Jan 1984 05:00:00 GMT");
	header("Cache-Control: no-cache, must-revalidate");
	header("Pragma: no-cache");
	header("Content-Type: text/html");
	die("<html><title>Status 503: Site temporarily unavailable</title><body><h1>Status 503</h1><p>Site temporarily unavailable (load average $load)</p></body></html>");
}

?>