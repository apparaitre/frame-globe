<?php
	
 if (!defined("_PROFIL_ACCESS")){header('location:../erreur');}

    $dsn = "mysql:dbname=$dbName;host=$host";
	try
	{
		$conn = new PDO($dsn, $user, $pwd);
		$conn->exec("SET CHARACTER SET utf8");
		$conn->query("SET NAMES utf8");
	}
	catch(PDOException $Exception)
	{
		echo "L'application n'est pas encore installée";
		// Vérifier droits d'accès à la page en enlevant exit
		exit;
	}
?>