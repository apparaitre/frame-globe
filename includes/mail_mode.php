<?php
	switch(_APP_MODE)
	{
		case "dev_local":
			$mail->IsSmtp();
			$mail->Mailer = 'smtp';
			$mail->Host = "localhost";
			break;
		
		case "dev":
			$mail->IsMail();
			$mail->Mailer = 'mail';
			break;
			
		case "production":
			$mail->IsMail();
			$mail->Mailer = 'mail';
			break;	
	}
	
	$mail->IsHTML(true);
	$mail->CharSet = 'UTF-8';
			
?>