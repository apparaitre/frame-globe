<?php
	if (!defined("_PROFIL_ACCESS")){header('location:../erreur');}
	
	function array_insert($arr, $insert, $position) {
	    $i = 0;
	    foreach ($arr as $key => $value) {
	            if ($i == $position) {
	                    foreach ($insert as $ikey => $ivalue) {
	                            $ret[$ikey] = $ivalue;
	                    }
	            }
	            $ret[$key] = $value;
	            $i++;
	    }
	    return $ret;
	}
	
	function role_exists($role)
	{
		$application = new DomDocument();

		$app_file = _BACKEND_PATH . "/framework/application.xml";
		
		$application->load($app_file);

		$params = $application->getElementsByTagName('param');
		if(count($params) > 0)
		{
			foreach ($params as $param)
			{
				$name = $param->getAttribute('name');
				$$name = $param->getAttribute('value');
			}
		}
		
		$roles = $application->getElementsByTagName('role');
		
		
		/* debut ROLES */
		if(count($roles) > 0)
		{
			$_CONTENT .= "<ul>";
			
			/* On cherche le role public */
			foreach ($roles as $role)
			{
				$FW_ROLE = $role->getAttribute('name');
				if($FW_ROLE == $role) { return true; exit;}
			}

		}
		return false;
	}
	


	function add_param($url, $param)
	{
		$url = strpos($url, "?") === false ? $url ."?" . $param : $url ."&" . $param ;
		return $url;
	}
	
	function is_logged()
	{
		if($_SESSION['access'] != "logged")
		{
			header('location:accueil');
		}
	}
	
	function infos_connexion($home=null)
	{
		$connexion = '<ul>';
		if($_SESSION['user']['groupe'] == "public" ){
			$connexion .= '<li ><a href="login" id="login_link">M\'identifier</a></li>';
		}
		elseif($_SESSION['access'] == "logged")
		{
			$connexion .= '<li class="pl3"><a href="logout">Déconnexion</a></li><li  class="pl3">Bonjour ' . $_SESSION['user']['prenom']. ' ' . $_SESSION['user']['nom'] . ' | <a href="' . $home . '">Mon compte</a></li>';
		}
		
		$connexion .= '</ul>';
		
		return $connexion;
	}
	
	function log_user($user_data, $provider)
	{
		global $T01, $T14;
		
		$login = $T14->getCompteByEmail($user_data->email, $provider);
		
		$_SESSION['user']['id_user'] = $login[0]->T01_codeinterne_i;
		$_SESSION['user']['id_user_externe'] = $user_data->identifier;
		$_SESSION['user']['login'] = $user_data->email;
		$_SESSION['user']['avatar'] = $user_data->photoURL;
		$_SESSION['user']['nom']= $user_data->lastName;
		$_SESSION['user']['prenom'] = $user_data->firstName;
		$_SESSION['user']['email'] = $user_data->email;
		$_SESSION['user']['adresse']['adresse'] = $user_data->address;
		$_SESSION['user']['adresse']['cp'] = $user_data->zip;
		$_SESSION['user']['adresse']['ville'] = $user_data->city;
		$_SESSION['user']['adresse']['pays'] = $user_data->country == null ? "France" : $adresse->country;
		
		$_SESSION['user']['adresse']['latitude'] = 48.8675652;
		$_SESSION['user']['adresse']['longitude'] = 2.3439901;
		$_SESSION["user"]["provider"] = $provider;

		
		if(count($login) > 0)
		{
			$_SESSION['user']['role'] = "MEMBRE";
			$_SESSION['user']['groupe'] = "MEMBRE";
			$_SESSION['access'] = "logged";
			return true;
		}
		else
		{
			$_SESSION['access'] = "";
			$_SESSION['user']['role'] = "PUBLIC";
			$_SESSION['user']['groupe'] = "PUBLIC";
			if(_INSCRIPTION_MODE == "open")
			{
				$t01_codeinterne_i = $T01->insert(2, $_SESSION['user']['id_user_externe'] , $_SESSION['user']['nom'], $_SESSION['user']['prenom'], $_SESSION['user']['email'], $_SESSION['user']['adresse']['ville'], $_SESSION['user']['adresse']['cp'], $_SESSION['user']['login'], now(), "fr", $_SESSION["user"]["provider"]);
				
				$_SESSION['user']['id_user'] = $t01_codeinterne_i;
				$_SESSION['user']['role'] = "MEMBRE";
				$_SESSION['user']['groupe'] = "MEMBRE";
				$_SESSION['access'] = "logged";
			}
			else
			{
				header("location:login?error=oAuth&provider={$provider}");
				exit;
			}
			return false;
		}
	}

	
	function array_change_value_case(array $input, $case = CASE_LOWER) { 
    switch ($case) { 
        case CASE_LOWER: 
            return array_map('mb_strtolower', $input); 
            break; 
        case CASE_UPPER: 
            return array_map('mb_strtoupper', $input); 
            break; 
        default: 
            trigger_error('Case is not valid, CASE_LOWER or CASE_UPPER only', E_USER_ERROR); 
            return false; 
    } 
} 

	function nettoie_chaine($chaine, $separateur="_")
	{
		$caracteres = array(
				'À' => 'a', 'Á' => 'a', 'Â' => 'a', 'Ä' => 'a', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ä' => 'a', '@' => 'a',
				'È' => 'e', 'É' => 'e', 'Ê' => 'e', 'Ë' => 'e', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', '€' => 'e',
				'Ì' => 'i', 'Í' => 'i', 'Î' => 'i', 'Ï' => 'i', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
				'Ò' => 'o', 'Ó' => 'o', 'Ô' => 'o', 'Ö' => 'o', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'ö' => 'o',
				'Ù' => 'u', 'Ú' => 'u', 'Û' => 'u', 'Ü' => 'u', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'µ' => 'u',
				'Œ' => 'oe', 'œ' => 'oe',
				'$' => 's');
				
		$chaine = strtr($chaine, $caracteres);
		$chaine = preg_replace('#[^A-Za-z0-9]+#', $separateur, $chaine);
				
		return strtolower($chaine);
	}
	
	function isort($a,$b) {
		return strtolower($a)>strtolower($b);
	}

	function roundDown($value, $prec)
	{
		return intval($value * puissance(10, $prec) ) / puissance(10, $prec);
	}
	
	function puissance($x,$y) { 
	  $resultat=1;
	  for ($i=0;$i<$y;$i++)
	   $resultat *= $x;
	  return $resultat;
	}
	
	function now($mode = "date")
	{
		$now = getdate();
		$format = "";
		if($mode == "date") { $format = "%02d/%02d/%04d";}
		if($mode == "datetime") { $format = "%02d/%02d/%04d %02d:%02d:%02d";}
		$today = sprintf($format, $now['mday'], $now['mon'], $now['year'], $now['hours'], $now['minutes'], $now['seconds']);
		return $today;	
	}
	
	function mysql2frDate($date, $type="date")
	{
		if(!empty($date))
		{
				$_D = explode(' ', $date);
			$_T = $_D[1];
			$_D = explode('-', $_D[0]);
			switch($type)
			{
				default;
				
				case "date":
					return $_D[2]."/".$_D[1].'/'.$_D[0];
					break;
					
				case "datetime":
					return $_D[2]."/".$_D[1].'/'.$_D[0] . " " . $_T;
					break;
			}
		}
	}
	
	function fr2mysqlDate($date, $type="date")
	{
		if(!empty($date))
		{
			$_D = explode(' ', $date);
			$_T = $_D[1];
			$_D = explode('/', $_D[0]);
			
			
			switch($type)
			{
				default;
				
				case "date":
				
					return $_D[2]."-".$_D[1].'-'.$_D[0];
					break;
					
				case "datetime":
					$_T = $_T == "" ? "00:00:00" : $_T;
					return $_D[2]."-".$_D[1].'-'.$_D[0] . " " . $_T;
					break;
			}
		}
		else
		{
			return "0000-00-00";
		}
	}
	
	function raccourcir($string, $length)
	{
		if(mb_strlen($string) <= $length)
		{
			return $string;
		}
		else
		{
			return mb_substr($string, 0, $length)."...";
		}
	}
	
	function NumTel($tel) 
	{ 
		$ch = 10;                               // Numéro à 10 chiffres 
		$tel = eregi_replace('[^0-9]',"",$tel); // supression sauf chiffres 
		$tel = trim($tel);                      // suppression espaces avant et après 
		if (strlen($tel) > $ch) 
		{ 
			$d = strlen($tel) - $ch; // retrouve la position pour ne garder 
									 // que les $ch derniers 
		} 
		else 
		{ 
			$d = 0; 
		} 
		$tel = substr($tel,$d,$ch); // récupération des $ch derniers chiffres 
		$regex = '([0-9]{1,2})([0-9]{1,2})([0-9]{1,2})([0-9]{1,2})([0-9]{1,2})$'; 
		$newtel = eregi_replace($regex, 
			'\\1 \\2 \\3 \\4 \\5',$tel); // mise en forme 
		return $newtel; /* Exemple : 03-81-51-45-78  */ 
	}

	function NumSiret($tel) 
	{ 
		$ch = 14;                               // Numéro à 10 chiffres 
		$tel = eregi_replace('[^0-9]',"",$tel); // supression sauf chiffres 
		$tel = trim($tel);                      // suppression espaces avant et après 
		if (strlen($tel) > $ch) 
		{ 
			$d = strlen($tel) - $ch; // retrouve la position pour ne garder 
									 // que les $ch derniers 
		} 
		else 
		{ 
			$d = 0; 
		} 
		$tel = substr($tel,$d,$ch); // récupération des $ch derniers chiffres 
		$regex = '([0-9]{1,3})([0-9]{1,3})([0-9]{1,3})([0-9]{1,5})$'; 
		$newtel = eregi_replace($regex, 
			'\\1 \\2 \\3 \\4',$tel); // mise en forme 
		return $newtel; /* Exemple : 03-81-51-45-78  */ 
	}
	
	function formtel($tel)
	{
		$tel = str_replace(" ", "", $tel);
		$tel = str_replace("/", "", $tel);
		$tel = str_replace("-", "", $tel);
		
		return $tel;
	}

	function formPDF($str)
	{
		for($k = 0; $k < strlen($str); $k++)
		{
			$l=substr($str, $k, 1);
			$val .= $l." ";
		}
		
		return trim($val);
	}
	
	function flatArray($array)
	{
		ob_start();
		print_r($array);
		$text = ob_get_contents();
		ob_end_clean();
		
		return $text;
	}
	function make_block($text, $className)
	{
		return '<div class="' . $className . '"><div class="box-top png  margin-top-block"></div><div class="box-content"><div class="content">' . $text . '</div><br class="nettoyeur" /></div><div class="box-bottom png"></div><br class="nettoyeur" style="margin-bottom:20px;"/></div>';
	}
	
	
	/*
	 * Protéger les variables des formulaires
	 */
	function validInputData($data, $type = "string", $default = null) {
		switch($type)
		{
			case "string":
				if (is_string($data) && strlen($data = trim($data)) > 0) {
					$data = str_replace(chr(0), '-', $data);
					return get_magic_quotes_gpc() ?  stripslashes($data) : $data;
				} else {
					 return $default;
				}
				break;
				
			case "numeric":
				if (is_numeric($data))
				{
					return $data;
				}
				else
				{
					return $default;
				}
				break;
			case "array":
				if (is_array($data))
				{
					return $data;
				}
				else
				{
					return $default;
				}
				break;

		}
	}
	
	function load_metas()
	{
		$metas = null;
		if(file_exists(_VHOST_URL  . 'metas.xml'))
		{
			$xml = new DomDocument();
			$xml->preserveWhiteSpace = false;
			$xml->load(_VHOST_URL  . 'metas.xml');
			$xml->formatOutput = true;
			$racine = $xml->documentElement;
			
			$oPages = $racine->getElementsByTagName("page");
			foreach ($oPages as $oPage)
			{
				$page_id = $oPage->getAttribute('page_id');
				
				$oMetas = $oPage->getElementsByTagName("meta");
				
				foreach($oMetas as $oMeta)
				{
					$metas[$page_id][$oMeta->getAttribute('name')] = $oMeta->nodeValue;
				}
			}
		}
		return $metas;
	}
	
	function controleLuhn($val){
	   	$val = preg_replace("/[^\d]+/", '', $val); // on ne garde que les chiffres
	   	$len = strlen($val);
	    $total = 0;
	    for ($i = 1; $i <= $len; $i++) {
	        $chiffre = substr($val,-$i,1);
	        if($i % 2 == 0) {
	            $total += 2 * $chiffre;
	            if((2 * $chiffre) >= 10) $total -= 9;
	            }
	        else $total += $chiffre;
	        }
	        if($total % 10 == 0){return true;} else {return false;}
	}
	

	function init_modules($modulesDir)
	{
		$Sys = SystemCore::GetInstance(); // On récupère l'instance unique du Systeme.
		$files = scandir($modulesDir);
		$_SESSION['modules'] = "";			
		/* Ceci est la façon correcte de traverser un dossier. */
		$modules = '<ul>';
		foreach ($files as $file)
		{
			if((false !== is_file($modulesDir.$file))&&(false!==strpos($file, 'class')))
			{
				try 
				{
					include_once($modulesDir.$file);
					$eClass = strlen($file)-17;
					$dClass = strpos($file, "module")+7;
					$classname = substr($file, $dClass, $eClass);
					if(substr($file, 0, 12) == "class.module")
					{
						try
						{
							$$classname = new $classname;
							$_SESSION['modules'][$classname]['libelle'] = $classname;
							$_SESSION['modules'][$classname]['version'] = $$classname->getVersion();
							$modules .= '<li>'.$classname.' version '.$$classname->getVersion(); 
							if ( $Sys->isActive(strtolower($classname)) ) 
							{
								$_SESSION['modules'][$classname]['actif'] = true;
							} 
							$modules .= '</li>';
						}
						catch ( Exception $e ) 
						{
									
						}	
					}
				} 
				catch ( Exception $e ) 
				{
				
				}	
			}	
		}
	}

	function listeModules()
	{
		$modules = '<select  name="nom_composant" id="nom_composant"">'.chr(13);
		foreach($_SESSION[modules] as $module)
		{
			$modules .= "<option>".$module[libelle]."</option>".chr(13);	
		}
		$modules .= '</select>'.chr(13);
		return $modules;
	}
	
	
function create_file($dir, $fileName, $content="", $message="")
{
	echo $dir.$fileName."<br/>";
	touch($dir.$fileName);
    if (!$handle = fopen($dir.$fileName, 'w')) 
	{
        return false;
        exit;
    }

   	if (fwrite($handle, $content) === FALSE) 
   	{
       		return false;
       		exit;
    }
    
    fclose($handle);
    return true;
}

function array_to_json_string($arraydata) 
{
	$output = "";
	$output .= "{";
	foreach($arraydata as $key=>$val){
		if (is_array($val)) {
			$output .= "\"".$key."\" : [{";
			foreach($val as $subkey=>$subval){
				$output .= "\"".$subkey."\" : \"".$subval."\",";
			}
			$output .= "}],";
		} else {
			$output .= "\"".$key."\" : \"".$val."\",";
		}
	}
	$output .= "}";
	return $output;
}
function countFiles($dir){ 
    $files = array(); 
    $directory = opendir($dir); 
    while($item = readdir($directory)){ 
    // We filter the elements that we don't want to appear ".", ".." and ".svn" 
         if(($item != ".") && ($item != "..") && ($item != ".svn") ){ 
              $files[] = $item; 
         } 
    } 
    $numFiles = count($files); 
    return $numFiles; 
} 

function aff_timer($step)
{
	global $time_start, $time_end;
	
	$time_end = microtime(true);
	$time = $time_end - $time_start;
	error_log("Step: " . $step . "  ". $time. "s");
}



	function encrypt($mprhase) {
		$key = "a45f4e7a"; 
	    $td = mcrypt_module_open(MCRYPT_CAST_128   , '', 'ecb', '');
	    $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
	    mcrypt_generic_init($td, $key, $iv);
	    $crypted_value = mcrypt_generic($td, $mprhase);
	    mcrypt_generic_deinit($td);
	    mcrypt_module_close($td);
	 
	    return urlencode(base64_encode($crypted_value));
	}
	 
	function decrypt($mprhase) {
		$mprhase = urldecode($mprhase);
		$key = "a45f4e7a"; 
	    $td = mcrypt_module_open(MCRYPT_CAST_128   , '', 'ecb', '');
	    $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
	    mcrypt_generic_init($td, $key, $iv);
	 
	    $decrypted_value = mdecrypt_generic($td, base64_decode($mprhase));
	    mcrypt_generic_deinit($td);
	    mcrypt_module_close($td);
	    return $decrypted_value;
	}


function fileSafe($value)
{
	$value = utf8_encode(strtr(utf8_decode($value), utf8_decode('ŠŽšžŸÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÑÒÓÔÕÖØÙÚÛÜÝàáâãäåçèéêëìíîïñòóôõöøùúûüýÿ'), 'SZszYAAAAAACEEEEIIIINOOOOOOUUUUYaaaaaaceeeeiiiinoooooouuuuyy'));
     $value = strtr($value, array('Þ' => 'TH', 'þ' => 'th', 'Ð' => 'DH', 'ð' => 'dh', 'ß' => 'ss', 'Œ' => 'OE', 'œ' => 'oe', 'Æ' => 'AE', 'æ' => 'ae', 'µ' => 'u'));
     $value = preg_replace(array('/\s/', '/\.[\.]+/', '/[^\w_\.\-]/'), array('_', '.', ''), $value);
     return $value;
}


?>