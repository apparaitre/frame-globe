<?php
	if (!defined("_PROFIL_ACCESS")){header('location:../erreur');}
	error_reporting(E_ALL ^ E_NOTICE);
	
	
	ini_set("session.bug_compat_42" , 0);
	ini_set('mbstring.language', 'UTF-8');
	ini_set('mbstring.internal_encoding', 'UTF-8');
	ini_set('mbstring.http_input', 'UTF-8');
	ini_set('mbstring.http_output', 'UTF-8');
	ini_set('mbstring.detect_order', 'auto');
	
	$include_dir = dirname(__FILE__);
	
	$path = substr($include_dir , 0, strlen($include_dir) - strlen("includes"));
	define('_VHOST_PATH', $path);
	define('_MODULE_PATH', $path . "modules");
	define('_BACKEND_PATH', $path . "backend");

	// Définition du fuseau horaire
	date_default_timezone_set('Europe/Paris');
	if(!isset($_SESSION['locale'])){$_SESSION['locale'] = "FR";}
	if(!isset($_SESSION['app'])){$_SESSION['app'] = "app";}
	
	if(isset($_GET['debug'])){$_SESSION['debug'] = $_GET['debug'];}
	$_SESSION['maintenance'] = "off";


	
	/* à revoir public ne peut pas être en dur, voir application.xml params public_role */ 
	if(!isset($_SESSION['user']['groupe'])){$_SESSION['user']['groupe'] = "public";}
	
	if(!isset($_SESSION['access'])){$_SESSION['access']="";}

	$provider = ""; 
	$page_id = "accueil";
	$BROADCAST = "";
	$page_id = "";
	$titre = "frame-globe";
	$soustitre = "";
	$univers = "";
	$message = "";
	
	$ZONE_PUB = 1;
	
	$_SESSION['univers']['actif'] = "";
	
	$_SESSION['type_url'] = "propre"; /* normale|propre */
		
	
	
	if(file_exists(_VHOST_PATH . 'includes/config.php'))
	{
		include_once(_VHOST_PATH . 'includes/config.php');
	}
	else
	{
		echo "Application non installée";
		exit;
	}
	
	
 	include_once(_VHOST_PATH . 'includes/connection.php');
	include_once(_VHOST_PATH . 'classes/db/class.db.php');
	include_once(_VHOST_PATH . 'classes/db/class.queryException.php');
	include_once(_VHOST_PATH . 'includes/db.php');
	
	include_once(_VHOST_PATH . 'classes/class.page.php');
	include_once(_VHOST_PATH . 'classes/class.phpmailer.php');
	include_once(_VHOST_PATH . 'classes/class.url.php' );
	include_once(_VHOST_PATH . 'classes/class.log.php');
	include_once(_VHOST_PATH . 'classes/class.formulaire.php' );
	
	include_once(_VHOST_PATH . 'classes/upload/class.upload.php' );
	include_once(_VHOST_PATH . 'classes/hybridauth/hybridauth/Hybrid/Auth.php' );



	

	include_once(_VHOST_PATH . 'includes/securite.php');
	include_once(_VHOST_PATH . 'includes/debug.php');
   
	include_once(_VHOST_PATH . 'includes/functions.php');

	include_once(_VHOST_PATH . 'includes/session.php');

	
	
	$metas = load_metas();
	$log = new Log($conn);
?>