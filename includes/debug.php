<?php
	function show_bar($args)
	{
		global $time_start;
		
		if(_DEBUG_MODE)
		{
			$time_end = microtime(true);
			$time = $time_end - $time_start;
			
			ob_start();
			echo "<div id=\"debug_bar\">";
			
			echo "<h1>Objet USER</h1>";
			print_r($_SESSION['user']);
			
			echo "<h1>Objet GET</h1>";
			print_r($_GET);
			
			echo "<h1>Objet POST</h1>";
			print_r($_POST);
			
			echo "<h1>Objet SESSION</h1>";
			print_r($_SESSION);
			
			echo "<h1>Variables</h1>";
			print_r($args);
			
			echo "<div>temps total: " . $time. "s</div>";
			echo "</div>";
			$bar = ob_get_clean();
			
			
			return $bar;
		}
	}
?>