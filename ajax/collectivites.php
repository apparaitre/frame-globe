<?php
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");
	
	define("_PROFIL_ACCESS", "FRAME-GLOBE");
	include ('../includes/init.php');
	
	$type = validInputData($_GET['type'], "string");
	$nom = strtoupper(validInputData($_GET['nom'], "string"));
	
	if(empty($type))
	{
		$collectivites = $T120->like("%" . $nom ."%");
	}
	else
	{
		$collectivites = $T120->likeByType($type , "%" . $nom ."%");
	}
	
	foreach($collectivites  as $collectivite)
	{
		$results[] = array('codeinterne' => $collectivite->T120_codeinterne_i, 'nom' => $collectivite->T120_nom_va);
	}
	
	echo json_encode($results);
?>