<?php
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");
	
	define("_PROFIL_ACCESS", "FRAME-GLOBE");
	include ('../includes/init.php');
	$grades = $T103->getGradeByFC($_GET['filiere'], $_GET['categorie']);
	if(count($grades) > 0)
	{
		$aGrades = array();
		foreach($grades as $grade)
		{
			
			$aGrades[] = array($grade->T103_codeinterne_i, $grade->T103_grade_va);
		}
		echo json_encode($aGrades);
	}
	else
	{
		echo "false";
	}
	
?>