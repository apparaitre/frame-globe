<?php
	
	define("_PROFIL_ACCESS", "FRAME-GLOBE");
	
	include ('../includes/init.php');
	
	// set variables
	$dir_dest = _VHOST_PATH . "assets/" . $_POST['dir']  ;
	$dir_pics = "assets/" . $_POST['dir'];

    // ---------- XMLHttpRequest UPLOAD ----------

    // we first check if it is a XMLHttpRequest call
    if (isset($_SERVER['HTTP_X_FILE_NAME']) && isset($_SERVER['CONTENT_LENGTH'])) 
    {
		// we create an instance of the class, feeding in the name of the file
        // sent via a XMLHttpRequest request, prefixed with 'php:'
        $handle = new Upload('php:'.$_SERVER['HTTP_X_FILE_NAME']);  

    }
    else
    {  
        // we create an instance of the class, giving as argument the PHP object
        // corresponding to the file field from the form
        // This is the fallback, using the standard way
        $handle = new Upload($_FILES['fichier']);
		
    }
    
    // then we check if the file has been uploaded properly
    // in its *temporary* location in the server (often, it is /tmp)
    if ($handle->uploaded)
    {
	
		switch($_POST['dir'])
		{
			case "logos":
				$handle->allowed = array('image/*');
			
				if(file_exists($dir_dest . "/" . md5($_POST['id_user']) . "/logo.png"))
				{
					@unlink($dir_dest . "/" . md5($_POST['id_user']) ."/logo.png");
				}
				// yes, the file is on the server
				// below are some example settings which can be used if the uploaded file is an image.
				$handle->image_resize            = true;
				$handle->image_ratio_y           = true;
				$handle->image_x         		 = 200;
				$handle->image_y         		 = 200;
				$handle->image_convert = 'png';
				$handle->file_new_name_body = "logo";
				$handle->png_compression = 5;

				$handle->Process($dir_dest. "/" . md5($_POST['id_user']));
       			if ($handle->processed) {
       				$result['type'] = "logos";
					$result['source'] = $dir_pics.'/' . md5($_POST['id_user']) . "/" . $handle->file_dst_name;
					$result['text'] =  '<b>Fichier envoyé avec succés (' .  round(filesize($handle->file_dst_pathname)/256)/4 . 'KB )</b><br />';
					$result['text'] .= '<p class="result">';
					$result['text'] .= '  <img src="'.$dir_pics.'/' . md5($_POST['id_user']) . '/' . $handle->file_dst_name . '" />';
					$result['text'] .=  '  Fichier: <a href="'.$dir_pics.'/' . md5($_POST['id_user']) . $handle->file_dst_name . '">' . $handle->file_dst_name . '</a><br/>';
					$result['text'] .=  '</p>';
       			}
       			else 
		        {
		           	$result['error'] = true;
					$result['text'] .=  '<p class="result">';
		            $result['text'] .=  '  <b>Le fichier n\'a pas pu être envoyé</b><br />';
		            $result['text'] .=  '  Erreur: ' . $handle->error . '';
		            $result['text'] .=  '</p>';
		        }
		        $handle-> Clean();

				break;

			case "CV":
				$handle->allowed = array('application/pdf');
				if(file_exists($dir_dest . "/" . md5($_POST['id_user']) . "/" . fileSafe($_FILES['fichier']['name'])))
				{
					@unlink($dir_dest . "/" . md5($_POST['id_user']) . "/" . fileSafe($_FILES['fichier']['name']));
				}
				$T119->deleteByUserType($_POST['id_user'], "CV");

				$id = $T119->insert($_POST['id_user'], "CV", "CV", fileSafe($_FILES['fichier']['name']));
				
				$handle->Process($dir_dest. "/" . md5($_POST['id_user']));
       			if ($handle->processed) {
	       			$result['type'] = "CV";
					$result['text'] =  '<b>Fichier ' . fileSafe($_FILES['fichier']['name']) . ' envoyé avec succés (' .  round(filesize($handle->file_dst_pathname)/256)/4 . 'KB )</b><br />';
					$_SESSION['contexte']['cv'][0] = $id;
					$result['liste_cv'] = '<ul class="puce-off"><li><a href="assets/CV/' . md5($_POST['id_user']) . '/' . $handle->file_dst_name . '"><img src="assets/mimetype/pdf.png" alt="pdf.png"><span class="pl3">' . $_POST['libelle'] . '</span></a> <a href="?delete&amp;line=0" title="Supprimer le CV" class="right icon"><img src="app/templates/images/icons/icon_delete.png" alt="Supprimer le CV"></a></li></ul>';
				}
				else 
		        {
		           	$result['error'] = true;
					$result['text'] .=  '<p class="result">';
		            $result['text'] .=  '  <b>Le fichier n\'a pas pu être envoyé</b><br />';
		            $result['text'] .=  '  Erreur: ' . $handle->error . '';
		            $result['text'] .=  '</p>';
		        }
		         $handle-> Clean();

				break;
				
			case "pub":
				$handle->allowed = array('image/*');

				if(file_exists($dir_dest . "/" . md5("pub" . $_POST['id_annonceur']) . "/" . fileSafe($_FILES['banniere']['name'])))
				{
					@unlink($dir_dest . "/" . md5("pub" . $_POST['id_annonceur']) . "/" . fileSafe($_FILES['banniere']['name']));
				}
				
				$T119->deleteByUserType($_POST['id_annonceur'], "CV");
				$id = $T119->insert($_POST['id_user'], "CV", $_POST['libelle'], fileSafe($_FILES['banniere']['name']));

				$handle->Process($dir_dest. "/" . md5("pub" . $_POST['id_annonceur']));
       			if ($handle->processed) {
       				$result['type'] = "pub";
					$result['source'] = $dir_pics.'/' . md5("pub" . $_POST['id_annonceur']) . "/" . $handle->file_dst_name;
					$result['text'] =  '<b>Fichier envoyé avec succés (' .  round(filesize($handle->file_dst_pathname)/256)/4 . 'KB )</b><br />';
					$result['text'] .= '<p class="result">';
					$result['text'] .= '  <img src="'.$dir_pics.'/' . md5("pub" . $_POST['id_annonceur']) . '/' . $handle->file_dst_name . '" />';
					$result['text'] .=  '  Fichier: <a href="'.$dir_pics.'/' . md5("pub" . $_POST['id_annonceur']) . $handle->file_dst_name . '">' . $handle->file_dst_name . '</a><br/>';
					$result['text'] .=  '</p>';
       			}
       			else 
		        {
		        	$result['error'] = true;
					$result['text'] .=  '<p class="result">';
		            $result['text'] .=  '  <b>Le fichier n\'a pas pu être envoyé</b><br />';
		            $result['text'] .=  '  Erreur: ' . $handle->error . '';
		            $result['text'] .=  '</p>';
		        }
		         $handle-> Clean();
				
			
		}
    }
    else
    {
        // if we're here, the upload file failed for some reasons
        // i.e. the server didn't receive the file
        $result['error'] = true;
        $result['text'] = '<p class="result">';
        $result['text'] .=  '  <b>Le fichier n\'a pas pu être envoyé</b><br />';
        $result['text'] .=  '  Erreur: ' . $handle->error . '';
        $result['text'] .=  '</p>';
    }

echo json_encode($result);