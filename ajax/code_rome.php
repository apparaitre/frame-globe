<?php
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");
	
	define("_PROFIL_ACCESS", "FRAME-GLOBE");
	include ('../includes/init.php');
	
	$recherche = validInputData($_GET['term'], "string") ;
		
	if(isset($recherche))
	{
		$metiers = $T117->getMetierByRome('%' . $recherche . '%');
		if(count($metiers) == 0 )
		{
			$metiers = $T117->getRomeByMetier('%' . $recherche . '%');
		}
		
		foreach($metiers  as $metier)
		{
			$results[] = array('codeinterne' => $metier->T117_codeinterne_i, 'code_rome' => $metier->T117_codeRome_va, 'libelle' => $metier->T117_libelle_va);
		}
		echo json_encode($results);
	}
?>