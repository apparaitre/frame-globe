<?php
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");
	
	define("_PROFIL_ACCESS", "FRAME-GLOBE");
	include ('../includes/init.php');
	if(isset($_GET['departement']))
	{
		$communes = $T100->getCommunesByDepartement($_GET['departement']);
	}
	elseif (isset($_GET['code_postal']))
	{
		$communes = $T100->getCommunesByZipCode($_GET['code_postal']);

	}
	else
	{
		echo "it should not happened!";
	}
	if(count($communes) > 0)
	{
		$aCommunes = array();
		foreach($communes as $commune)
		{
			
			$aCommunes[] = array($commune->T100_codeinterne_i, $commune->T100_ville_va);
		}
		echo json_encode($aCommunes);
	}
	else
	{
		echo "false";
	}
	
?>