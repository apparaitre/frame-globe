<?php
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");
	
	define("_PROFIL_ACCESS", "FRAME-GLOBE");
	include ('../includes/init.php');

	$filtre_theme = json_decode($_GET['themes']);
	$filtre_public = json_decode($_GET['public']);

	$partners = $T125->getPartenaires();
	
	if(!empty($partners))
	{
		foreach($partners as $partner)
		{
			$theme_partner = $T128->getByPartner($partner->T125_codeinterne_i);
			$public_partner = $T129->getByPartner($partner->T125_codeinterne_i);
			$themes = null;
			$public = null;
			foreach($theme_partner as $tp_item)
			{
				$themes[] = $tp_item->T126_codeinterne_i;
			}

			foreach($public_partner as $pb_item)
			{
				$public[] = $pb_item->T127_codeinterne_i;
			}

			if(array_intersect($filtre_theme, $themes) || array_intersect($filtre_public, $public) || (count($filtre_theme)==0 && count($filtre_public)==0 ))
			{
				$liste_partners[] = array (
					'code_interne' 	=> $partner->T125_codeinterne_i,
					'title' 		=> $partner->T125_raison_sociale_va,
					'content' 		=> "",
					'address' 		=> $partner->T125_adresse1_va . " " . $partner->T125_adresse2_va . " " . $partner->T125_code_postal_va . " " . $partner->T100_ville_va . ", France",
					'email' 		=> $partner->T125_email_va,
					'themes' 		=> $themes,
					'public' 		=> $public,
					'type' 			=> $partner->T125_type_va
				);
			}
			
		}
	}
	echo json_encode($liste_partners);
	
	
?>