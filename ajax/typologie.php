<?php
	session_start();
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");
	
	define("_PROFIL_ACCESS", "FACTURATION");
	include ('../includes/init.php');
	
	$val_1 = $_REQUEST['val_1'];
	$val_2 = $_REQUEST['val_2'];
	
	switch ($_REQUEST['by'])
	{
		case "soitc":
			if($val_1 != "" && $val_2 != "")
			{
				$typologies = $T107->getTypologieBySOITC($val_1, $val_2);
			}
			elseif($val_1 != "" && $val_2 == "")
			{
				$typologies = $T107->getTypologieBySOITCMetier($val_1);
			}
			break;
			
		case "ijti":
			$typologies = $T107->getTypologieByIJTI($val_1);
			break;
			
		case "intelligences":
		
			$typologies = $T107->getTypologieByIntelligence($val_1, $val_2);
			break;
	}
	
	if(count($typologies) > 0)
	{
		foreach($typologies as $typologie)
		{
			
			$ret[] = array(
				'id' => $typologie->T107_codeinterne_i, 
				'metier' => $typologie->metier_libelle, 
				'fonction' => $typologie->fonction_libelle,
				'ijti' => $typologie->T109_libelle_va,
				'mots_cles' => $typologie->T114_mots_cles_va,
				'iStyle' => $typologie->iStyle, 
				'iPref' => $typologie->iPref
			);
		}
		
		
		
		echo json_encode($ret);
	}
	else
	{
		echo "false";
	}

	function unique($array, $cles)
	{
		
		foreach($array as $item)
		{
			foreach ($cles as $cle => $verif)
			{
				if($verif !== null)
				{
					if ($item[$cle] == $verif)
					{
						return false;
					}
				}
			}
		}
		return true;
	}
?>