<?php
	session_start();
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");
	
	define("_PROFIL_ACCESS", "FACTURATION");
	include ('../includes/init.php');
	
	$intelligences = $T110->getByStyle($_GET['istyle']);
	if(count($intelligences) > 0)
	{
		$ret[] = array("", "Fonction préférentielle");
		foreach($intelligences as $intelligence)
		{
			$ret[] = array($intelligence->T114_codeinterne_i, $intelligence->T114_libelle_va);
		}
		
		echo json_encode($ret);
	}
	else
	{
		echo "false";
	}

?>