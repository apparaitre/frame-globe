<?php
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	header("Cache-Control: no-cache");
	header("Pragma: no-cache");
	define("_PROFIL_ACCESS", "HUBWIN");
	include ('../includes/init.php');
	$term = htmlentities($_GET["q"]) .'%';
	$fonction = htmlentities($_GET["fonction"]);
	$keywords = $T06->getMotCleByMC($term);
    //build array of results  
    foreach ($keywords as $keyword){  
        
        $aKw[] = array("T06_value_va" => $keyword->T06_value_va);  
    }  
    
   # JSON-encode the response
	$json_response = json_encode($aKw);

	# Optionally: Wrap the response in a callback function for JSONP cross-domain support
	if($_GET["callback"]) {
		$json_response = $_GET["callback"] . "(" . $json_response . ")";
	}

	# Return the response
	echo $json_response;
?>