-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le : Lun 11 Août 2014 à 08:10
-- Version du serveur: 5.5.37
-- Version de PHP: 5.3.10-1ubuntu3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `open_ce`
--

-- --------------------------------------------------------

--
-- Structure de la table `t00_url`
--

CREATE TABLE IF NOT EXISTS `t00_url` (
  `T00_codeinterne_i` bigint(20) NOT NULL AUTO_INCREMENT,
  `T00_url_va` varchar(255) DEFAULT NULL,
  `T00_role_va` varchar(255) DEFAULT NULL,
  `T00_application_va` varchar(255) DEFAULT NULL,
  `T00_rubrique_va` varchar(255) DEFAULT NULL,
  `T00_page_va` varchar(255) DEFAULT NULL,
  `T00_locale_va` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`T00_codeinterne_i`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=234 ;

--
-- Contenu de la table `t00_url`
--

INSERT INTO `t00_url` (`T00_codeinterne_i`, `T00_url_va`, `T00_role_va`, `T00_application_va`, `T00_rubrique_va`, `T00_page_va`, `T00_locale_va`) VALUES
(184, 'admin', NULL, 'backend', 'accueil', '', 'FR'),
(185, 'accueil_accueil', NULL, 'backend', 'accueil', 'accueil', 'FR'),
(186, 'utilisateurs', NULL, 'backend', 'utilisateurs', '', 'FR'),
(187, 'utilisateurs_liste', NULL, 'backend', 'utilisateurs', 'liste', 'FR'),
(188, 'utilisateurs_ajouter_utilisateur', NULL, 'backend', 'utilisateurs', 'ajouter_utilisateur', 'FR'),
(189, 'utilisateurs_editer_utilisateur', NULL, 'backend', 'utilisateurs', 'editer_utilisateur', 'FR'),
(190, 'utilisateurs_supprimer_utilisateur', NULL, 'backend', 'utilisateurs', 'supprimer_utilisateur', 'FR'),
(191, 'blog', NULL, 'backend', 'blog', '', 'FR'),
(192, 'blog_liste_rubrique', NULL, 'backend', 'blog', 'liste_rubrique', 'FR'),
(193, 'blog_liste_articles', NULL, 'backend', 'blog', 'liste_articles', 'FR'),
(194, 'blog_ajouter_rubrique', NULL, 'backend', 'blog', 'ajouter_rubrique', 'FR'),
(195, 'blog_editer_rubrique', NULL, 'backend', 'blog', 'editer_rubrique', 'FR'),
(196, 'blog_supprimer_rubrique', NULL, 'backend', 'blog', 'supprimer_rubrique', 'FR'),
(197, 'blog_ajouter_article', NULL, 'backend', 'blog', 'ajouter_article', 'FR'),
(198, 'blog_editer_article', NULL, 'backend', 'blog', 'editer_article', 'FR'),
(199, 'blog_supprimer_article', NULL, 'backend', 'blog', 'supprimer_article', 'FR'),
(200, 'cms', NULL, 'backend', 'cms', '', 'FR'),
(201, 'cms_liste', NULL, 'backend', 'cms', 'liste', 'FR'),
(202, 'cms_ajouter_rubrique', NULL, 'backend', 'cms', 'ajouter_rubrique', 'FR'),
(203, 'cms_ajouter_article', NULL, 'backend', 'cms', 'ajouter_article', 'FR'),
(204, 'cms_editer_rubrique', NULL, 'backend', 'cms', 'editer_rubrique', 'FR'),
(205, 'cms_editer_article', NULL, 'backend', 'cms', 'editer_article', 'FR'),
(206, 'cms_supprimer_rubrique', NULL, 'backend', 'cms', 'supprimer_rubrique', 'FR'),
(207, 'cms_supprimer_article', NULL, 'backend', 'cms', 'supprimer_article', 'FR'),
(208, 'accueil', NULL, 'app', 'accueil', '', 'FR'),
(209, 'accueil_accueil', NULL, 'app', 'accueil', 'accueil', 'FR'),
(210, 'comite_entreprise', NULL, 'app', 'comite_entreprise', '', 'FR'),
(211, 'comite_entreprise_ce', NULL, 'app', 'comite_entreprise', 'ce', 'FR'),
(212, 'comite_entreprise_mutuelle', NULL, 'app', 'comite_entreprise', 'mutuelle', 'FR'),
(213, 'comite_entreprise_gic', NULL, 'app', 'comite_entreprise', 'gic', 'FR'),
(214, 'comite_entreprise_formation', NULL, 'app', 'comite_entreprise', 'formation', 'FR'),
(215, 'comite_entreprise_reglement_interieur', NULL, 'app', 'comite_entreprise', 'reglement_interieur', 'FR'),
(216, 'comite_entreprise_bulletin_salaire', NULL, 'app', 'comite_entreprise', 'bulletin_salaire', 'FR'),
(217, 'comite_entreprise_aide_salarie', NULL, 'app', 'comite_entreprise', 'aide_salarie', 'FR'),
(218, 'comite_entreprise_representation_personnel', NULL, 'app', 'comite_entreprise', 'representation_personnel', 'FR'),
(219, 'comite_entreprise_organigramme', NULL, 'app', 'comite_entreprise', 'organigramme', 'FR'),
(220, 'comite_entreprise_evolution_effectifs', NULL, 'app', 'comite_entreprise', 'evolution_effectifs', 'FR'),
(221, 'loisirs', NULL, 'app', 'loisirs', '', 'FR'),
(222, 'loisirs_accueil', NULL, 'app', 'loisirs', 'accueil', 'FR'),
(223, 'loisirs_cinema', NULL, 'app', 'loisirs', 'cinema', 'FR'),
(224, 'loisirs_piscine', NULL, 'app', 'loisirs', 'piscine', 'FR'),
(225, 'loisirs_culture_sport', NULL, 'app', 'loisirs', 'culture_sport', 'FR'),
(226, 'loisirs_noel', NULL, 'app', 'loisirs', 'noel', 'FR'),
(227, 'loisirs_sortie_spectacle', NULL, 'app', 'loisirs', 'sortie_spectacle', 'FR'),
(228, 'loisirs_mobil_home', NULL, 'app', 'loisirs', 'mobil_home', 'FR'),
(229, 'loisirs_promos', NULL, 'app', 'loisirs', 'promos', 'FR'),
(230, 'contact', NULL, 'app', 'contact', '', 'FR'),
(231, 'contact_accueil', NULL, 'app', 'contact', 'accueil', 'FR'),
(232, 'divers', NULL, 'app', 'divers', '', 'FR'),
(233, 'divers_contact', NULL, 'app', 'divers', 'contact', 'FR');

-- --------------------------------------------------------

--
-- Structure de la table `t01_users`
--

CREATE TABLE IF NOT EXISTS `t01_users` (
  `T01_codeinterne_i` bigint(20) NOT NULL AUTO_INCREMENT,
  `T02_codeinterne_i` bigint(20) NOT NULL,
  `T01_login_va` varchar(255) DEFAULT NULL,
  `T01_password_va` varchar(255) DEFAULT NULL,
  `T01_email_va` varchar(255) DEFAULT NULL,
  `T01_lastlog_d` date DEFAULT NULL,
  `T01_locale_va` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`T01_codeinterne_i`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `t01_users`
--

INSERT INTO `t01_users` (`T01_codeinterne_i`, `T02_codeinterne_i`, `T01_login_va`, `T01_password_va`, `T01_email_va`, `T01_lastlog_d`, `T01_locale_va`) VALUES
(1, 2, 'phracktale', '8acd77c6bb36fdb90cbada77e9f8d7ad', 'phracktale@gmail.com', NULL, 'FR');

-- --------------------------------------------------------

--
-- Structure de la table `t02_role`
--

CREATE TABLE IF NOT EXISTS `t02_role` (
  `T02_codeinterne_i` bigint(20) NOT NULL AUTO_INCREMENT,
  `T02_role_va` varchar(255) NOT NULL,
  `T02_application_va` varchar(255) NOT NULL,
  PRIMARY KEY (`T02_codeinterne_i`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `t02_role`
--

INSERT INTO `t02_role` (`T02_codeinterne_i`, `T02_role_va`, `T02_application_va`) VALUES
(1, 'public', 'app'),
(2, 'admin', 'backend');

-- --------------------------------------------------------

--
-- Structure de la table `t03_profil`
--

CREATE TABLE IF NOT EXISTS `t03_profil` (
  `T03_codeinterne_i` bigint(20) NOT NULL AUTO_INCREMENT,
  `T01_codeinterne_i` bigint(20) NOT NULL,
  `T03_civilite` varchar(255) DEFAULT NULL,
  `T03_prenom` varchar(255) DEFAULT NULL,
  `T03_nom_va` varchar(255) DEFAULT NULL,
  `T03_adresse1_va` varchar(255) NOT NULL,
  `T03_adresse2_va` varchar(255) NOT NULL,
  `T03_codepostal_va` varchar(255) NOT NULL,
  `T03_ville_va` varchar(255) NOT NULL,
  `T03_pays_va` varchar(255) NOT NULL,
  `T03_telephone_va` varchar(255) NOT NULL,
  PRIMARY KEY (`T03_codeinterne_i`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `t04_dico`
--

CREATE TABLE IF NOT EXISTS `t04_dico` (
  `T04_codeinterne_i` bigint(20) NOT NULL AUTO_INCREMENT,
  `T04_section_va` varchar(255) NOT NULL,
  `T04_code_va` varchar(255) NOT NULL,
  `T04_locale_va` varchar(2) NOT NULL,
  `T04_libelle_va` varchar(255) NOT NULL,
  PRIMARY KEY (`T04_codeinterne_i`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `t04_dico`
--

INSERT INTO `t04_dico` (`T04_codeinterne_i`, `T04_section_va`, `T04_code_va`, `T04_locale_va`, `T04_libelle_va`) VALUES
(1, 'role', 'public', 'fr', 'Public'),
(2, 'role', 'admin', 'fr', 'Administateur');

-- --------------------------------------------------------

--
-- Structure de la table `t05_rubriques`
--

CREATE TABLE IF NOT EXISTS `t05_rubriques` (
  `T05_codeinterne_i` bigint(20) NOT NULL AUTO_INCREMENT,
  `T05_tag_va` varchar(255) NOT NULL,
  `T05_titre_va` varchar(255) DEFAULT NULL,
  `T05_texte_va` text,
  `T05_date_d` datetime DEFAULT NULL,
  PRIMARY KEY (`T05_codeinterne_i`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `t06_articles`
--

CREATE TABLE IF NOT EXISTS `t06_articles` (
  `T06_codeinterne_i` bigint(20) NOT NULL AUTO_INCREMENT,
  `T05_codeinterne_i` bigint(20) NOT NULL,
  `T06_type_va` varchar(255) NOT NULL,
  `T06_url_va` varchar(255) NOT NULL,
  `T06_titre_va` varchar(255) NOT NULL,
  `T06_chapo_va` text NOT NULL,
  `T06_texte_va` text NOT NULL,
  `T06_date_d` datetime NOT NULL,
  PRIMARY KEY (`T06_codeinterne_i`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
