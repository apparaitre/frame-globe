<?php
	if (!defined("_PROFIL_ACCESS")){header("location:../erreur");}
	## Profil public
	#
	#	Routage des fonctionnalités du role public
	#
	#	Attention ! Ce fichier ne doit jamais être édité
	
	$soustitre = "";
	
	switch($_SESSION["contexte"]["rubrique"])
	{
		default:
						
		case "accueil":
			$titre = "Accueil";
			$soustitre = "";
			$page_id = "accueil";
			include("public_accueil.php");
			
			break;
			
		case "comite_entreprise":
			$titre = "le comité d'entreprise";
			$soustitre = "";
			$page_id = "comite_entreprise";
			include("public_comite_entreprise.php");
			
			break;
			
		case "loisirs":
			$titre = "Les loisirs";
			$soustitre = "";
			$page_id = "loisirs";
			include("public_loisirs.php");
			
			break;
			
		case "contact":
			$titre = "Nous contacter";
			$soustitre = "";
			$page_id = "contact";
			include("public_contact.php");
			
			break;
			
		case "divers":
			$titre = "divers";
			$soustitre = "";
			$page_id = "divers";
			include("public_divers.php");
			
			break;
			
	}
	
	$meta_title = isset($metas[$page_id]["title"])?$metas[$page_id]["title"]:"";
	$meta_keywords = isset($metas[$page_id]["keywords"])?$metas[$page_id]["keywords"]:"";
	$meta_description = isset($metas[$page_id]["description"])?$metas[$page_id]["description"]:"";
	
	$templateDir = "app/templates/";
	$templateFile = isset($_GET["popin"]) ? "popin.html" : "main_public.html"; 
	$tmpVars = array("/{SITE_NAME}/i", "/{SITE_BASELINE}/i", "/{CONTENT}/i", "/{MENU_PRINCIPAL}/i", "/{SOUS_MENU}/i", "/{TITRE}/i", "/{SOUSTITRE}/i", "/{INFOS_CONNEXION}/i", "/{REDIRECT}/i","/{META_TITLE}/i", "/{META_KEYWORDS}/i", "/{META_DESCRIPTION}/i", "/{BROADCAST}/i");
	$replace = array(_SITE_NAME, _SITE_BASELINE, $html, $menu_principal, $sous_menu[$_SESSION["contexte"]["rubrique"]], $titre, $soustitre, infos_connexion($menu[$_SESSION["user"]["groupe"]]["item1"]["code"]), $_SERVER["REQUEST_URI"], $meta_title, $meta_keywords, $meta_description, $BROADCAST);
	
	/* Hook injection variables dans layout principal */
	$hook_file= _VHOST_PATH . $_SESSION['app'] . '/hook/main_layout_vars.php';
	if(file_exists($hook_file))
	{
		include($hook_file);
	}
	/* Hook injection variables dans layout principal */
	
	$page = new Page($templateDir, $templateFile, $_SESSION["locale"]);
	$page->replace_tags($tmpVars, $replace);
	echo $page->output();
	exit;
?>