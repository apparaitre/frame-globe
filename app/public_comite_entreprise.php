<?php
	if (!defined("_PROFIL_ACCESS")){header("location:../erreur");}
	
	//switch($_SESSION["contexte"][$_SESSION["contexte"]["rubrique"]]["page"])
	//$page = isset($_GET["page"])?$_GET["page"]:"";
	switch($_PAGE)
	{
		default:
		
		case "ce":
			include("actions/public/comite_entreprise_ce.php");
			break;
			
		case "mutuelle":
			include("actions/public/comite_entreprise_mutuelle.php");
			break;
			
		case "gic":
			include("actions/public/comite_entreprise_gic.php");
			break;
			
		case "formation":
			include("actions/public/comite_entreprise_formation.php");
			break;
			
		case "reglement_interieur":
			include("actions/public/comite_entreprise_reglement_interieur.php");
			break;
			
		case "bulletin_salaire":
			include("actions/public/comite_entreprise_bulletin_salaire.php");
			break;
			
		case "aide_salarie":
			include("actions/public/comite_entreprise_aide_salarie.php");
			break;
			
		case "representation_personnel":
			include("actions/public/comite_entreprise_representation_personnel.php");
			break;
			
		case "organigramme":
			include("actions/public/comite_entreprise_organigramme.php");
			break;
			
		case "evolution_effectifs":
			include("actions/public/comite_entreprise_evolution_effectifs.php");
			break;
			
	}
	
	$page = new Page($templateDir, $templateFile,  $_SESSION["locale"]);
	$page->setVars($dynamic_vars);
	
	$page->replace_tags($tmpVars, $replace);
	$html = $page->output();
	
?>