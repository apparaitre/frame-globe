<?php
	$page = new Page();
	if (!defined("_PROFIL_ACCESS")){header("location:../erreur");}
	 $menu = array (
						
						'public' => array (
											"item1"	=> array( 
																"code" => "accueil",
																"class" => "", 
																"libelle" => $page->_T("libMenuAccueil"),
																"sousmenu" => array(
													
															)
													),
											"item2"	=> array( 
																"code" => "comite_entreprise",
																"class" => "", 
																"libelle" => $page->_T("libMenuComite_entreprise"),
																"sousmenu" => array(
																				"item2-1" => array( "code" => "mutuelle", "class" => "", "libelle" => $page->_T("libMenuComite_entrepriseMutuelle")),
																					"item2-2" => array( "code" => "gic", "class" => "", "libelle" => $page->_T("libMenuComite_entrepriseGic")),
																					"item2-3" => array( "code" => "formation", "class" => "", "libelle" => $page->_T("libMenuComite_entrepriseFormation")),
																					"item2-4" => array( "code" => "reglement_interieur", "class" => "", "libelle" => $page->_T("libMenuComite_entrepriseReglement_interieur")),
																					"item2-5" => array( "code" => "bulletin_salaire", "class" => "", "libelle" => $page->_T("libMenuComite_entrepriseBulletin_salaire")),
																					"item2-6" => array( "code" => "aide_salarie", "class" => "", "libelle" => $page->_T("libMenuComite_entrepriseAide_salarie")),
																					"item2-7" => array( "code" => "organigramme", "class" => "", "libelle" => $page->_T("libMenuComite_entrepriseOrganigramme")),
																					"item2-8" => array( "code" => "evolution_effectifs", "class" => "", "libelle" => $page->_T("libMenuComite_entrepriseEvolution_effectifs")),
														
															)
													),
											"item3"	=> array( 
																"code" => "loisirs",
																"class" => "", 
																"libelle" => $page->_T("libMenuLoisirs"),
																"sousmenu" => array(
																				"item3-1" => array( "code" => "cinema", "class" => "", "libelle" => $page->_T("libMenuLoisirsCinema")),
																					"item3-2" => array( "code" => "piscine", "class" => "", "libelle" => $page->_T("libMenuLoisirsPiscine")),
																					"item3-3" => array( "code" => "culture_sport", "class" => "", "libelle" => $page->_T("libMenuLoisirsCulture_sport")),
																					"item3-4" => array( "code" => "noel", "class" => "", "libelle" => $page->_T("libMenuLoisirsNoel")),
																					"item3-5" => array( "code" => "sortie_spectacle", "class" => "", "libelle" => $page->_T("libMenuLoisirsSortie_spectacle")),
																					"item3-6" => array( "code" => "mobil_home", "class" => "", "libelle" => $page->_T("libMenuLoisirsMobil_home")),
																					"item3-7" => array( "code" => "promos", "class" => "", "libelle" => $page->_T("libMenuLoisirsPromos")),
														
															)
													),
											"item4"	=> array( 
																"code" => "contact",
																"class" => "", 
																"libelle" => $page->_T("libMenuContact"),
																"sousmenu" => array(
													
															)
													)
					),
		);
				
		$make_menu = function($user_groupe)
		{
			global $public_role, $menu;
			if(isset($user_groupe))
			{
							$menu_route["public"] = "local";

				if($menu_route[$user_groupe] == "local")
				{
					$role =  $user_groupe;
					$public = "";
				}
				else
				{if($public_role != "")
				{
					$role = $public_role ;
					$public = $public_role;
				}
				else
				{
					$role =  $user_groupe;
					$public = "";
				}}
			$menu_role = $menu[$role];
			if(count($menu_role) > 0)
			{
				$menu_principal = "<nav id=\"menu\"><ul class=\"menu\">";
				foreach ($menu_role as $item)
				{
					$rubrique = $item["code"];
					$active = $rubrique == $_SESSION["contexte"]["rubrique"] ? "on" : "off";
					$class = $item["class"] . " " . $active;
					
					$rubrique_url = new Url();
					$rubrique_url->setType($_SESSION["type_url"]);
					/*
					if($public_role != "" && $_SESSION["user"]["groupe"] != $public_role)
					{
						$rubrique_url->setParam("role", $public_role);
					}
					*/
					$item_url = $rubrique_url->getUrl($rubrique.",");
					
					$menu_principal .= "<li class=\"menu " . $item["code"] . " " . $class . "\"><a href=\"" . $item_url . "\" ><span class=\"lib\">" . $item["libelle"] . "</span></a>";
					if(count($item["sousmenu"]) > 0)
					{
						$sous_menu[$rubrique] .= "<ul class=\"sousmenu\">";
						foreach ($item["sousmenu"] as $ssitem)
						{
							$ssitem_url = new Url();
							$ssitem_url->setType($_SESSION["type_url"]);
							/*
							if($public_role != "" && $_SESSION["user"]["groupe"] != $public_role)
							{
								$role = $public_role ."@";
							}
							*/
							$page_url = $ssitem_url->getUrl($role . $rubrique . "," . $ssitem["code"]);
							
							$ssactive = $ssitem["code"] == $_SESSION["contexte"][$_SESSION["contexte"]["rubrique"]]["page"] ? "on" : "off";
							$class = $ssitem["class"] . " " . $ssactive;
							$sous_menu[$rubrique] .= "<li class=\"" . $ssitem["code"] . " " . $class . "\"><a href=\"" . $page_url . "\" ><span class=\"lib\">" . $ssitem["libelle"] . "</span></a></li>";
							
						}
					
						$sous_menu[$rubrique] .= "</ul>";
					}
					$menu_principal .= $sous_menu[$rubrique] . "</li>";
				}
				$menu_principal .= "</ul></nav>";
			}
		}
		return $menu_principal;
	}
?>