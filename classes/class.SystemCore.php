<?php
/*
 * CMS_DIRECT
 * 
 * class.SystemCore.php
 * Created on 16 mars 08 by phracktale
 * 
 * 
 */
 // Classe Système. Peut intéragir avec le Core.
class SystemCore extends Core {
#
// Instance unique du Système.
private static $_SystemInstance;
#
// Identifie le système dans le Core.
protected function __construct() {
parent::__construct();
}
#
// Instance unique.
public static function GetInstance() {
if ( !isset(self::$_SystemInstance) ) self::$_SystemInstance = new self;
return self::$_SystemInstance;
}
#
// Recherche de l'activation ou non d'un module dans le Core.
public function isActive($modulename) {
$mod = $this->GetCoreInfos('module',$modulename);
return $mod['actif'];
}
#
// Pré-initialise un module dans le corps. (Evite le parsing du fichier xml)
public function PreInit($modulename) {
$this->SetCoreInfos('module', $modulename, 'actif', TRUE);
/* if ( $this->debug ) {
$this->newEntry('Pré-initialisation du module '.$modulename.' accomplie.');
}*/
}

} 
?>
