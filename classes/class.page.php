<?php
	use \Michelf\Markdown;

	/********************************************
	 * TEMPLATE   Version 1.00                  *
	 ********************************************
	 * Copyright 2003 - 2011  Thierry Cazalet   *
	 ********************************************
	 ********************************************
	 * class.template.php                       *
	 ********************************************
	 */
	 
	 
	/* 
	 * @package template
	 * @author Thierry Cazalet
	 * @Company phracktale
	 * @copyright 2003 - 2009 Thierry Cazalet
	 *
	 * La classe Page dispose de toutes les fonctions afin de gérer le système de template
	 *	
	 * @param $page  Sortie html de la page web
	 * @parem $replaceCount Valeur des variables globales de la template
	 */
	 
	class Page
	{
	   
		var $page;
		var $const;
		var $replaceConst;
		var $vars;
		var $locale;
		var $templateFile;
		var $templateDir;
		var $dico;
		
		/**
		 * Récupère les fichiers sur le serveur qui vont servir de template à l'affichage de l'application
		 * 1ère méthode
		 * $templateDir existe, $templateFile existe et $HF existe
		 * le template retourné sera du type :
		 * header + template + footer
		 * 3 fichiers distincts seront utilisé, un header et un footer pour le look général de la page
		 * et 1 fichier template pour mettre en forme le contenu de la page 
		 *
		 * 2ème méthode
		 * $templateDir existe, $templateFile existe mais pas $HF
		 * Le template retourné dépends uniquement d'un seul fichier de template spécifié dans $templateFile
		 *
		 * 3ème méthode (à supprimer)
		 * Même résultat que pour la 2ème méthode mais $templateDir = $templateDir.$templateFile
		 */


		public function Page($templateDir = "", $templateFile ="", $locale='FR') 
		{
			GLOBAL $datapath, $urlPublic, $urlPro, $dataUrl, $url, $const, $replaceConst;
			
			$this->locale = $locale;

			$this->initDico();

			if($templateDir == "" || $templateFile == "")
			{
				$this->page = "Template non renseignée";
			}
			else
			{
				include_once(_VHOST_PATH . 'classes/markdown/Markdown.php');
				include_once(_VHOST_PATH . 'classes/class.url.php');
							
				/*
				 * modules
				 * Insertion du hook module inject_rsc
				 */

				$module_hook = "inject_rsc";

				if ($handle = opendir(_MODULE_PATH)) {
				   while (false !== ($entry = readdir($handle))) {
				   		if ($entry != "." && $entry != "..") {
				   			if(is_file(_MODULE_PATH . "/" . $entry . "/module.php"))
				   			{
				   				include(_MODULE_PATH . "/" . $entry . "/module.php");
				   			}
					     }
				    }


				    closedir($handle);
				}			
				$this->templateFile =  $templateFile;
				$this->templateDir =  $templateDir;
				$const = array("/{FG_URL_SELF}/i", "/{URL}/i", "/{URLDATA}/i", "/{URLPUBLIC}/i", "/{URLPPRO}/i", "/{LOCALE}/i", "/{INSERT_JS}/i", "/{INSERT_CSS}/i");
				$replaceConst = array($_SERVER['REQUEST_URI'], $url, $dataUrl, $urlPublic, $urlPro, $locale, $module_scripts, $module_css);
				
				$this->locale=$locale;
				
				$template=$templateDir.$templateFile;
				
				if (file_exists($template))
				{
					
					$this->page = join('', file($template));
					
					// On remplace les #INCLURE => {#INCLURE=template}
					if(preg_match_all('/\{#INCLURE=(.*?)}/m', $this->page, $tmp))
					{
						for($k=0; $k<=count($tmp[0]) - 1; $k++)
						{
							$var[$k]='/'.$tmp[0][$k]."/i";
							
							if(file_exists($templateDir.$tmp[1][$k] . ".html"))
							{
								$replace[$k] = join('', file($templateDir.$tmp[1][$k] . ".html"));
							}
						}
						
						$this->page = preg_replace($var, $replace, $this->page);
						
						
					}
				}
				elseif (file_exists("default/erreur404.html"))
				{			
					$this->page = join('', file("default/erreur404.html"));
				}
				else
				{
					$this->page = "Cette page n'existe pas : " .$template;
				}
			}
		}
		public function setPage($page)
		{
			$this->page = $page;
		}
		
		public function addvar($var)
		{
				$this->$var['name'] = $var['value'];
		}
		
		public function getArray($arr, $index)
		{
			return $arr[$index];
		}
		
		public function replace_array($matches)
		{
			
			$tmp_arr = $this->$matches[1];
			return $tmp_arr[$matches[2]];
		}

		public function initDico()
		{
			GLOBAL $dataPath;
			
			$dicoFiles = array($this->locale . ".inc",  "menu." . $this->locale . ".inc", $_SESSION['app'] . "." . $this->locale . ".inc", );
			

			foreach ($dicoFiles as $dicoFile)
			{
				$dicoFile = $datPath . "traductions/" . $dicoFile;
				if (file_exists($dicoFile))
				{
					$contenu_array = file($dicoFile);
					$num_line=sizeof($contenu_array);
					while ( list( $numero_ligne, $ligne ) = each( $contenu_array ) ) 
					{
						$var = explode("=", $ligne);
						if(count($var) == 2)
						{
							$this->dico[$var[0]] = rtrim(str_replace("::", "=", $var[1]));
							
						}
					}
				}
			}
		}


		/*
		 *  Remplacement des variables template
		 *  @param array $tmpVars
		 *  @param array $replace
		 *  @return void
		 */
		public function replace_tags($tmpVars = array(), $replace = array()) 
		{
			GLOBAL $dataPath, $const, $replaceConst, $dataUrl;
			if(!isset($const))$const = array();
			if(!isset($replaceConst))$replaceConst = array();
			$tmpVars = array_merge($tmpVars, $const);
			$replace = array_merge($replace, $replaceConst);
			

			// Recherche des variables du dico dans les templates
			if( preg_match_all('/\{lib(.*?)}/m', $this->page, $tmpDico))
			{
				for($k=0; $k<count($tmpDico[0]); $k++)
				{
					$varDico[$k]='/'.$tmpDico[0][$k]."/i";
					$tmp='lib'.$tmpDico[1][$k];
					
				   if(isset($this->dico[$tmp])){ $replaceDico[$k]=$this->dico[$tmp];}else{ $replaceDico[$k] = null;}
				}
			}
			else
			{
				$varDico = array();
				$replaceDico = array();
			}
			
			$tmpVars = array_merge($varDico, $tmpVars);
			$replace = array_merge($replaceDico, $replace);
			
			
			$this->page = $this->parse($this->page);
			
			if(isset($_GET['dgbTemplate'])){
				echo "\n*************************************************************************************************************************";
				echo "\n".$this->templateFile;
				echo "\n*************************************************************************************************************************";
				print_r($tmpVars);
				print_r($replace);
				echo "\n*************************************************************************************************************************";
				echo "\n*************************************************************************************************************************";
			}
			
			if($tmpVars != ""  || $replace != "")
			{
				
				$this->page = preg_replace($tmpVars, $replace, $this->page);
			}
		}
		
		/*
		 *  Implantation du langage de template
		 *  @return $this->page
		 */
		private function parse($page)
		{
			global $T06;
			// Traitement des conditions {if variable == "quelquechose"}affiche ça{/if}
			// la condition doit être sur une même ligne (trouver pour le saut de ligne)
			
			
			if( preg_match_all('/\{if (.*?)}(.*?){\/if}/m', $page, $out))
			{
				for ($k=0; $k<count($out)+1; $k++)
				{
					$condition = $out[1][$k];

					if(preg_match('/(.*?) (.*?) \"(.*?)\"/s', $condition, $cond))
					{
						
						$var = $cond[1];
						$operateur = $cond[2];
						
						if(preg_match('/session\[(.*?)\]/', $var, $sess))
						{
							
							$resultat = $_SESSION[$sess[1]];
						}
						else
						{
							$var = '"'.$this->vars[$var].'"';
							$resultat = $cond[3];
						}
						
						if($var != "" || $resultat != "" || $operateur != "")
						{
						
							eval("\$test = $var $operateur \"$resultat\";");
							if($test)
							{
								$code = $out[2][$k];
							}
							else
							{
								$code = "";
							}
						}
						else
						{
							$code = "";
						}
					}
					$page = preg_replace('#{if (.*)}(.*){\/if}#', $code,  $page, 1);
				}
			}
			
			// Bibliothèque d'objets template
			/*
			$page = preg_replace('`{TEMPLATE src="blockheader" title="([^"]*)" class="([^"]*)"}`', '<div class="block"><div class="header"><div class="titre">$1</div></div><div id="$2">', $page);
			
			$page = preg_replace('`{TEMPLATE src="blockheader" title="([^"]*)" class="([^"]*)" width="([^"]*)"}`', '<div class="block" style="width:$3"><div class="header"><div class="titre">$1</div></div><div id="$2">', $page);
			
			$page = preg_replace('`{TEMPLATE src="blockfooter"}`', '</div><div class="footer">&nbsp;</div></div>', $page);
			
			$page = preg_replace('`{BUTTON name="([^"]*)" title="([^"]*)" value="([^"]*)" href="#" onclick="([^"]*)"}`', '<div class="inline" id="$1"  style="display:inline; cursor:hand" title="$2"><div  onclick="$4"><table cellpadding="0" cellspacing="0" border=0><tr><td width="10"></td><td><img src="'.$dataUrl.'design/images/'.$this->locale.'/btnLeft.gif" name="L$1"></td><td background="'.$dataUrl.'design/images/'.$this->locale.'/btnMid.gif" class="btnText" nowrap>$3</td><td><img src="'.$dataUrl.'design/images/'.$this->locale.'/btnRight.gif" name="R$1"></td></tr></table></div></div>', $page);
			
			$page = preg_replace('`{BUTTON name="([^"]*)" title="([^"]*)" value="([^"]*)" href="([^"]*)" onclick="([^"]*)"}`', '<div class="inline" id="$1"  style="display:inline; cursor:hand" title="$2"><div  onclick="document.location=\'$4\';$5"><table cellpadding="0" cellspacing="0" border=0><tr><td width="10"></td><td><img src="'.$dataUrl.'design/images/'.$this->locale.'/btnLeft.gif" name="L$1"></td><td background="'.$dataUrl.'design/images/'.$this->locale.'/btnMid.gif" class="btnText" nowrap>$3</td><td><img src="'.$dataUrl.'design/images/'.$this->locale.'/btnRight.gif" name="R$1"></td></tr></table></div></div>', $page);
				
			$page = preg_replace('`{BUTTON name="([^"]*)" title="([^"]*)" value="([^"]*)" href="([^"]*)"}`', '<div class="inline" id="$1"  style="display:inline; cursor:hand" title="$2"><div  onclick="document.location=\'$4\'"><table cellpadding="0" cellspacing="0" border=0><tr><td width="10"></td><td><img src="'.$dataUrl.'design/images/'.$this->locale.'/btnLeft.gif" name="L$1"></td><td background="'.$dataUrl.'design/images/'.$this->locale.'/btnMid.gif" class="btnText" nowrap>$3</td><td><img src="'.$dataUrl.'design/images/'.$this->locale.'/btnRight.gif" name="R$1"></td></tr></table></div></div>', $page);
			
			$page = preg_replace_callback('`{(.*)\[(.*)\]}`', Array($this,"replace_array"), $page);
			*/
			
			/*
			if(preg_match('/\$_SESSION\[(.*?)\]/m', $page, $sess))
			{
				$tempVar = $_SESSION[$sess[1]];
			}
			else
			{
				$tempVar = "";
			}
			*/
			
			if(preg_match('/{SESSION(.*?)}/m', $page, $sess))
			{
				eval('$sessionVar = $_SESSION' .$sess[1] .';');
				$page = preg_replace('#{SESSION(.*?)}#', $sessionVar,  $page, 1);
			}
			else
			{
				$tempVar = "";
			}
			
			/*
				Traitement des modules
				
			*/
			
			$var = null;
			// On remplace les #URL_PAGE => {#URL_PAGE=page}
			if(preg_match_all('/\{#URL_PAGE=(.*?)}/m', $page, $tag_tmp))
			{
				for($k=0; $k<=count($tag_tmp[0]) - 1; $k++)
				{
					$var[$k]='/'.$tag_tmp[0][$k]."/i";
					/* on recherche les pattern d'url possible
						role@rubrique,page
					*/
					if(preg_match('/(.*?)@(.*?),(.*?)/m', $tag_tmp[1][$k], $url_tmp))
					{
						$url = $tag_tmp[1][$k];
					}
					elseif(preg_match('/(.*?),(.*?)/m', $tag_tmp[1][$k], $url_tmp))
					{
						$url = $tag_tmp[1][$k];
					}
					elseif(preg_match('/(.*?)/m', $tag_tmp[1][$k], $url_tmp))
					{
						/* si un paramètre unique est donné à url_page c'est obligatoirement une page de la rubrique courante */
						$url = $_SESSION["contexte"]["rubrique"] . "," . $tag_tmp[1][$k];
					}
					
					$page_url = new Url();
					$page_url->setType($_SESSION['type_url']);
					$replace[$k] = $page_url->getUrl($url);
				
				}
				
				$page = preg_replace($var, $replace, $page);
			}
			// On remplace les #URL_RUBRIQUE => {#URL_RUBRIQUE=rubrique}
			if(preg_match_all('/\{#URL_RUBRIQUE=(.*?)}/m', $page, $tag_tmp))
			{
				for($k=0; $k<=count($tag_tmp[0]) - 1; $k++)
				{
					$var[$k]='/'.$tag_tmp[0][$k]."/i";
					
					$url = $tag_tmp[1][$k];
				
					$page_url = new Url();
					$page_url->setType($_SESSION['type_url']);
					$replace[$k] = $page_url->getUrl($url);
				
				}
				
				$page = preg_replace($var, $replace, $page);
			}
				
			/*
			 * modules
			 * Insertion du hook module parse_template
			 */

			$module_hook = "parse_template";

			if ($handle = opendir(_MODULE_PATH)) {
			   while (false !== ($entry = readdir($handle))) {
			   		if ($entry != "." && $entry != "..") {
			   			if(is_file(_MODULE_PATH . "/" . $entry . "/module.php"))
			   			{
			   				include(_MODULE_PATH . "/" . $entry . "/module.php");
			   			}
				    	
				     }
			    }


			    closedir($handle);
			}

			
			
				
			return $page;
		}
		
		public function setLocale($value)
		{
			$this->locale = $value;
		}
		
		public function setVars ($array)
		{
			$this->vars = $array;
		}
		
		/*
		 *  Sortie HTML
		 *  @return $this->page
		 */
		public function output() 
		{
			return $this->page;
		}

		public function _T($libelle , $variables = array())
		{

			if(isset($this->dico[$libelle]))
			{
				
				$trad = $this->dico[$libelle];
				foreach ($variables as $name => $value)
				{
					$trad = preg_replace("/@" . $name . "@/i", $value, $this->dico[$libelle]);
				}
			}
			else
			{
				$trad = $libelle;
			}
			return $trad;
		}
	}
?>