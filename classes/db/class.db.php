<?php    
/*     
 *  Classe DB     
 *  DB    
 *  @author: Thierry Cazalet    
 *	   
 *	Classe non générée    
 */	
class Query {
	var $db;
	function __construct($value)
	{				
		$this->db = $value;		
	}	
	 
	public function nextval($id_region) 		
	{			
		$query="select nextval('numDossier', ?) as valeur";
		$sth = $this->db->prepare($query);			
		$sth->execute(array($id_region));			
		$result = $sth->fetch(PDO::FETCH_OBJ);			
		return $result->valeur;		
	}	
	
	public function __call($query, $datas) 		
	{						
		if(isset($$query))			
		{				
			$sth = $this->db->prepare($$query);				
			$sth->execute($datas);				
			$result = $sth->fetchAll(PDO::FETCH_OBJ);				
			return $result;		
		}			
		else			
		{    			
			throw new queryException("La requète  $m est indéfinie");			
		}		
	}	
}
?>