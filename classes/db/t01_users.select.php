<?php
	$getCompteByID = "SELECT t01.*,  t02.*, t03.T03_civilite, t03.T03_prenom, t03.T03_nom_va, t03.T03_adresse1_va, t03.T03_adresse2_va, t03.T03_codepostal_va, t03.T03_ville_va, t03.T03_pays_va, t03.T03_telephone_va, t04a.T04_libelle_va as role_libelle
		FROM t01_users t01 
		LEFT JOIN t03_profil t03 on t01.T01_codeinterne_i=t03.T01_codeinterne_i 
		LEFT JOIN t02_role t02 ON t02.T02_codeinterne_i = t01.T02_codeinterne_i  
		LEFT JOIN t04_dico t04a on t04a.T04_code_va=t02.T02_role_va  
		WHERE t01.T01_codeinterne_i = ?  and t04a.T04_section_va='role' and t04a.T04_locale_va='" . $_SESSION['locale'] . "'";
		
	$getCompteByLogin = "SELECT t01.*, t02.*, t03.T03_civilite, t03.T03_prenom, t03.T03_nom_va, t03.T03_adresse1_va, t03.T03_adresse2_va, t03.T03_codepostal_va, t03.T03_ville_va, t03.T03_pays_va, t03.T03_telephone_va, t04.T04_libelle_va as role_libelle
	FROM t01_users t01 
	LEFT JOIN t03_profil t03 on t01.T01_codeinterne_i=t03.T01_codeinterne_i 
	LEFT JOIN t02_role t02 ON t02.T02_codeinterne_i = t01.T02_codeinterne_i  
	LEFT JOIN t04_dico t04 on t04.T04_code_va=t02.T02_role_va  
	WHERE t01.T01_login_va =  ?  and t04.T04_section_va='role' and t04.T04_locale_va='" . $_SESSION['locale'] . "'";
	
	
	$getCompteByEmail = "SELECT `t01`.`T01_codeinterne_i`, `t01`.`T02_codeinterne_i`, `t01`.`T01_login_va`, `t01`.`T01_password_va`, `t01`.`T01_email_va`, `t01`.`T01_lastlog_d`, `t01`.`T01_locale_va`,  t02.*, t03.T03_civilite, t03.T03_prenom, t03.T03_nom_va, t03.T03_adresse1_va, t03.T03_adresse2_va, t03.T03_codepostal_va, t03.T03_ville_va, t03.T03_pays_va, t03.T03_telephone_va, t04.T04_libelle_va as role_libelle FROM t01_users t01 
	LEFT JOIN t03_profil t03 on t01.T01_codeinterne_i=t03.T01_codeinterne_i 
	LEFT JOIN t02_role t02 ON t02.T02_codeinterne_i = t01.T02_codeinterne_i  
	LEFT JOIN t04_dico t04 on t04.T04_code_va=t02.T02_role_va  
	WHERE t01.T01_email_va = ?  and t04.T04_section_va='role' and t04.T04_locale_va='" . $_SESSION['locale'] . "'";
	
	
	$getCompteByToken ="SELECT `t01`.`T01_codeinterne_i`, `t01`.`T02_codeinterne_i`, `t01`.`T01_login_va`, `t01`.`T01_password_va`, `t01`.`T01_email_va`, `t01`.`T01_lastlog_d`, `t01`.`T01_locale_va`,  t03.T03_civilite, t03.T03_prenom, t03.T03_nom_va, t03.T03_adresse1_va, t03.T03_adresse2_va, t03.T03_codepostal_va, t03.T03_ville_va, t03.T03_pays_va, t03.T03_telephone_va from t01_users t01 
		left join t03_profil t03 on t01.T01_codeinterne_i = t03.T01_codeinterne_i
		where MD5(T01_login_va)=?";
	
	
		
?>