<?php
	$update = "UPDATE t03_profil set `T03_civilite_va`=?, `T03_prenom_va`=?, `T03_nom_va`=?, `T03_adresse1_va`=?, `T03_adresse2_va`=?,  `T100_codeinterne_i`=?, `T03_telephone_va`=? where T01_codeinterne_i=?";
	
	$updateInfosRecruteur = "UPDATE t03_profil set `T03_nom_structure_va`=?, `T03_presentation_va`=?, `T03_url_site_va`=? where T01_codeinterne_i=?";
	
	$updateSocial = "UPDATE t03_profil set  T03_viadeo_va=?, T03_linkedin_va=?, T03_facebook_va=?, T03_google_va=?, T03_instagram_va=?, T03_twitter_va=? where  T01_codeinterne_i=?";
	
	$updateTypologie = "UPDATE t03_profil set  T107_codeinterne_i=? where  T01_codeinterne_i=?";
	
	$updateSlug = "UPDATE t03_profil set T03_slug_va=? where T01_codeinterne_i=?";
?>