<?php
	$getProfil = "SELECT * from t03_profil t03 LEFT JOIN t100_communes t100 on t100.T100_codeinterne_i=t03.T100_codeinterne_i where T01_codeinterne_i=?";
	$getUserBySlug = "SELECT t01.*, t02.*, t03.*, t100.*, t04a.T04_libelle_va role_libelle, t04b.T04_libelle_va type_user_libelle, t115.* 
		FROM t01_users t01 
		LEFT JOIN t03_profil t03 on t01.T01_codeinterne_i=t03.T01_codeinterne_i 
		LEFT JOIN t100_communes t100 on t100.T100_codeinterne_i=t03.T100_codeinterne_i
		LEFT JOIN t02_role t02 ON t02.T02_codeinterne_i = t01.T02_codeinterne_i  
		LEFT JOIN t04_dico t04a on t04a.T04_code_va=t02.T02_role_va  
		LEFT JOIN t04_dico t04b on t04b.T04_code_va=t03.T04_code_type_user_va
		LEFT JOIN t115_users_abo t115 on t115.T01_codeinterne_i=t01.T01_codeinterne_i  
		WHERE t03.T03_slug_va =  ? and t04a.T04_section_va='role' and t04a.T04_locale_va='" . $_SESSION['locale'] . "' and t04b.T04_section_va='type_user' and t04b.T04_locale_va='" . $_SESSION['locale'] . "'";

	$getNbRecruteurs = "select count(*) nb from t03_profil where T04_code_type_user_va='recruteur_public' OR T04_code_type_user_va='recruteur_prive'";
	$getNbRecruteursPrive = "select count(*) nb from t03_profil where T04_code_type_user_va='recruteur_prive'";

	$getNbRecruteursPublic = "select count(*) nb from t03_profil where T04_code_type_user_va='recruteur_public'";
	$getNbCandidats = "select count(*) nb from t03_profil where T04_code_type_user_va='candidat'";


	$relance30days = "SELECT * FROM t03_profil t03 
	LEFT JOIN t01_users t01 on t01.T01_codeinterne_i=t03.T01_codeinterne_i
	LEFT JOIN  t115_users_abo t115 ON t115.T01_codeinterne_i = t03.T01_codeinterne_i 
	WHERE T04_code_type_user_va='recruteur_prive' AND T115_status_va='ACTIVE' AND T115_date_fin_d=DATE_ADD(CURDATE(), INTERVAL 30 DAY)";

	$relance15days = "SELECT * FROM t03_profil t03
	LEFT JOIN t01_users t01 on t01.T01_codeinterne_i=t03.T01_codeinterne_i
	LEFT JOIN  t115_users_abo t115 ON t115.T01_codeinterne_i = t03.T01_codeinterne_i 
	WHERE T04_code_type_user_va='recruteur_prive' AND T115_status_va='ACTIVE' AND T115_date_fin_d=DATE_ADD(CURDATE(), INTERVAL 15 DAY)";

	$relance7days = "SELECT * FROM t03_profil t03 
	LEFT JOIN t01_users t01 on t01.T01_codeinterne_i=t03.T01_codeinterne_i
	LEFT JOIN  t115_users_abo t115 ON t115.T01_codeinterne_i = t03.T01_codeinterne_i 
	WHERE T04_code_type_user_va='recruteur_prive' AND T115_status_va='ACTIVE' AND T115_date_fin_d=DATE_ADD(CURDATE(), INTERVAL 7 DAY)";

	$expireToday = "SELECT * FROM t03_profil t03
	LEFT JOIN t01_users t01 on t01.T01_codeinterne_i=t03.T01_codeinterne_i
	LEFT JOIN  t115_users_abo t115 ON t115.T01_codeinterne_i = t03.T01_codeinterne_i 
	WHERE T04_code_type_user_va='recruteur_prive' AND T115_status_va='ACTIVE' AND T115_date_fin_d=CURDATE()";

?>