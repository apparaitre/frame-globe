<?php
function NbLines($largeur_txt,$txt)
{
    //Calcule le nombre de lignes qu'occupe un MultiCell de largeur w
    $car_width = &$this->CurrentFont['cw'];
    if($largeur_txt == 0)
        $largeur_txt = $this->w - $this->rMargin - $this->x;
    $largeur_txtmax=( $largeur_txt - 2 * $this->cMargin ) * 1000 / $this->FontSize;
	
    $txt = str_replace("\r",'',$txt);
    $txt_lenght = strlen($txt);
    if($txt_lenght>0 and $txt[$txt_lenght-1]=="\n")
	{
        $txt_lenght--;
	}
    $separateur=-1;
    $i=0;
    $j=0;
    $l=0;
    $nombre_lignes=1;
    while($i < $txt_lenght)
    {
        $car = $txt[$i];
        if($car == "\n")
        {
            $i++;
            $separateur=-1;
            $j=$i;
            $l=0;
            $nombre_lignes++;
            continue;
        }
        if($car == ' '){$separateur  =$i;}
        $l += $car_width[$car];
        if($l > $largeur_txtmax)
        {
            if($separateur == -1)
            {
                if($i == $j)
                    $i++;
            }
            else
                $i=$separateur+1;
            $separateur=-1;
            $j=$i;
            $l=0;
            $nombre_lignes++;
        }
        else
		{
            $i++;
		}
    }
    return $nombre_lignes;
}