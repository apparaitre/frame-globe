<?php
	class MyFPDF extends FPDI
	{
		function footer($text)
		{
		// Positionnement à 1,5 cm du bas
		$this->SetY(-20);
		// Police Arial italique 8
		$this->SetFont('Arial','I',7);
		$this->Cell(0,10,$text, 0, 0, 'C');
		// Numéro de page centré
		$this->Cell(0,10,'Page '.$this->PageNo() .'/{nb}',0,0,'R');
		}
	}