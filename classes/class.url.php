<?php
	/**
	 * class.url.php
	 *
	 * @author    Thierry Cazalet <thierry@apparaitre.fr>
	 * @copyright 2007-2013 Apparaitre
	 * @license   
	 * @version   1.0.0
	 * @link      http://www.apparaitre.fr/frame-globe	 
	 */
	 
	/**
	 *   Url class Handle Url manipulation 
	 */
	 
	class Url
	{
		var $db;
		var $url;
		var $type;
		var $params ;
		var $chainParams ;
		var $urlBase;
		
		
		public function __construct()
		{
			global $conn;
			$this->db = $conn;
			$this->url = "";
			$this->type = "";
			$this->params = array();
			$this->chainParams = "";
			$this->urlBase = _VHOST_URL;
		}
		function setParam($key, $val)
		{
			$this->params[$key] = $val;
			$this->chainParams = http_build_query($this->params, '', '&');
		}
		
		
		function getUrl($url)
		{
			global $T00;
			$this->url = $url;
			if(strpos($this->url, "@") !== false)
			{
				$url = explode('@', $this->url);
				$role = $url[0];
				$this->url = $url[1];
			}
			
			if(strpos($this->url, ":") !== false)
			{
				$url = explode(':', $this->url);
				$this->url = $url[0];
				/* affectations des paramètres contenu dans l'URL */
				$params = explode('&', $url[1]);
				
				foreach ($params as $param)
				{
					$param = explode('=', $param);
					$param[1] = empty($param[1]) ? true : $param[1];
					$this->setParam($param[0], $param[1]);
				}
			}
			
			if(!empty($role))
			{
				$this->setParam("role", $role);
			}
			
			switch($this->type)
			{
				default:
				case "normale":
					return $this->getNormal();
					break;
				
				case "propre":
					// si il n'y a une virgule, l'url correspond à la rubrique
					if(strpos($this->url, ",") === false)
					{
						$url_propre = $T00->getUrl($url, "", $_SESSION['app']);
					}
					else
					{
						// Sinon on sépare rubrique et page
						$url = explode(",", $this->url);
						$url_propre = $T00->getUrl($url[0], $url[1], $_SESSION['app']);
						
					}
					
					if(!empty($url_propre))
					{
						if($this->params != "")
						{
							return $this->urlBase . "/" . rtrim($url_propre[0]->T00_url_va . "?" . $this->chainParams, "?");
						}
						else
						{
							return $this->urlBase . "/" . $url_propre[0]->T00_url_va;
						}
					}
					else
					{
						return $this->getNormal();
					}
					
					break;
			}
		}
		
		function setType($type)
		{
			$this->type = $type;
		}
		
		function getNormal()
		{
			if(strpos($this->url, ",") === false)
			{
				/* Syntaxe pour rubrique, page */
				$this->setParam("rubrique", $this->url);
				
	
			}
			else
			{
				/* Syntaxe pour page */
				$url = explode(",", $this->url);
				$this->setParam("rubrique", $url[0]);
				$this->setParam("page", $url[1]);
			}
			
			if(count($this->params) > 0)
			{
				return $this->urlBase . "/" . rtrim("index.php" . "?" . $this->chainParams, '&');
			}
			else
			{
				return $this->urlBase . "/index.php";
			}
		}
	}
?>