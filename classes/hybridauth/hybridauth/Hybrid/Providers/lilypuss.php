<?php
/*!
* HybridAuth
* http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
* (c) 2009-2012, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html 
*/

/**
* Hybrid_Providers_Twitter provider adapter based on OAuth1 protocol
*/
class Hybrid_Providers_lilypuss extends Hybrid_Provider_Model_OAuth1
{
	/**
	* IDp wrappers initializer 
	*/
	function initialize()
	{
		parent::initialize();

		// Provider api end-points
		$this->api->api_base_url      = "https://www.lilypuss.com/blog/oauth/";
		$this->api->authorize_url     = "https://www.lilypuss.com/blog/oauth/authorize";
		$this->api->request_token_url = "https://www.lilypuss.com/blog/oauth/request_token";
		$this->api->access_token_url  = "https://www.lilypuss.com/blog/oauth/access_token";

		$this->api->curl_auth_header  = false;
		
	}

	/**
	* load the user profile from the IDp api client
	*/
	function getUserProfile()
	{
		echo "getUserProfile";
		
		$response = $this->api->get( "getUserConnected" ); 

		// check the last HTTP status code returned
		if ( $this->api->http_code != 200 ){
			throw new Exception( "User profile request failed! {$this->providerId} returned an error. " . $this->errorMessageByStatus( $this->api->http_code ), 6 );
		}

		if ( ! is_object( $response ) || ! isset( $response->id ) ){
			throw new Exception( "User profile request failed! {$this->providerId} api returned an invalid response.", 6 );
		}
		exit;
		# store the user profile.  
		$this->user->profile->identifier  = (property_exists($response,'id'))?$response->id:"";
		$this->user->profile->displayName = (property_exists($response,'screen_name'))?$response->screen_name:"";
		$this->user->profile->description = (property_exists($response,'description'))?$response->description:"";
		$this->user->profile->firstName   = (property_exists($response,'name'))?$response->name:""; 
		$this->user->profile->photoURL    = (property_exists($response,'profile_image_url'))?$response->profile_image_url:"";
		$this->user->profile->profileURL  = (property_exists($response,'screen_name'))?("http://twitter.com/".$response->screen_name):"";
		$this->user->profile->webSiteURL  = (property_exists($response,'url'))?$response->url:""; 
		$this->user->profile->region      = (property_exists($response,'location'))?$response->location:"";

		return $this->user->profile;
 	}

	
	
	/**
	* logout
	*/
	function logout()
	{ 
		
		parent::logout();
	}
	
	
}
