FR=Français
EN=Anglais

libValeurObligatoire=Valeur obligatoire

/* menu super admin */
libMenuAccueil=Accueil
libMenuUtilisateurs=Utilisateurs
libMenuCms=CMS
libMenuArticles=Articles

/* menu public */
libMenuPresentation=Présentation
libMenuOrientation=Orientation
libMenuMetiers=Métiers
libMenuRecrutement=Recrutement
libMenuRecruteurs=Recruteurs
libMenuTelechargement=Telechargement

/* Menu admin */
LibMenuZone_admin=Acceuil 
LibMenuGestion_annonces=Annonces
LibMenuRegie_pub=Régie pub
LibMenuGestion_contenus=Blog
LibMenuMediatheque=Mediatheques



libPasIdentique=Les 2 valeurs ne sont pas identiques
libPasUnique=Cette valeur est déjà utilisée

libMessageAccord=Bonjour {CANDIDAT},<p>Vous avez passé un entretien dans nos locaux pour le poste de  {ANNONCE}.</p><p>Nous sommes heureux de vous faire savoir que votre candidature a été retenue pour ce poste.</p><p>A ce titre nous vous remercions vivement de nous contacter afin que nous puissions constituer votre dossier et finaliser votre embauche.</p><p>Dans l'attente du plaisir de cette collaboration, toute l'équipe en charge du recrutement vous prie d'agréer ses meilleurs sentiments.</p>
libMessageRefus=Bonjour {CANDIDAT},<p>Vous avez postulé pour le poste de  {ANNONCE}.</p><p>Nous sommes en regret de vous informer que nous n'avons pas retenu votre candidature.</p><p>Soyez persuadé que cela ne remet nullement en question la qualité de votre profil et la pertinence de votre dossier. Votre candidature n'est en effet pas totalement en adéquation avec les compétences que nous recherchons.</p><p>N'hésitez pas à contacter le service recrutement si vous souhaitez avoir plus d'informations quant à nos critères de recrutement.</p><p>Bien respectueusement,<br/>Le service recrutement.</p>
libMessageConvocation=Bonjour {CANDIDAT},<p>Vous avez postulé pour le poste de  {ANNONCE}.</p><p>Nous avons le plaisir de vous informer que <b>votre candidature a été présélectionnée</b>.</p><p>A ce titre nous aimerions vous rencontrer lors d'un <b>entretien dans nos locaux</b> le {DATE} et [HEURE]à {ADRESSE}.</p>Vous serez reçu(e) par</p><p>Merci de vous présenter à l'accueil 10 minutes avant l'heure de votre rendez-vous.</p><p>Dans cette attente, veuillez agréer l'expression de nos sentiments distingués<br />Le Service recrutement</p>