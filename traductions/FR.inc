FR=Français
EN=Anglais

libValeurObligatoire=Valeur obligatoire
libPasIdentique=Les 2 valeurs ne sont pas identiques
libPasUnique=Cette valeur est déjà utilisée
libPasEmail=Ce n'est pas un email valide
libPasSiren=Ce n'est pas un numéro SIREN valide