<?php
/* Isaac 3 
   by ivoras & nmrgan, 2005.
   Released under the Artistic License. For full text of the License
   see http://www.opensource.org/licenses/artistic-license.php
   $Id: i3.php,v 1.8 2005/11/27 23:19:13 ivoras Exp $
   powered by Isaac Browser III, somethingware �2002-2005 - http://mrgan.com/isaac
   Update, design & adaptation lamebrowser �2007 zero. - http://www.lamesoft.org/lamebrowser
*/

/* -------------------------
   User-definable properties
*/

$cfg['site_name'] = "&raquo; Base documentaire";
$cfg['show_subfolders'] = true;
$cfg['cache_file_info'] = false;
$cfg['show_thumbnails'] = true;
$cfg['cache_thumbnails'] = true;
$cfg['info_in_new_line'] = true;
$cfg['number_of_columns'] = 2;
$cfg['directory_header_file'] = 'README.txt';


/* -----------------
   Setup environment 
*/

define('SHORT', 1);
define('LONG', 2);

define('IMAGE_WEB', 10);
define('IMAGE_OTHER', 101);
define('DOCUMENT_WEB', 11);
define('DOCUMENT_OTHER', 111);
define('OTHER', 12);


$cfg['has_gd'] = function_exists('imagecopyresampled');

$rootdir_fs = dirname($_SERVER['SCRIPT_FILENAME']);
$rootdir = dirname($_SERVER['SCRIPT_NAME']);


$ignore_files = array(basename($_SERVER['SCRIPT_FILENAME']));

session_name('i3');
session_start();


/* ------------------------
   What does the user want?
*/
if (!empty($_SERVER['PATH_INFO']))
    $viewobj = $_SERVER['PATH_INFO'];
else
    $viewobj = '/';

if (strstr('..', $viewobj) !== false) // security
    $viewobj = '/';

$viewobj_fs = $rootdir_fs . $viewobj; // _fs suffix: it's a filesystem-absolute value

if (is_dir($viewobj_fs)) {  // url references a directory
    $viewdir = $viewobj;        // directory that is being viewed, web-relative, EXCLUDING Isaac script
    $viewdir_fs = $viewobj_fs;  // the above, resolved to be filesystem-absolute
    $viewfile = null;           // file that is being viewed, web-relative, etc.
    $viewfile_fs = null;        // the above, filesystem-absolute
} else {                    // url references a file
    $viewdir = dirname($viewobj);
    $viewdir_fs = dirname($viewobj_fs);
    $viewfile = basename($viewobj);
    $viewfile_fs = $viewobj;
}


if ($viewdir_fs[strlen($viewdir_fs)-1] == '/')  // strip trailing slash
    $viewdir_fs = substr($viewdir_fs, 0, strlen($viewdir_fs)-1);
if ($viewdir[strlen($viewdir)-1] == '/')        // ditto
    $viewdir = substr($viewdir, 0, strlen($viewdir)-1);


if (!empty($_GET['what'])) {
    switch (strtolower($_GET['what'])) {
        case 'thumbnail' :
            send_thumbnail();
            break;
        default:
            evil_state("Action inconnue : $_GET[what]");
    }
    exit;
} else {
    if (!is_dir($viewdir_fs))
        evil_state("Le dossier demand&eacute; n'existe pas : $viewdir_fs");
    
    read_directory();
    send_preamble();
    send_navigation();
    send_image();

    if ($cfg['show_subfolders'])
        send_dirlist();
    if ($cfg['directory_header_file'])
        send_dirheader();

    send_filelist();
    send_footer();
    send_closure();

    exit;
}


/* ------------------
   Read the directory
   This caches the file information list of the current directory
*/
function read_directory() {
    global $dirlist, $filelist, $ignore_files, $cfg;
    $dirlist = array();
    $filelist = array();

    global $viewdir_fs;

    if ($cfg['cache_file_info'] && $_SESSION['old_viewdir_fs'] === $viewdir_fs) {
        $dirlist = $_SESSION['old_dirlist'];
        $filelist = $_SESSION['old_filelist'];
        return;
    }
    
    if ($h = opendir($viewdir_fs)) {
        while ( ($de = readdir($h)) !== false ) {
            if ($de[0] == '.')
                continue;
            if ($de === '_cache')
                continue;
                if ($de === '_gfx')
                continue;
            $de_fs = $viewdir_fs . '/' . $de;
            if (is_dir($de_fs))
                $dirlist[] = utf8_encode($de);
            elseif (!in_array($de, $ignore_files))
                $filelist[] = array('de' => $de, 'shortinfo' => file_info_str($de, SHORT), 'url'=>make_url($de), 'url_file'=>make_url_file($de));
        }
        closedir($h);

        casesort($dirlist);
        casesort_de($filelist);

        $_SESSION['old_viewdir_fs'] = $viewdir_fs;
        $_SESSION['old_dirlist'] = $dirlist;
        $_SESSION['old_filelist'] = $filelist;
    } else
        evil_state("Impossible d'ouvrir le dossier : $viewdir_fs");

}


/* ------------------
   Send HTML preamble   
   This includes: HTTP Headers, HTML preamble, CSS
*/
function send_preamble() {
    global $viewobj, $cfg, $rootdir;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
    <title><?=$cfg['site_name']?> <?=$rootdir; ?> <?=$viewobj?></title>
    <style type="text/css">
        body, td {
           
			font-family: Verdana, Arial, sans-serif;
            font-size: 10pt;
			color: #666;
            margin:0px; 
            padding:0px; 
        }
        A                           { text-decoration: none; }
		A:link, A:active, A:visited { color: #666; }
        A:hover                     { color: #333; text-decoration: underline; }
 
		A.raquo:before {}
		
		A.up {
			display:block;
			float:left;
		}
		A.up IMG {
		 position: relative; top: 4px;
		}

        A.dir { font-weight: bold; }                                   /* directory name in filelist */
        A.dir:visited, A.dir:active, A.dir:link, { color: #666; }
        A.dir:hover { color: #333; }
 
        A.text:link, A.text:active, A.text:visited, { color: #666; }
        A.text:hover                                { color: #333; }

        A IMG { border: 1px solid #666; }
        A IMG.borderNone {border:0;background:none;}
        SMALL { font-size: 8pt; font-family: "Arial, Small Fonts, Courier", sans-serif }

        .container { width: 100%; }                /* this contains most of the page in it */
		.content { width: 100%; }
		.headcontent { margin: auto; padding-top: 10px; margin-bottom: 0px;display:none; }
        .isaac1 { color: #999; }  /* first word, lame  */
        .isaac2 { color: #AAA; }  /* second word, browser */
        .titleCell {                                              /* very top row */
            font: bold 12pt/18pt "Trebuchet MS", "Verdana",sans-serif;
            background-color: #dedede;
            padding: 5px 12px 6px 16px;
            border: solid 0 #ccc; border-bottom-width: 1px;
        }
       
        
        .control, .controlArrow {
            border: 1px solid #ccc;
			color: #333;
            margin-top:6px; margin-bottom: 1px;
            padding: 2px 7px 4px 7px;
            /*float: left;*/
            display: block;
            background-color: #dedede;
        }

		.listfile { 
			width: width: 820px; 
			margin-left: auto; 
			margin-right: auto; 
			padding: 10px; 
			
			}
		.listdirectory { 
			
			z-index: 9999;
			width: 820px; 
			margin-left: auto; 
			margin-right: auto; 
			padding: 10px; 
			}
		.footer { font-size: 10px; padding: 10px; margin: 0px; border-top: 1px dotted #666; display:none;}
        .button {
            width: 1px;
            white-space: nowrap;
            background-color: #dedede; padding: 5px 12px 6px 12px;
            border: solid 1 #ccc;
            cursor: hand;
        }

        .grey { color: #AAAAAA; }
        .warning { color: #AA4411; cursor: default; font-weight: bold }  /*  ! and ? */
        .title { color: #333; font: bold 12pt/18pt "Arial", "Helvetica", sans-serif; }  /* title of selected image, top of page */
         
		.barNav { 
			height:50px;
			padding:8px;
		}
		.up_cont {
			width:50px;
			height:50px;
			float:left;
		}
		
		.barNav .url {  
            font-size: 14px;
			font-family: "Verdana", "Arial", "Helvetica", sans-serif;
			font-weight:bold;
            color: #666;
			float:left;
			margin-left:20px;
			padding:8px;
			line-height:36px;
			background-color: #dedede; 
			width:713px;
			
        }
		.ui-corner-all { -moz-border-radius: 5px; -webkit-border-radius: 5px; border-radius: 5px; }
    </style>
</head>
<body>

<div class="container">

<div class="content">
<div class="headcontent"><center><img src="<?=$rootdir; ?>/_gfx/logo.png" border="0" alt="logo"></center></div>
<?php
}



/* ---------------
   Send navigation
   This generates isaac navigation
*/
function send_navigation() {
    global $rootdir, $dirlist, $filelist, $viewobj, $viewfile;
echo "<div class=\"listfile\">\n";
    if (!$viewfile)
        return;

    echo "<b class=\"title\">$viewfile</b><br /><small class=\"grey\">".file_info_str($viewfile, LONG)."</small><br>\n";

    $idx = array_search_de($viewfile, $filelist);
    
    if ($idx !== false) {
        echo "<table style=\"border:none; width:100%\"><tr>";
        if ($idx > 0) {
            echo "<td class=\"control\">\n";
            echo "&laquo; &nbsp;";
            echo "<a href=\"".make_url($filelist[$idx-1]['de'])."\">".$filelist[$idx-1]['de']."</a>";
            echo "</td>\n";
            if ($idx < count($filelist)-1)
                echo "<td width=\"10\">&nbsp;</td>";
        }
        if ($idx < count($filelist)-1) {
            echo "<td class=\"control\">\n";
            echo "<a href=\"".make_url($filelist[$idx+1]['de'])."\">".$filelist[$idx+1]['de']."</a>";
            echo "&nbsp; &raquo;";
            echo "</td>\n";
        }
        echo "</tr></table>\n";
		echo "</div>\n";
    }
}


/* ----------
   Send image
   If the image is specified in viewobj, send HTML to view it. If not,
   seek default image.
*/
function send_image() {
    global $rootdir, $viewfile, $viewdir;
echo "<div class=\"listfile\">\n";
    if (!$viewfile)
        return;
    
    if ($rootdir != '/')
        $file_url = $rootdir.$viewdir.'/'.$viewfile;
    else
        $file_url = "/$viewfile";
   
    switch (file_type($viewfile)) {
        case IMAGE_WEB:
            echo "<a href=\"".$file_url."\">\n";
            echo "<img src=\"".$file_url."\" ".image_wh($viewfile)." alt=\"$viewfile\">\n";
            echo "</a><br>\n";
            echo "<span class=\"url\">http://".$_SERVER['HTTP_HOST'].$file_url."</span><br><br>\n";
            break;
        case DOCUMENT_WEB:
            echo "<iframe width=\"100%\" height=\"620\" src=\"$file_url\"></iframe>\n";
            echo "<br><br>\n";
        default:
            echo "<div style=\"display:block; float:none; width:100%;\">";
            echo "<div class=\"control\" style=\"width: 200px\">\n";
            echo "<b>$viewfile</b><br>\n";
            echo file_info_str($viewfile, LONG)."<br>\n";
            echo "<a href=\"".$file_url."\"><b>&raquo; Voir / T&eacute;l&eacute;charger</b></a>\n";
            echo "</div>";
			echo "</div>";
            echo "</div>\n<br>\n";
            break;
    }
}


/* -------------------
   Send directory list
   Send list of directories in the currenty selected directory
*/
function send_dirlist() {
    global $rootdir, $viewdir, $dirlist, $cfg;

    $nc = $cfg['number_of_columns'];
    $ncprec = $nc-1;
    
    $updir = $_SERVER['SCRIPT_NAME'].dirname($viewdir);
    
    echo "<div class=\"listdirectory ui-corner-all\">\n";
    
	echo '<div class="barNav ui-corner-all"><div class="up_cont">';
    if ($viewdir != '/public')
	{	// this is a subdirectory
        echo "<a class=\"up\" href=\"$updir\"><img src=\"".$rootdir."/_gfx/back.png\"  class=\"borderNone\"/></a>";
	}
	echo "</div><div class=\"url ui-corner-all\">{$rootdir}{$viewdir}</div><br style=\"clear:both\" /></div>";
	
	 
    /*
	if (count($dirlist) > 1)
        echo ' | '. count($dirlist). ' autres dossiers :';
    else if (count($dirlist) == 1)
        echo ' | un autre dossier :';
*/
    /*
	echo "<table  style=\"border:none; width:100%; padding:5px; margin:10px;\">\n";
    for ($i = 0; $i < count($dirlist); $i++) {
        $z = $i % $nc;
        if ($z == 0)
            echo "<tr>";
        echo "<td align=\"center\"><a  href=\"".make_url($dirlist[$i])."\">";
        echo "<img src=\"".$rootdir."/_gfx/folder.png\"  class=\"borderNone\"/></a><br/>";
        echo "<a class=\"raquo\" href=\"".make_url($dirlist[$i])."\">";
        echo $dirlist[$i];
        echo "</a></td>\n";
        if ($z == $ncprec)
            echo "</tr>";
    }
    if ($z != 0 && $z != $ncprec)
        echo "</tr>";
    echo "</table>";
	
	*/
	
	 for ($i = 0; $i < count($dirlist); $i++) {
       echo "<div style=\"float:left;padding:20px; text-align:center\"><a  href=\"".make_url($dirlist[$i])."\">";
        echo "<img src=\"".$rootdir."/_gfx/folder.png\"  class=\"borderNone\"/></a><br/>";
        echo "<a class=\"raquo\" href=\"".make_url($dirlist[$i])."\">";
        echo $dirlist[$i];
        echo "</a></div>\n";
       
    }
    
	
	
	echo "<br style=\"clear:both\"/></div>\n";
    
}


/* --------------------------
   Send directory header file
   Sends textual file as directory header
*/
function send_dirheader() {
    global $cfg, $rootdir, $viewdir, $viewdir_fs;
    $dh_file = "{$viewdir_fs}/{$cfg['directory_header_file']}";
    if (file_exists($dh_file)) {
        $pi = pathinfo($cfg['directory_header_file']);
        echo '<hr>';
        switch (strtolower($pi['extension'])) {
            case 'html':
            case 'htm':
                $f = @fopen($dh_file, 'r');
                if (!$f)
                    return;
                $content = fread($f, filesize($dh_file));
                fclose($f);
                if (stristr($content, '<body>') !== false) {
                    preg_match('/\<body\>(.+)\<\/body\>/si', $content, $m);
                    $content = $m[1];
                }
                echo $content;
                break;
            case 'txt':
            case 'asc':
                echo '<pre>';
                readfile($dh_file);
                echo '</pre>';
                break;
        }
    }
}


/* -------------
   Send filelist
   Send list of files in the currently selected directory
*/
function send_filelist() {
    global $rootdir, $viewdir, $filelist, $cfg;
	$z = 0;
    $nc = $cfg['number_of_columns'];
    $ncprec = $nc-1;
    
    echo "<div class=\"listfile ui-corner-all\" >\n";
	
    echo "<table style=\"border:none; width:100%\">\n";
    for ($i = 0; $i < count($filelist); $i++) {
        $z = $i % $nc;
        if ($z == 0)
            echo "<tr>";
        echo "<td><table style=\"border:none; width:100%\"><tr><td align=\"center\">";
        echo "<a href=\"".$filelist[$i]['url_file']."\" target=\"_blank\">";
        if ($cfg['show_thumbnails']) {
            echo '<img src="'.$filelist[$i]['url'].'?what=thumbnail"   class="borderNone">';
            echo '<br>';
        }
        echo supprimer_numero($filelist[$i]['de']);
        echo '</a>';
        if ($cfg['info_in_new_line'])
            echo "<br><small class=\"grey\">".$filelist[$i]['shortinfo']."</small><br><br>";
        else
            echo "<small class=\"grey\">, ".$filelist[$i]['shortinfo']."</small>";
        echo "</td></table></td>\n";
        if ($z == $ncprec)
            echo "</tr>";
    }
    if ($z != 0 && $z != $ncprec)
        echo "</tr>";
    echo "</table>";
	echo "</div>\n";
}

function supprimer_numero($texte) {
	return preg_replace(
	",^[[:space:]]*([0-9]+)([.)]|".chr(194).'?'.chr(176).")[[:space:]]+,S",
	"", $texte);
}

/* -----------
   Send footer
   Footer includes copyright notice, etc.
*/
function send_footer() {
?>
<br>
<div class="footer">
<span class="url" style="color: #AAA;"><a href="http://www.lamesoft.org/lamebrowser">powered by LameBrowser</a>, lamesoft &copy; 2007 <a class="text" href="mailto:pixelwall@gmail.com">David Roussel</a> &amp; <a class="text" href="mailto:phracktale@gmail.com">Thierry Cazalet</a> <br><a href="http://mrgan.com/isaac">based on Isaac Browser III</a>, somethingware &copy; 2002-2005 <a class="text" href="mailto:ivoras@gmail.com">Ivan Voras</a> &amp; <a class="text" href="mailto:neven@mrgan.com">Neven Mrgan</a>
</center></div>
</div>
<?php
}


/* -----------------
   Send HTML closure
   What the title says...
*/
function send_closure() {
?>
</div>
</div>
</body>
</html>
<?php
}



/* ---------------------------------------
   Send a thumbnail of the requested image
*/
function send_thumbnail() {
    global $cfg, $viewfile, $viewfile_fs, $HTTP_IF_MODIFIED_SINCE, $viewdir_fs, $rootdir_fs;
    $realfile = substr($viewfile_fs, 1);

    $mtime = filemtime($realfile);
    $gmdate_mod = gmdate('D, d M Y H:i:s', $mtime) . ' GMT';
    $gmdate_expires = gmdate('D, d M Y H:i:s', $mtime+36000) . ' GMT';

    header("Last modified: $gmdate_mod");
    header("Date: $gmdate_mod");
    header("Cache-control: public");
    header("Expires: $gmdate_expires");

    if ($HTTP_IF_MODIFIED_SINCE) {
        $if_modified_since = preg_replace('/;.*$/', '', $HTTP_IF_MODIFIED_SINCE);

        if ($if_modified_since == $gmdate_mod) {
            header("HTTP/1.0 304 Not Modified");
            exit;
        }
    }
   
    $stat = stat($realfile);
    $cache_file = md5($realfile.$stat[9].$stat[7]).'.jpeg';
    if (!empty($viewdir))
        $cache_dir = "$viewdir/_cache";
    else
        $cache_dir = '_cache';
    $cache_realfile = "$viewdir_fs/_cache/$cache_file";
    
    if ($cfg['cache_thumbnails'] && is_dir($cache_dir)) {
        $from_cache = true;
        if (file_exists($cache_realfile)) {
            $stat_cache = stat($cache_realfile);
            Header("Content-type: image/jpeg");
            Header("Content-length: {$stat_cache[7]}");
            readfile($cache_realfile);
            exit;
        }
    }
    
    $pinfo = pathinfo($realfile);
    $gd = null;
    switch (strtolower($pinfo['extension'])) {
        case 'jpg':
        case 'jpeg':
        case 'jfif':
            $gd = imagecreatefromjpeg($realfile);
            break;
        case 'png':
            $gd = imagecreatefrompng($realfile);
            break;
        case 'gif':
            $gd = imagecreatefromgif($realfile);
            break;
    }
    if (!$gd) {
        // Cr�ation du thumbnail N/A en cas de fichier non affichable
        /*
        
        */
        
        $pinfo = pathinfo($realfile);
        $extension = strtolower($pinfo['extension']);
        $icon_file = $rootdir_fs."/_gfx/".$extension.".png";
        
        if(file_exists($icon_file))
        {
            $fp = fopen($icon_file,r);
            if($fp)
            {
                $data = fread($fp, filesize($icon_file));
                header("Content-Length: ".strlen($data));
                header("Content-type: image/png");
                header('Content-transfer-encoding: binary');
            }
        }
        else
        {
            $data = base64_decode("iVBORw0KGgoAAAANSUhEUgAAACAAAAAgBAMAAACBVGfHAAAALHRFWHRDcmVhdGlvbiBUaW1lAG5lZCAxMCBzcnAgMjAwNSAwMDowMDo1OCArMDEwMLDSpakAAAAHdElNRQfVBwkWBAQ6pWp5AAAACXBIWXMAAAsSAAALEgHS3X78AAAABGdBTUEAALGPC/xhBQAAAAZQTFRF////AACAuHpQXgAAAEVJREFUeNrVzckRADAIAkDpAPpvNgk5zFFBeOmOgxHfBTmKQQO0AxOoAQITWsUC0FBXJbhCA+afdVHBFTYDTuh45YEPUgB3sgM0ySaJwQAAAABJRU5ErkJggg==");
            header("Content-Length: ".strlen($data));
            header("Content-type: image/png");
            header('Content-transfer-encoding: binary');
        }
        echo $data;
        
        exit;
    }
    list($width, $height, $type, $attr) = getimagesize($realfile);
    $max_w = 99;
    $max_h = 80;
    
    $gd2 = imagecreatetruecolor($max_w+1, $max_h+1);
    $black = imagecolorallocate($gd2, 0, 0, 0);
    imagefilledrectangle($gd2, 0, 0, $max_w, $max_h, $black);

    // todo
    // imagecopyresampled($gd2, $gd, 0, 0, 0, 0, $max_w, $max_h, $width, $height);
    // imagecopyresized($gd2, $gd, 0, 0, 0, 0, $max_w, $max_h, $width, $height);

    $new_w = $width;
    $new_h = $height;
    $aspect = $width / $height;

    if ($new_w > $max_w+1) {
        $new_w = $max_w+1;
        $new_h = $new_w/$aspect;
    }

    if ($new_h > $max_h+1) {
        $new_h = $max_h+1;
        $new_w = $aspect*$new_h;
    }

    $pos_x = ($max_w - $new_w) / 2;
    $pos_y = ($max_h - $new_h) / 2;

    if (function_exists('imagecopyresampled'))
        imagecopyresampled($gd2, $gd, $pos_x, $pos_y, 0, 0, $new_w, $new_h, $width, $height);
    else if (function_exists('imagecopyresized'))
        imagecopyresized($gd2, $gd, $pos_x, $pos_y, 0, 0, $new_w, $new_h, $width, $height);
    else
        exit;
    
    header("Content-type: image/jpeg");
    imagejpeg($gd2, "", 50);

    if ($from_cache)
        imagejpeg($gd2, $cache_realfile, 50);
    
    exit;
}



/* -----------------
   Support functions
*/
function evil_state($msg) {
    echo "This should not happen!<br>";
    echo $msg;
    exit;
}


function make_url($file) { 
    /* Make URL that references Isaac */
    global $rootdir, $viewdir;
    return $_SERVER['SCRIPT_NAME'].$viewdir.'/'.$file;
}

function make_url_file($file) { 
    /* Make URL that references Isaac */
    global $rootdir, $viewdir;
	$mUrl=substr($_SERVER['SCRIPT_NAME'], 0, strlen($_SERVER['SCRIPT_NAME']) - 10);
	return $mUrl.$viewdir.'/'.$file;
}

function file_info_str($file, $size=SHORT) {
    /* Generate and return file information string (size, date, etc.) */
    global $viewdir_fs, $viewdir;
    $filename = $viewdir_fs . '/' . $file;

    list($width, $height, $type, $attr) = @getimagesize($filename);
	 $stat = @stat($filename);
    $fsize = intval($stat['size']/1024);
    if ($fsize == 0)
        $fsize = 1;
    if ($size == LONG) {
        $lastmod = strftime("%d %m %Y, %H:%M", $stat['mtime']);
        if ($width && $height) 
            return "{$width} x {$height}, {$fsize}Kb. Derni&egrave;re modification&nbsp;: $lastmod";
        else
            return "Dimensions inconnues, {$fsize}Kb. Derni&egrave;re modification&nbsp;: $lastmod";
    } else {
        $lastmod = strftime("%d %m %Y, %H:%M", $stat['mtime']);
        return "{$fsize}Kb $lastmod";
    }
}


function image_wh($file) {
    /* Generate string in form 'width=X height=Y' for the specified image file.
       Width and height are proportionaly scaled to fit on screen */
    global $viewdir_fs;
    $filename = $viewdir_fs . '/' . $file;

    list($width, $height, $type, $attr) = getimagesize($filename);
    
    $whaspect = $width/$height;

    if ($width > 800) {
        $width = 800;
        $height = $width/$whaspect;
    }

    if ($height > 600) {
        $height = 600;
        $width = $height * $whaspect;
    }
    
    $width = intval($width);
    $height = intval($height);

    return "width=\"$width\" height=\"$height\"";
}


function file_type($file) {
    /* Determine what broad category the file belongs to */
    global $viewdir_fs;
    $filename = $viewdir_fs . '/' . $file;
	if(file_exists($file))
	{
		$parts = pathinfo($file);
		$ext = strtolower($parts['extension']);
		if (in_array($ext, array('gif', 'png', 'jpg', 'jpeg')))
			return IMAGE_WEB;
		elseif (in_array($ext, array('psd', 'pcx', 'tif', 'tiff', 'bmp', 'pbm')))
			return IMAGE_OTHER;
		elseif (in_array($ext, array('txt', 'asc', 'htm', 'html', 'php')))
			return DOCUMENT_WEB;
		elseif (in_array($ext, array('doc', 'xls', 'ppt', 'pps', 'sxw', 'sxi', 'sxc')))
			return DOCUMENT_OTHER;
		else
			return OTHER;
	}
}

/* -----------------
   Utility Functions
*/
function dump_var($v) {
    echo "<pre>";
    var_dump($v);
    echo "</pre>";
}


function e_dump_var($var) {
    ob_start();
    var_dump($var);
    $content = ob_get_contents();
    ob_end_clean();
    $f = fopen("/tmp/i3.log", "a+");
    fwrite($f, $content);
    fclose($f);
}
    

function case_cmp($a, $b) {
    $a = strtolower($a);
    $b = strtolower($b);
    if ($a == $b)
        return 0;
    return ($a < $b) ? -1 : 1;
}


function casesort(&$a) {
    usort($a, "case_cmp");
}


function case_cmp_de($a, $b) {
    $a = strtolower($a['de']);
    $b = strtolower($b['de']);
    if ($a == $b)
        return 0;
    return ($a < $b) ? -1 : 1;
}


function casesort_de(&$a) {
    usort($a, "case_cmp_de");
}


function array_search_de($name, $dearray) {
    for ($i = 0; $i < count($dearray); $i++)
        if ($dearray[$i]['de'] === $name)
            return $i;
    return false;
}


?>
