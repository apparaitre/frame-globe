<?php
	include_once(_VHOST_PATH . 'modules/membres/class.membres.php');

	switch($module_hook)
	{
		case "menu_principal":
			$libelle = "Gestion des membres";
			$code = "membres";
			$position = "primary"; // "module" pour être placé dans le sous menu modules
			$icon = "fa-users"; // liste des icones : 
			$color = "amethyst"; // liste des couleurs : 

			break;

		case "admin":
			/*
			 *  Admin section modules
			 *  Interface d'administration du module
			 */

			$membres = new Membres();
			$content = $membres->route($_FUNCTION);
			$templateDir = "modules/membres/templates/";
			$templateFile = $membres->templateFile;
			$tmpVars = $membres->tmpVars;
			$replace = $membres->replace;
			$dynamic_vars = $membres->dynamic_vars;
			break;


	}

?>