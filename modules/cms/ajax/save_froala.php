<?php

	if (isset($_POST['body'])) 
	{
		$body = $_POST['body'];
		$page = $_POST['page'];
		$dir = "../../../app/cms/";
		$file_cms = $dir . $page;
		
		if (file_exists($file_cms)) 
		{
			if($fc = fopen($file_cms, "w"))
			{
				fwrite($fc, $body);
				fclose($fc);
				echo "OK";
			}
			else
			{
				echo "Le fichier n'a pas les bons droits d'accès";
			}
		}
		else
		{
			echo "le fichier n'existe pas !";
		}
	}
	else
	{
		echo "Il n'y a rien à sauver !";
	}
?>