<?php

	class Cms {

		var $templateFile;
		var $tmpVars;
		var $replace;
		var $dynamic_vars;

		public function __construct()
		{

		}
		
		public function route($function)
		{
			switch($function)
			{
				default:
				case "liste":
					$content = $this->liste("app/cms");
					$this->templateFile = "liste.html";
					$this->tmpVars = array("/{CONTENT}/i");
					$this->replace = array($content);	
					$this->dynamic_vars = array();
		   			break;
					
				case "ajouter_rubrique":
					
					break;
					
				case "ajouter_article":
					
					break;
					
				case "editer_rubrique":
					
					break;
					
				case "editer_article":
					$filename = file('app/cms/' . $_GET['article'] . '.html');
					$file = implode('', $filename);

					$this->templateFile = "editer_article.html";
					$this->tmpVars = array("/{PAGE}/i", "/{FILENAME}/i");
					$this->replace = array($file, $_GET['article'] . '.html');	
					$this->dynamic_vars = array();
					break;
					
				case "supprimer_rubrique":
					
					break;
					
				case "supprimer_article":
					
					break;
					
			}
		}

		public function liste($dir)
		{
			if(countFiles($dir)>0)
			{
				
				$dh  = opendir($dir);
				while (false !== ($filename = readdir($dh))) 
				{
					if(substr($filename, 0, 1) != ".")
					{
						$file = explode ('.', $filename);
						if(count($file) == 4)
						{
							$role = $file[0];
							$rubrique = $file[1];
							$page = $file[2];
							$ext = $file[3];
							$liste_pages[$role][$rubrique][] = $page;
						}
					}
				}
				if(count($liste_pages) > 0)
				{
					$content .= '<ul class="puce-off cms-roles">';
					foreach ($liste_pages as $role => $rubriques)
					{
						$content .= "<li><h3>" . $role . "</h4>";
						if(count($rubriques) > 0)
						{
							$content .= '<div class="dd" id="FG-arbo"><ol class="dd-list">';
							foreach ($rubriques as $rubrique => $pages)
							{
								$filename= $role . "." . $rubrique . "." . $page;
										
										$content .= '<li class="dd-item dd3-item dd-heading" data-id="1">

		                          <div class="dd-handle dd3-handle">Drag</div>
		                          <div class="dd3-content">
		                            <strong>' . $rubrique . '</strong>
		                            <div class="controls">
		                              <a href="?page=cms&function=editer_article&article='. $filename . '" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil-square-o"></i></a>
		                            </div>
		                          </div>';
								if(count($pages) > 0)
								{
									$content .= " <ol class=\"dd-list\">";
									foreach ($pages as $page)
									{
										$filename= $role . "." . $rubrique . "." . $page;
										
										$content .= '<li class="dd-item dd3-item dd-field" data-id="12">
		                              <div class="dd-handle dd3-handle">Drag</div>
		                              <div class="dd3-content">
		                                <strong>'. $page . '</strong>
		                                <div class="controls">
		                                  <a href="?page=cms&function=editer_article&article='. $filename . '" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><i class="fa fa-pencil-square-o"></i></a>
		                                </div>
		                              </div>
		                            </li>';
									}
									$content .= "</ol>";
								}
								$content .= "</li>";
							}
							$content .= "</ol></div>";
						}
						$content .= "</li>";
					}
					$content .= "</ul>";
				}
			}

			return $content;
		}

		public function ajoute_rubrique($titre, $texte, $url)
		{
			ajoute_objet("rubrique", $titre, $chapo, $texte, $url);
		}
		
		public function ajoute_article($titre, $chapo, $texte, $url)
		{
			ajoute_objet("article", $titre, $chapo, $texte, $url);
		}
		
		public function ajoute_objet($type, $titre, $chapo, $texte, $url)
		{
		
			if(!url_exists($url))
			{
			
			}
			else
			{
			
			}
		}
	}

?>