<?php
	include_once(_VHOST_PATH . 'modules/cms/class.cms.php');
	switch($module_hook)
	{
		case "menu_principal":
			$libelle = "Gestion des contenus";
			$code = "cms";
			$position = "primary"; // "module" pour être placé dans le sous menu modules
			$icon = "fa-edit"; // liste des icones : 
			$color = "green"; // liste des couleurs : 

			break;

		case "admin":
			/*
			 *  Admin section modules
			 *  Interface d'administration du module
			 */
			$cms = new Cms();
			$content = $cms->route($_FUNCTION);
			$templateDir = "modules/cms/templates/";
			$templateFile = $cms->templateFile;
			$tmpVars = $cms->tmpVars;
			$replace = $cms->replace;
			$dynamic_vars = $cms->dynamic_vars;
			break;

		case "generer_application":
			/*
			 *  framework/generer_application.php
			 *  Inclusion des blocs modules dans les templates générées.
			 */

			$TEMPLATE_FILE .= "<div class=\"module\">\n\t{#CMS=" . $FW_ROLE . "." . $FW_RUBRIQUE . "." . $module->getAttribute('name') . "}\n</div>\n";
			$file_cms = "../../" . $FW_APPLICATION . "/cms/" . $FW_ROLE . "." . $FW_RUBRIQUE . "." . $module->getAttribute('name') . ".html";
			if (!file_exists($file_cms)) {
				$fc = fopen($file_cms, "w");
				fwrite($fc, $FW_RUBRIQUE);
				fclose($fc);
			}
			
			break;

		case "parse_template":
			/*
			 *  classes/class.page.php
			 * On remplace les #CMS => {#CMS=page}
			 */
			
			if(preg_match_all('/\{#CMS=(.*?)}/m', $page, $tag_tmp))
			{
				
				for($k=0; $k<=count($tag_tmp[0]) - 1; $k++)
				{
					$var[$k]='/'.$tag_tmp[0][$k]."/i";
					$cms_file = "app/cms/".$tag_tmp[1][$k] . ".html";
					
					if(file_exists($cms_file))
					{
						$contenu = join('', file($cms_file));
						
						//$replace[$k] = Markdown::defaultTransform($contenu);
						$replace[$k] = $this->parse($contenu);
					}
				}
				
				$page = preg_replace($var, $replace, $page);
			}
			break;
			case "inject_rsc":

			$module_scripts .= '<script src="modules/cms/templates/module.js"></script>';

			$module_css .= '';

		break;
	}

?>