<?php
	include_once(_VHOST_PATH . 'modules/blog/class.blog.php');
	switch($module_hook)
	{
		case "menu_principal":
			$libelle = "Actualités/Blog";
			$code = "blog";
			$position = "primary"; // "module" pour être placé dans le sous menu modules
			$icon = "fa-globe"; // liste des icones : 
			$color = "red60"; // liste des couleurs : 
			
			break;

		case "admin":
			/*
			 *  Admin section modules
			 *  Interface d'administration du module
			 */

			$blog = new Blog();
			$content = $blog->route($_FUNCTION);
			
			$templateDir = "modules/blog/templates/";
			$templateFile = $blog->templateFile;
			$tmpVars = $blog->tmpVars;
			$replace = $blog->replace;
			$dynamic_vars = $blog->dynamic_vars;
			break;

		case "generer_application":
			/*
			 *  framework/generer_application.php
			 *  Inclusion des blocs modules dans les templates générées.
			 */

			$blog_options = "";
			if($module->getAttribute('pagination') != ""){$blog_options = ";pagination:" . $module->getAttribute('pagination');}
			if($module->getAttribute('template') != ""){$blog_options = ";template:" . $module->getAttribute('template');}
			$TEMPLATE_FILE .= "<div class=\"module\">\n\t{#BLOG=" . $module->getAttribute('rubriqueUrl') . $blog_options . "}\n</div>\n";
			$rubrique = $T100->selectByUrl($module->getAttribute('rubriqueUrl'));
			if(count($rubrique) == 0)
			{
				$T100->insert($module->getAttribute('rubriqueUrl'), $module->getAttribute('titre'), "");
			}

			break;

		case "parse_template":
			/*
			 *  classes/class.page.php
			 * On remplace les #BLOG => {#BLOG=page}
			 */

			if(preg_match_all('/\{#BLOG=(.*?)}/m', $page, $url_tmp))
			{
				/* Traitement */
				for($k=0; $k<=count($url_tmp[0]) - 1; $k++)
				{
					$var[$k]='/'.$url_tmp[0][$k]."/i";
					$params = $url_tmp[1][$k];
					$params = explode(';', $params);
					
					$url_rubrique = $params[0];
					
					for($kAtt = 1; $kAtt < count($params); $kAtt++)
					{
						/* Attribut = pagination|template */
						
						$attribut = explode(':', $params[$kAtt]);
						$$attribut[0] = $attribut[1];
					}
					$pagination = $pagination > 0 ? $pagination : 5;
					$cur = isset($_REQUEST['cur']) ? $_REQUEST['cur'] : 1;
					
					$blog = new Blog();
					$blog->setRubrique($url_rubrique);
					$blog->setCurrentPage($cur);
					$blog->setMaxResults($pagination);
					$blog->setTemplate($this->templateDir, $template);
					//$replace[$k] = Markdown::defaultTransform($blog->result());
					$replace[$k] = $blog->result();
					
				}	
				$page = preg_replace($var, $replace, $page);	
			}
		break;	
		case "inject_rsc":

			$module_scripts .= '<script src="modules/blog/templates/module.js"></script>';

			$module_css .= '';

		break;
	}

?>