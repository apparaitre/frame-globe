<?php

	class Blog {

		var $templateFile;
		var $tmpVars;
		var $replace;
		var $dynamic_vars;
		var $db;
		var $pageCurrent;
		var $resultsMax;
		var $pageCount;
		var $nbResults;
		var $items;
		var $templateDir;
		var $template;
		var $type;
		var $rubrique;
		var $T100;
		var $T101;

		public function __construct()
		{
			global $conn, $T100, $T101;
			$this->T100 = $T100;
			$this->T101 = $T101;
			$this->db = $conn;
			$this->pageCurrent = 0;
			$this->resultsMax = 10;
			$this->pageCount = 0;
			$this->nbResults = 0;
			$this->items = array();
			$this->templateDir = "";
			$this->template = "";
			$this->type = "article";
			$this->rubrique = "default";
		}

		public function route($function)
		{
			switch($function)
			{
				default:
				case "liste_articles":
				case "liste_rubriques":
					$liste_rubriques = $this->liste_rubriques();
					$line = isset($_GET['line']) ? $_GET['line'] : 0;
					$_SESSION['codeinterne'] = $_SESSION['contexte']['modules']['blog']['rubrique'][$line];
					$rubrique = $this->T100->selectByID($_SESSION['codeinterne']);

					$liste_articles = $this->liste_articles($_SESSION['codeinterne']);

					$this->templateFile = "liste.html";
					$this->tmpVars = array("/{CONTENT}/i", "/{LISTE_ARTICLES}/i", "/{LISTE_RUBRIQUES}/i", "/{LINE}/i", "/{RUBRIQUE}/i");
					$this->replace = array($content, $liste_articles, $liste_rubriques, $line, $rubrique->T100_titre_va);
					$this->dynamic_vars = array();
		   			break;

		   		case "enregistrer_rubrique":
		   			$liste_rubriques = $this->liste_rubriques();
		   			$liste_articles = $this->liste_articles($_SESSION['codeinterne']);

					$nom_rubrique = validInputData($_POST['nom_rubrique'], "string");
					$texte_rubrique = validInputData($_POST['texte_rubrique'], "string");
					$this->T100->insert($_SESSION['user']['id_user'], nettoie_chaine($nom_rubrique), nettoie_chaine($nom_rubrique), $nom_rubrique, $texte_rubrique, $_SESSION['locale'], "draft");

					$content = $this->liste_rubriques();
					$this->templateFile = "liste.html";
					$this->tmpVars = array("/{CONTENT}/i", "/{LISTE_ARTICLES}/i", "/{LISTE_RUBRIQUES}/i");
					$this->replace = array($content,$liste_articles, $liste_rubriques);
					$this->dynamic_vars = array();
					break;
					
				case "editer_rubrique":
					
					break;
					
				
				case "supprimer_rubrique":
					
					break;

				case "ajouter_article":

				case "editer_article":
				
					$T101_codeinterne_i = $_SESSION['contexte']['modules']['blog']['article'][$_GET['line']];
					if($T101_codeinterne_i > 0)
					{
						$article = $this->T101->selectByID($T101_codeinterne_i);
						$blog_titre = $article->T101_titre_va;
						$blog_soustitre = $article->T101_soustitre_va;
						$blog_introduction = $article->T101_chapo_va;
						$blog_texte = $article->T101_texte_va;
					}

					if(isset($_POST['hash']) && $_POST['hash'] == $_SESSION['hash'])
					{
						// Si le hash récupéré au hash sauvegardé on traite le formulaire
						$T01_codeinterne_i = validInputData($_POST['T01_codeinterne_i'], "int");
						$blog_titre = validInputData($_POST['blog_titre']);
						$blog_soustitre = validInputData($_POST['blog_soustitre']);
						$blog_introduction = validInputData($_POST['blog_introduction']);
						$blog_texte = validInputData($_POST['blog_texte']);

						if($blog_titre != "")
						{
							if($T101_codeinterne_i > 0)
							{
								$result = $this->T101->update(nettoie_chaine($blog_titre), $blog_titre, $blog_soustitre, $blog_introduction, $blog_texte, $T101_codeinterne_i);
							}
							else
							{
								print_r(array($_SESSION['user']['id_user'], $_SESSION['codeinterne'], "article", nettoie_chaine($blog_titre), $blog_titre, $blog_soustitre, $blog_introduction, $blog_texte, $_SESSION['locale'], "off_line"));
								$T101_codeinterne_i = $this->T101->insert($_SESSION['user']['id_user'], $_SESSION['codeinterne'], "article", nettoie_chaine($blog_titre), $blog_titre, $blog_soustitre, $blog_introduction, $blog_texte, $_SESSION['locale'], "off_line");
								$result = $T101_codeinterne_i > 0 ? true : false;
							}
							if($result)
							{
								$page_url = new Url();
								$url = $page_url->getUrl("modules,blog");
								header('location:' . $url);
							}
							else
							{
								/* erreur de sauvegarde */
								$message = "Erreur de sauvegarde";
							}
						}
						else
						{
							$blog_titre_err = "Vous devez indiquer un titre";
							$blog_titre_err = "error";
						}

					}
					else
					{
						// Initialisation du hash au premier accès
						$_SESSION['hash'] = md5(time());
					}

					

					$this->templateFile = "editer_article.html";
					$this->tmpVars = array("/{T101_codeinterne_i}/i", "/{BLOG_TITRE}/i", "/{BLOG_TITRE_ERR}/i", "/{BLOG_TITRE_CLASS}/i", "/{BLOG_SOUSTITRE}/i", "/{BLOG_INTRODUCTION}/i", "/{BLOG_TEXTE}/i", "/{HASH}/i");
					$this->replace = array($T101_codeinterne_i, $blog_titre, $blog_titre_err, $blog_titre_class, $blog_soustitre, $blog_introduction, $blog_texte, $_SESSION['hash']);	
					$this->dynamic_vars = array();
					break;
					
				
				
				case "supprimer_article":
					
					break;
					
			}
		}

		public function liste_rubriques()
		{
			$rubriques =  $this->T100->selectAll();
			$line = 0;
			foreach($rubriques as $rubrique)
			{
				$_SESSION['contexte']['modules']['blog']['rubrique'][$line] = $rubrique->T100_codeinterne_i;
				
				if($rubrique->T04_code_statut_va == "on_line")
				{
					$checked_online = "checked";
					$active_online = "active";
					$checked_offline = "";
					$active_offline = "";
				}
				else
				{
					$checked_online = "";
					$active_online = "";
					$checked_offline = "checked";
					$active_offline = "active";
				}

				$templateDir = "modules/blog/templates/";
				$templateFile = "item_rubrique.html";
				$tmpVars = array("/{CODEINTERNE}/i", "/{STATUT}/i", "/{LINE}/i", "/{URL}/i", "/{TITRE}/i", "/{DATE}/i", "/{TEXTE}/i", "/{CHECKED_OFFLINE}/i", "/{CHECKED_ONLINE}/i", "/{ACTIVE_OFFLINE}/i", "/{ACTIVE_ONLINE}/i");
				$replace = array($rubrique->T100_codeinterne_i, $statut, $line, $rubrique->T100_url_va, $rubrique->T100_titre_va, mysql2frdate($rubrique->T100_date_d),  $rubrique->T100_texte_va, $checked_offline, $checked_online, $active_offline, $active_online);

				$page = new Page($templateDir, $templateFile,  $_SESSION["locale"]);
				$page->setVars($dynamic_vars);
				
				$page->replace_tags($tmpVars, $replace);
				$content .= $page->output();
				$line ++ ;
			}

			return $content;
		}

		public function liste_articles($T100_codeinterne_i)
		{
			$liste_articles = $this->T101->selectByRubrique($T100_codeinterne_i);
			if(count($liste_articles) >0)
			{
				$line = 0;
				foreach($liste_articles as $article)
				{
					$_SESSION['contexte']['modules']['blog']['article'][$line] = $article->T101_codeinterne_i;
					if($article->T04_code_statut_va == "on_line")
					{
						$checked_online = "checked";
						$active_online = "active";
						$checked_offline = "";
						$active_offline = "";
					}
					else
					{
						$checked_online = "";
						$active_online = "";
						$checked_offline = "checked";
						$active_offline = "active";
					}

					$templateDir = "modules/blog/templates/";
					$templateFile = "item_article.html";
					$tmpVars = array("/{CODEINTERNE}/i", "/{STATUT}/i", "/{LINE}/i", "/{URL}/i", "/{TITRE}/i", "/{DATE}/i", "/{TEXTE}/i", "/{CHECKED_OFFLINE}/i", "/{CHECKED_ONLINE}/i", "/{ACTIVE_OFFLINE}/i", "/{ACTIVE_ONLINE}/i");
					$replace = array($article->T101_codeinterne_i, $statut, $line, $article->T101_url_va, $article->T101_titre_va, mysql2frdate($article->T101_date_d),  $article->T101_texte_va, $checked_offline, $checked_online, $active_offline, $active_online);

					$page = new Page($templateDir, $templateFile,  $_SESSION["locale"]);
					$page->setVars($dynamic_vars);
					
					$page->replace_tags($tmpVars, $replace);
					$content .= $page->output();
					$line ++ ;
				}
				
			}
			
			return $content;
		}

		public function setRubrique($rubrique)
		{
			$this->rubrique = $rubrique;
		}
		
		public function setTemplate($templateDir, $template)
		{
			$this->templateDir = $templateDir;
			$this->template = $template;
		}
		
		public function pagination($pagination)
		{
			$this->resultsMax = $pagination;
		}
		
		function result()
		{
			global $T101, $selectByUrlRubrique;
			if($this->resultsMax > 0)
			{
				$selectByUrlRubrique = "SELECT * from t101_articles t101 left join t100_rubriques t100 on t100.t100_codeinterne_i=t101.T100_codeinterne_i where t100.T100_url_va=? order by T101_date_d DESC ";
				$result = $T101->selectByUrlRubrique($this->rubrique);
				

				$this->nbResults = count($result);
				
				$this->pageCount = ceil($this->nbResults/$this->resultsMax);
				
				$selectByUrlRubrique .= " LIMIT " . ( ($this->pageCurrent - 1 ) * $this->resultsMax ) . ", " . $this->resultsMax;
				$pagination = $this->getPagination();
				
				$items = $T101->selectByUrlRubrique($this->rubrique);
				$blog = "";
				foreach($items as $article)
				{
					$page_article = new Page($this->templateDir, $this->template . ".html", $locale);
					switch($this->type)
					{
						default:
						case "article":
							$tmpVars_article = array('/{TITRE}/i', '/{SOUSTITRE}/i', '/{CHAPO}/i', '/{TEXTE}/i', );
							$replace_article = array($article->T101_titre_va, $article->T101_soustitre_va, $article->T101_introduction_va, $article->T101_texte_va);
							break;
					}
					
					$page_article->replace_tags($tmpVars_article, $replace_article);
					$blog .= $page_article->output();
				}
			}


			return $blog . $pagination ;
		}

		public function getPagination($url = "")
		{
			$adj = 1;
			$link = strpos($url , "?") === false ? "?cur=" : "&cur=";

			// Initialisation des variables
			$prev = $this->pageCurrent - 1; // numéro de la page précédente
			$next = $this->pageCurrent + 1; // numéro de la page suivante
			$penultimate = $this->pageCount - 1; // numéro de l'avant-dernière page
			$pagination = ''; // variable retour de la fonction : vide tant qu'il n'y a pas au moins 2 pages

			if ($this->pageCount > 1) {
				$pagination .= '<ul class="pagination_container row">';
			

				/* =================================
				 *  Affichage du bouton [précédent]
				 * ================================= */
				if ($this->pageCurrent > 1) {
					$pagination .= "<li class=\"col txtleft\"><a class=\"bouton bouton_violet\" href=\"{$url}{$link}{$prev}\">Retour</a> </li>";
				}

				/**
				 * Début affichage des pages, l'exemple reprend le cas de 3 numéros de pages adjacents (par défaut) de chaque côté du numéro courant
				 * - CAS 1 : il y a au plus 12 pages, insuffisant pour faire une troncature
				 * - CAS 2 : il y a au moins 13 pages, on effectue la troncature pour afficher 11 numéros de pages au total
				 */

				/* ===============================================
				 *  CAS 1 : au plus 12 pages -> pas de troncature
				 * =============================================== */
				if ($this->pageCount < 7 + ($adj * 2)) {
					$pagination .= '<li class="col txtcenter">';
					// Ajout de la page 1 : on la traite en dehors de la boucle pour n'avoir que index.php au lieu de index.php?p=1 et ainsi éviter le duplicate content
					$pagination .= ($this->pageCurrent == 1) ? '<b class="pagination_active">1</b>' : " <a class=\"pagination\" href=\"{$url}{$link}1\">1</a> "; // Opérateur ternaire : (condition) ? 'valeur si vrai' : 'valeur si fausse'

					// Pour les pages restantes on utilise itère
					for ($i=2; $i<=$this->pageCount; $i++) {
						if ($i == $this->pageCurrent) {
							// Le numéro de la page courante est mis en évidence (cf. CSS)
							$pagination .= "<b class=\"pagination_active\">{$i}</b>";
						} else {
							// Les autres sont affichées normalement
							$pagination .= "<a class=\"pagination\"  href=\"{$url}{$link}{$i}\">{$i}</a>";
						}
					}
					$pagination .= '</li>';
				}
				/* =========================================
				 *  CAS 2 : au moins 13 pages -> troncature
				 * ========================================= */
				else {
					/**
					 * Troncature 1 : on se situe dans la partie proche des premières pages, on tronque donc la fin de la pagination.
					 * l'affichage sera de neuf numéros de pages à gauche ... deux à droite
					 * 1 2 3 4 5 6 7 8 9 … 16 17
					 */
					 $pagination .= '<li class="col txtcenter">';
					if ($this->pageCurrent < 2 + ($adj * 2)) {
						// Affichage du numéro de page 1
						$pagination .= ($this->pageCurrent == 1) ? "<b class=\"pagination_active\">1</span>" : "<a  class=\"pagination\" href=\"{$url}\">1</a>";

						// puis des huit autres suivants
						for ($i = 2; $i < 4 + ($adj * 2); $i++) {
							if ($i == $this->pageCurrent) {
								$pagination .= "<b class=\"pagination_active\">{$i}</b>";
							} else {
								$pagination .= "<a class=\"pagination\"  href=\"{$url}{$link}{$i}\">{$i}</a>";
							}
						}

						// ... pour marquer la troncature
							$pagination .= "<b class=\"pagination_active\">&hellip;</b>";

						// et enfin les deux derniers numéros
						$pagination .= "<a  class=\"pagination\" href=\"{$url}{$link}{$penultimate}\">{$penultimate}</a>";
						$pagination .= "<a  class=\"pagination\" href=\"{$url}{$link}{$this->pageCount}\">{$this->pageCount}</a>";
					}
					/**
					 * Troncature 2 : on se situe dans la partie centrale de notre pagination, on tronque donc le début et la fin de la pagination.
					 * l'affichage sera deux numéros de pages à gauche ... sept au centre ... deux à droite
					 * 1 2 … 5 6 7 8 9 10 11 … 16 17
					 */
					elseif ( (($adj * 2) + 1 < $this->pageCurrent) && ($this->pageCurrent < $this->pageCount - ($adj * 2)) ) {
						// Affichage des numéros 1 et 2
						$pagination .= "<a class=\"pagination\"  href=\"{$url}\">1</a>";
						$pagination .= "<a  class=\"pagination\" href=\"{$url}{$link}2\">2</a>";
							$pagination .= "<b class=\"pagination_active\">&hellip;</b>";

						// les pages du milieu : les trois précédant la page courante, la page courante, puis les trois lui succédant
						for ($i = $this->pageCurrent - $adj; $i <= $this->pageCurrent + $adj; $i++) {
							if ($i == $this->pageCurrent) {
								$pagination .= "<b class=\"pagination_active\">{$i}</b>";
							} else {
								$pagination .= "<a  class=\"pagination\" href=\"{$url}{$link}{$i}\">{$i}</a>";
							}
						}

						$pagination .= "<b class=\"pagination_active\">&hellip;</b>";

						// et les deux derniers numéros
						$pagination .= "<a  class=\"pagination\" href=\"{$url}{$link}{$penultimate}\">{$penultimate}</a>";
						$pagination .= "<a  class=\"pagination\" href=\"{$url}{$link}{$this->pageCount}\">{$this->pageCount}</a>";
					}
					/**
					 * Troncature 3 : on se situe dans la partie de droite, on tronque donc le début de la pagination.
					 * l'affichage sera deux numéros de pages à gauche ... neuf à droite
					 * 1 2 … 9 10 11 12 13 14 15 16 17
					 */
					else {
						// Affichage des numéros 1 et 2
						$pagination .= "<a  class=\"pagination\" href=\"{$url}\">1</a>";
						$pagination .= "<a  class=\"pagination\" href=\"{$url}{$link}2\">2</a>";
							$pagination .= "<b class=\"pagination_active\">&hellip;</b>";

						// puis des neuf derniers numéros
						for ($i = $this->pageCount - (2 + ($adj * 2)); $i <= $this->pageCount; $i++) {
							if ($i == $this->pageCurrent) {
								$pagination .= "<b class=\"pagination_active\">{$i}</b>";
							} else {
								$pagination .= "<a class=\"pagination\" href=\"{$url}{$link}{$i}\">{$i}</a>";
							}
						}
					}
					
					$pagination .= '</li>';
				}

				/* ===============================
				 *  Affichage du bouton [suivant]
				 * =============================== */

					
				if($this->pageCurrent<($this->pageCount)){$pagination .= " <li class=\"col txtleft\"><a class=\"bouton bouton_violet right col w25 txtright\" href=\"{$url}{$link}{$next}\">Suite</a> </li>";}
					
				$pagination .= '</ul>';
			}

			return ($pagination);
		}
		
		public function setCurrentPage($cur)
		{
			$this->pageCurrent = $cur;
		}
		
		public function setMaxResults($val)
		{
			$this->resultsMax = $val;
		}
		
		
		
	}

?>